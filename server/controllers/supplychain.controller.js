const InventoryPlanner = require("../models/supplychain.model")
import httpStatus from "http-status"
export const getMaterialDescription = async (req, res) => {
  try {
    let limit = req.params.limit.split(",")
    const data = await InventoryPlanner.find(
      {},
      {
        MaterialCode: 1,
        MaterialDescription: 1,
        ApprovalStatus: 1,
        InventoryHealth: 1,
        "HistoricalInventoryFlow.Outgoing.ActualSales": 1,
        "HistoricalInventoryFlow.EndingInventory": 1,
      }
    )
      .skip(parseInt(limit[0]))
      .limit(parseInt(limit[1]))
    let count = await InventoryPlanner.countDocuments()
    return res.status(200).json({
      message: "done",
      data,
      totalCount: count,
      currentCount: data.length,
    })
  } catch (err) {
    return res.status(500).json(err)
  }
}
export const getDataByMaterialDescription = async (req, res) => {
  try {
    const code = req.params.code
    const data = await InventoryPlanner.find({ MaterialCode: code })
    return res.status(200).json({ message: "done", data })
  } catch (err) {
    return res.status(500).json(err)
  }
}
export const getBrands = async (req, res) => {
  try {
    const brands = await InventoryPlanner.find({}, { Product: 1 })
    let arr = [...brands]
    let brandsSet = arr.filter((item, index, ar) => {
      return index === ar.findIndex(a => a.Product === item.Product)
    })
    return res.status(200).json({ message: "Brands", data: brandsSet })
  } catch (err) {
    return res.status(500).json(err)
  }
}
//
export const getMonths = async (req, res) => {
  try {
    const month = await InventoryPlanner.find(
      {},
      { "ForecastInventoryFlow.MonthRun": 1 }
    )
    let arr = [...month]
    let monthSet = arr.filter((item, index, ar) => {
      return index === ar.findIndex(a => a.MonthRun === item.MonthRun)
    })
    return res.status(200).json({ message: "Month", data: monthSet })
  } catch (err) {
    return res.status(500).json(err)
  }
}
//
export const getDataByCurrentMonth = async (req, res) => {
  try {
    const currentMonth = req.params.date
    let limit = req.params.limit.split(",")
    const data = await InventoryPlanner.find(
      {
        ForecastInventoryFlow: { $elemMatch: { MonthRun: currentMonth } },
        "HistoricalInventoryFlow.EndingInventory": {
          $elemMatch: { key: currentMonth },
        },
      },
      { "HistoricalInventoryFlow.EndingInventory": 1 }
    )
      .skip(parseInt(limit[0]))
      .limit(parseInt(limit[1]))
    return res.status(200).json({
      message: "Month",
      data,
      currentCount: data.length,
    })
  } catch (err) {
    return res.status(500).json(err)
  }
}

export const getFilterData = async (req, res) => {
  try {
    let limit = req.params.limit.split(",")
    const body = req.body
    for (let key in body) {
      if (body[key] === "all" || body[key] === "") delete body[key]
      if (key === "MaterialDescription" && body[key]) {
        let md = body[key]
        body[key] = {
          $regex: md,
          $options: "i",
        }
      }
      if (key === "MonthRun") {
        delete body[key]
      }
    }
    const data = await InventoryPlanner.find(body, {
      MaterialCode: 1,
      MaterialDescription: 1,
      ApprovalStatus: 1,
      InventoryHealth: 1,
      "HistoricalInventoryFlow.Outgoing.ActualSales": 1,
      "HistoricalInventoryFlow.EndingInventory": 1,
      SafetyStockTime:1,
    })
    let filterData = data.slice(
      parseInt(limit[0]),
      parseInt(limit[0]) + parseInt(limit[1])
    )
    return res.status(200).json({
      message: "done",
      data: filterData,
      currentCount: data.length,
    })
  } catch (err) {
    return res.status(500).json(err.message)
  }
}


export const getInventoryPreview= async (req, res) => {
  try {
    const code = req.params.code;
    const previewPpo = req.body.ppo|| [];
    const month = req.body.MonthRun;
    const previewCForecast = req.body.cForecast || [];


    if(!code || !month){
     return res
      .status(404)
      .json( {
          status:404,
          ok:false,
          message:"Missing Code or Month",
          data:{}
         });
   }

    let data = await InventoryPlanner.findOne({ MaterialCode: code });
    if(data){
      data = data._doc;
      let forecasts = data.ForecastInventoryFlow;
      let forecastMap =  new Map(forecasts.map(i => [i.MonthRun, i]));
      let ppo =  forecastMap.get(month).Incoming.PlannedPO;
      if(!ppo){
        return res
         .status(404)
         .json( {
             status:404,
             ok:false,
             message:"No data for the selected month",
             data:{}
            });
      }
      let cForecast =  forecastMap.get(month).Outgoing.ConsensusForecast;
      let bInventory =  forecastMap.get(month).BeginningInventory;
      let eInventory =  forecastMap.get(month).EndingInventory;

     if(previewPpo.length> 0){
      ppo.forEach(function (data, index) {
       let adj = (previewPpo[index].value - (data.value|| 0));
       if(adj!= 0){
        for (let i = index; i < eInventory.length; i++) {
          eInventory[i].value = eInventory[i].value + adj;
        }
        for (let i = index+1; i < bInventory.length; i++) {
          bInventory[i].value = bInventory[i].value + adj

        }
       }
      });
    }

    if(previewCForecast.length> 0){
      cForecast.forEach(function (data, index) {
        let adj = ((data.value|| 0) - previewCForecast[index].value);
        if(adj!= 0){
         for (let i = index; i < eInventory.length; i++) {
           eInventory[i].value = eInventory[i].value + adj;
         }
         for (let i = index+1; i < bInventory.length; i++) {
           bInventory[i].value = bInventory[i].value + adj;
         }
        }
       });
    }

    return res.status(200).json(
      { message: "Preview data",
        ok:true,
        status:200,
       data:{endingInventory:eInventory, beginningInventory:bInventory}});

  }
  } catch (err) {
    return res.status(500).json(err)
  }
}


export const updateInventory = async (req, res) => {
  const body = req.body
  try {
    if (!body._id) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No data available." })
    } else {
      let inventory;
      if(body.flowType==="planned"){
        inventory = await InventoryPlanner.update(
          { _id: body._id },
          { $set : { "ForecastInventoryFlow.$[m].Incoming.PlannedPO" : body.obj } },
          { arrayFilters : [ {"m.MonthRun" : body.MonthRun  } ],
          multi : true }
      )
      }else if(body.flowType==="consensus"){
        inventory = await InventoryPlanner.update(
          { _id: body._id },
          { $set : { "ForecastInventoryFlow.$[m].Outgoing.ConsensusForecast" : body.obj } },
          { arrayFilters : [ {"m.MonthRun" : body.MonthRun  } ],
          multi : true }
      )
      }
      const updatedObject = await InventoryPlanner.find({_id:body._id})

      return res.status(httpStatus.OK).json({
        status: true,
        message: "Inventory Updated Successfully",
        updatedObject,
      })
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}
