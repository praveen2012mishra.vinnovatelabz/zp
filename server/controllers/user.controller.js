const User = require("../models/user.model")
const multer = require("multer")
const upload = multer()
var crypto = require("crypto")
const httpStatus = require("http-status")

//Simple version, without validation or sanitation
export function test(req, res) {
  res.send("Greetings from the User controller!")
}

export const getUserProfileByName = async (req, res) => {
  const userName = req.params.userName
  if (userName) {
    await User.findOne({ name: userName }, (err, resp) => {
      if (err) {
        console.log(err)
      } else {
        return res.status(200).json({
          success: true,
          userProfile: resp,
        })
      }
    })
  }
}

export const updateUserProfile = async (req, res) => {
  let body = req.body
  if (body.profileData._id)
    User.findByIdAndUpdate(
      { _id: body.profileData._id },
      body.profileData,
      function (err, resp) {
        if (err) {
          console.log("222", err)
        } else {
          return res
            .status(200)
            .json({ status: true, message: "Profile Updated Successfully" })
        }
      }
    )
}

export const saveUserRoles = async (req, res) => {
  try {
  if (req.body.userName && req.body.groupName && req.body.roleName && req.body.email) {
  let name_id = req.body.userName;
  let group_id = req.body.groupName;
  let role_id = req.body.roleName;
  let email_id = req.body.email;

  User.findOne({
    name: name_id
  }, function (err, user) {
    if (err) {
      return res
           .status(httpStatus.UNPROCESSABLE_ENTITY)
           .json( {
               status:406,
               ok:false,
               message:"Please try again later",
               data:{Error:err}
              });
    }
    else if (user !== null) 
    {     let currentRoles = user.roles|| [];
           currentRoles = currentRoles.filter(v => v.groupId !== group_id)
          currentRoles.push({groupId:group_id, role:role_id});
          user.set({ roles: currentRoles})
          user.save(function (err, updatedUser) {
            if (err){
              return res
              .status(httpStatus.UNPROCESSABLE_ENTITY)
              .json( {
                  status:406,
                  ok:false,
                  message:"Please try again later",
                  data:{Error:err}
                 });
            }
            else{
              res.status(httpStatus.OK).json({
                status:200,
                ok:true,
                message:"Sucess Updating Saving user Role",
                data:updatedUser
               });
            }    
           } );
    } else {
      let newUserRole = [{groupId:group_id, role:role_id}]
      var newUser = new User({
        name: name_id,
        provider: 'activedirectory',
        active: false,
        email:email_id,
        roles:newUserRole
      })
      newUser.save(
        function (err) {
          if (err) {
            return res
            .status(httpStatus.UNPROCESSABLE_ENTITY)
            .json( {
                status:406,
                ok:false,
                message:"Please try again later",
                data:{Error:err}
               });
          }
          res.status(httpStatus.OK).json({
            status:200,
            ok:true,
            message:"Sucess Updating Saving user Role",
            data:newUser
           });
        })
    }
  })

} else {
  res
    .status(httpStatus.UNPROCESSABLE_ENTITY)
    .json( {
        status:406,
        ok:false,
        message:"Required data missing",
        data:{}
       })
}
} catch (err) {
  res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
      status:500,
      ok:false,
      message:"Internal Server Error",
      data:{}
     })
}

}



function migrateUserRoles(){
  User.find({}, (err, users) => {
    if (err) {
      console.log("error")
    }
    for (const user of users) {
      user.roles = [{groupId : 'default',role:'Basic User'}];
      user.save((err, data) => {
        if(err){console.log("error saving data")}
      })
  
    }
  })
}

//migrateUserRoles();

