"use strict"
const Notification = require("../models/notifications.model")
const httpStatus = require("http-status")

export const saveNotification = async (req, res) => {
  const body = req.body
  try {
    if (body.userId || body.groups) {
      if (!body.type || !body.header) {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "Please enter all fields." })
      } else {
        const notifications = await Notification.create(body)
        if (notifications) {
          return res.status(httpStatus.OK).json({
            status: true,
            message: "Notifications Created successfully",
          })
        } else {
          return res
            .status(httpStatus.UNPROCESSABLE_ENTITY)
            .json({ message: "Please try again" })
        }
      }
    } else {
      res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "Please select either user or groups/roles" })
    }
  } catch (err) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}

export const getNotificationsByUserId = async (req, res) => {
  const userId = req.params.userId
  try {
    if (!userId) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No data found" })
    } else {
      const notification = await Notification.find({ userId })
      if (notification.length) {
        return res.status(httpStatus.OK).json({ status: true, notification })
      } else {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "No data found", length: notigication.length })
      }
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}

export const updateCheckedNotification = async (req, res) => {
  const body = req.body
  try {
    if (!body.id) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No Notifications available." })
    } else {
      const notified = await Notification.update(
        { _id: { $in: body.id } },
        { $set: { checked: true } },
        { multi: true }
      )
      return res.status(httpStatus.OK).json({
        status: true,
        message: "Notification successfully checked",
        notified,
      })
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}
