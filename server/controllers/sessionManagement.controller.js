'use strict';
const pg = require('pg');
const config = require('../config/environment/index');
var async = require("async");
const SessionManager = require('../models/sessionManagement.model');

import User from '../models/user.model';


var mongoose = require('mongoose');


function getConnString() {
  var port = config.postgres.port === null || config.postgres.port == '' ? '5432' : config.postgres.port;
  var connString = 'postgres://' + config.postgres.username + ':' + config.postgres.pwd + '@' + config.postgres.server + ':' + port + '/' + config.postgres.database;
  return connString;
}

function handleError(res, err) {
  return res.status(500).json(err);
}

export function changeSingleSessionStatus(req, res) {
  var group = req.body.group_name;
  var type = req.body.type;
  if (type == "enable") {
    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.single_session_enforced = true;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });

        }
        else {
          var sessionInformation = new SessionManager({ group_name: group, single_session_enforced: true, two_factor_authentication_enforced: false });

          // save model to database
          sessionInformation.save(function (er, groupInfo) {
            if (er) { handleError(res, er) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInfo.group_name
              });
            }

          });
        }
      }
    })

  }
  else if (type == "disable") {
    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.single_session_enforced = false;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });
        }
        else {
          return res.status(200).json({
            success: false,
            group: group
          });
        }
      }
    })
  }
}



export function change2FAStatus(req, res) {
  var group = req.body.group_name;
  var type = req.body.type;

  if (type == "enable") {
    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.two_factor_authentication_enforced = true;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });

        }
        else {
          var sessionInformation = new SessionManager({ group_name: group, single_session_enforced: false, two_factor_authentication_enforced: true });

          // save model to database
          sessionInformation.save(function (er, groupInfo) {
            if (er) { handleError(res, er) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInfo.group_name
              });
            }

          });
        }
      }
    })
  }
  else if (type == "disable") {

    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.two_factor_authentication_enforced = false;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });
        }
        else {
          return res.status(200).json({
            success: false,
            group: group
          });
        }
      }
    })
  }

}

//Simple version, without validation or sanitation
export function test(req, res) {
  res.send('Greetings from the session');
};


export function getSessionInfobyGroup(req, res) {
  SessionManager.find({ 'group_name': req.params.groupName }, (err, group) => {
    if (err) { handleError(res, err) }
    return res.status(200).json(group);
  })

};

export function checkSSOStatus(req, res) {
  var user_name = req.params.userName;
  var connString = getConnString();
  var client = new pg.Client(connString);
  var match_count = 0;


  SessionManager.find({ 'single_session_enforced': true }, (err, groups) => {
    if (err) {
      handleError(res, err);
    }
    else {
      if (groups.length > 0) {
        var group_name_array = groups.map(v => v.group_name);
        client.connect(function (err) {
          if (err) {
            handleError(res, err);
          } else {
            client.query(`
                  select groupname from zp_webportal_groups_users
                  where username='${user_name}'`, function (err, result) {
              if (err) {
                handleError(res, err);
              } else {
                client.end();
                if (result.rows.length > 0) {
                  var user_groups_array = result.rows.map(g => g.groupname);
                  var found = group_name_array.some(r => user_groups_array.indexOf(r) >= 0)

                  if (found) {
                    return res.send('enforced');
                  } else {
                    return res.send('not enforced');
                  }
                }
                else {
                  return res.send('not enforced');
                }
              }
            });//query
          }//else
        });//connect        

      }
      else {
        return res.send('not enforced');
      }
    }


  });
}


export function check2FAStatus(req, res) {
  var user_name = req.params.userName;
  var connString = getConnString();
  var client = new pg.Client(connString);
  var match_count = 0;


  SessionManager.find({ 'two_factor_authentication_enforced': true }, (err, groups) => {
    if (err) {
      handleError(res, err);
    }
    else {
      if (groups.length > 0) {
        var group_name_array = groups.map(v => v.group_name);
        client.connect(function (err) {
          if (err) {
            handleError(res, err);
          } else {
            client.query(`
                select groupname from zp_webportal_groups_users
                where username='${user_name}'`, function (err, result) {
              if (err) {
                handleError(res, err);
              } else {
                client.end();
                if (result.rows.length > 0) {
                  var user_groups_array = result.rows.map(g => g.groupname);
                  var found = group_name_array.some(r => user_groups_array.indexOf(r) >= 0)

                  if (found) {
                    return res.status(200).json({enforced: true});
                  } else {
                    return res.status(200).json({enforced: false});
                  }
                }
                else {
                  return res.status(200).json({enforced: false});
                }
              }
            });//query
          }//else
        });//connect        

      }
      else {
        return res.status(200).json({enforced: false});
      }
    }


  });
}



export function getListofUserswithTwoFactorAuthentication(req, res) {
  var connString = getConnString();
  var client = new pg.Client(connString);
  var enforced_group_array = [];
  var users_with_groups_array = [];
  var count = 0;
  SessionManager.find({ 'two_factor_authentication_enforced': true }, (err, groups) => {
    if (err) { handleError(res, err) }
    else {
      var enforced_groups = groups.map(g => g.group_name);
      client.connect(async function (err) {
        if (err) {
          handleError(res, err);
        } else {
          var result = await client.query("select username from zp_webportal_groups_users where groupname= ANY ($1)", [enforced_groups]);
          var all_users_array = result.rows.map(u => u.username);

          var unique_users = all_users_array.filter(function (elem, index, self) {
            return index == self.indexOf(elem);
          })

          return res.status(200).json(unique_users);
        }
      })
    }
  })
}



export function resetQRCode(req, res) {
  var users = req.body;
  let error=null;
  for (let i = 0; i < users.length; i++) {
    let username = users[i];
    User.update({ name: username }, {
      otp_auth_url: '',
      secret: ''
    }, function (err, affected, resp) {
      if (err) {
        error=err;
      }
    })

  }
  if (error!==null) {
    return res.status(401).json({
      error: {
        message: 'Error in updating auth information',
        statusCode: 401
      }
    });
  }
  else {
    //req.session.reset();

    return res.status(200).json({
      success: true
    });
  }
}



export function getListofUserswithEnforcedSingleSession(req, res) {
  var connString = getConnString();
  var client = new pg.Client(connString);
  var enforced_group_array = [];
  var users_with_groups_array = [];
  var count = 0;
  SessionManager.find({ 'single_session_enforced': true }, (err, groups) => {
    if (err) { handleError(res, err) }
    else {
      var enforced_groups = groups.map(g => g.group_name);
      client.connect(async function (err) {
        if (err) {
          handleError(res, err);
        } else {
          var result = await client.query("select username from zp_webportal_groups_users where groupname= ANY ($1)", [enforced_groups]);
          var all_users_array = result.rows.map(u => u.username);

          var unique_users = all_users_array.filter(function (elem, index, self) {
            return index == self.indexOf(elem);
          })

          return res.status(200).json(unique_users);
        }
      })
    }
  })
}


export function resetSessionCount(req, res) {
  var users = req.body;
  let errors=null;
  for (let i = 0; i < users.length; i++) {
    let username = users[i];
    User.update({ name: username }, {
      active: false
    }, function (err, affected, resp) {
      if (err) {
        errors=err;
      }
    })

  }
  if (errors!==null) {
    return res.status(401).json({
      error: {
        message: 'Error in updating session information',
        statusCode: 401
      }
    });
  }
  else {
    //req.session.reset();

    return res.status(200).json({
      success: true
    });
  }
}




