'use strict';
var https = require('https');
var request = require("request");

const ActivityLog = require('../models/activitylogger.model');
const SystemLog = require('../models/systemlogger.model');

export function storeSystemLogs(req, res) {
    if (req.body)
    {
        var file_data = req.body;
        var file_data_string_tmp = JSON.stringify(file_data);
        var types_array = ['authtemp', 'crontemp', 'yumtemp', 'ng_accesstemp', 'ng_errortemp', 'systemp'];
	    var file_data_array = file_data_string_tmp.split('SEPARATOR');
        var file_data_string_temp = file_data_string_tmp.substring(1).substring(1).slice(0, -1).slice(0, -1);
        var file_data_string = file_data_string_temp.replace(/\\n/g, "");
        var file_data_string = file_data_string.replace(/\\\\/g, "");
        var file_data_array = file_data_string.split('SEPARATOR');
        file_data_array.pop();
     


        let mainDateWiseLog = {};
        let dateWiseLog = {};
        for (let i=0; i < types_array.length; i++)
        {
            let type_name = types_array[i];
            var type = types_array[i].replace("temp"," log");
            var file_data = file_data_array[i];
            
            if (typeof file_data !== 'undefined')
            {
                var final_file_data = file_data.substring(file_data.indexOf("<"));
                var type_data = final_file_data.replace(type_name+':','' ).replace('{','').replace('}','');
                dateWiseLog[type] = type_data;
            }

        }
        
        var current_date = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') 
        mainDateWiseLog["time"] = current_date;
        mainDateWiseLog["details"] = dateWiseLog; 

        var systemLog = new SystemLog(mainDateWiseLog);
        systemLog.save();
        res.send("Log processed successfully");
    }
    else{
        res.send("No data supplied");  
    }
}


export function storeActivityLogs(req, res) {
 	
	var login_details = {};
 	let current_date_time = new Date(new Date().toUTCString());
 	login_details.action = "login";
 	login_details.name = req.body.name;
 	login_details.IP = req.body.ip;
 	login_details.login_time = new Date(new Date().toUTCString());
 	login_details.provider = 'activedirectory';
	login_details.login_successful = true;
  
        ActivityLog.findOne({name: req.body.name}, function(error, result) {
        if (!error)
        {
          if(result)
          {
          }
          else
          {
            //login_details.new_user = true;	
          }
        
        }
        var activityLog = new ActivityLog(login_details);
        activityLog.save();
        res.send("Log processed successfully");
  });
       
    
};



//Simple version, without validation or sanitation
export function test(req, res) {
    res.send('Greetings from the Logger controller!');
};

