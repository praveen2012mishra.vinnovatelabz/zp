const multer = require('multer');
const upload = multer();
var crypto = require('crypto');

var hdb    = require('hdb');
var shell = require('shelljs');


export function getMaterial(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let sqlQuery = 'SELECT DISTINCT "Material Code" from ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY' ;

            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    let material=[];
                    result.forEach(element => { 
                        material.push(element['Material Code']); 
                      }); 
                    return res.status(200).json({ result: material });
                }//else
            });//query
        }//else
    });//connect
}


export function getCurrentMonthList(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let WebGroup = req.body.WebGroup;
             let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    var result_table_name = result[0]["ResultTable"]
                    client.exec('SELECT DISTINCT "MonthRun" FROM ' + result_table_name + ' ORDER BY "MonthRun" desc', function (error, data) {
                        if (error)
                        {
                            handleError(res, error)
                        }
                        else{
                            client.end();
                            let monthRunArray = [];
                            for (let i=0; i < data.length; i++)
                            {

                                var monthRun = data[i]["MonthRun"];
                                if(!monthRunArray.includes(monthRun))
                                { 
                                    monthRunArray.push(monthRun);
                                }
                            }
                            return res.status(200).json({ result: monthRunArray });
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
    
}

export function getProductList(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {           
            let sqlQuery = 'SELECT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST' ;
            
            client.exec(sqlQuery, function (err, result) {
               
                var result_table_name = result[0]["ProductMasterTable"]
                let material_code = req.body.MaterialCode;
                let  material_description = req.body.MaterialDescription;
                
                let sqlQuery ='';
                if(material_code!='' && material_description!=''){
                    sqlQuery = 'SELECT DISTINCT * FROM ' + result_table_name +' where "IsDeleted" =0 and  "MaterialCode"=\'' + material_code + '\' and "MaterialDescription"=\'' + material_description + '\'' ;
                }else{
                    sqlQuery = 'SELECT DISTINCT * FROM ' + result_table_name +' where "IsDeleted" =0 and  "MaterialDescription"=\'' + material_description + '\'' ;
                }
                    client.exec(sqlQuery, function (error, data) {       
                        if (err) { handleError(res, err) }
                        else {
                            client.end();                
                            return res.status(200).json({ result: data});                   
                        }//else
                    });
                
            });//query
        }//else
    });//connect
}

export function getOrderMaterialData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);
    
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {

                    var result_table_name = result[0]["ProductMasterTable"];
                    sqlQuery = 'SELECT DISTINCT "MaterialDescription" FROM ' + result_table_name + ' where "IsDeleted"=0 order by "MaterialDescription" asc';
                    
                    client.exec(sqlQuery, function (error, data) {
                        if (error) {
                            handleError(res, error)
                        }
                        else{
                            client.end();
                            
                            return res.status(200).json({ result: data});
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
    
}

export function getGroupSummaryData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);
    
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let product = req.body.product;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {

                    var result_table_name = result[0]["ProductMasterTable"];
                    var group_name = result[0]["GroupName"];
                    var tableauUrl = result[0]["SummaryTableauURL"];
                    let sqlQuery ="";
                    if(product != "" && product != "All"){
                        sqlQuery = 'SELECT DISTINCT "MaterialDescription" FROM ' + result_table_name + ' where "IsDeleted"=0 and "Product"=\'' + product + '\' order by "MaterialDescription" asc';
                    }
                    else{
                        sqlQuery = 'SELECT DISTINCT "MaterialDescription" FROM ' + result_table_name + ' where "IsDeleted"=0 order by "MaterialDescription" asc';
                    }
                    
                    client.exec(sqlQuery, function (error, data) {
                        if (error) {
                            handleError(res, error)
                        }
                        else{
                            client.end();
                            
                            return res.status(200).json({ result: data, group_name: group_name, tableauUrl: tableauUrl });
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
    
}

export function getGroupData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    var group_name = result[0]["GroupName"];
                    var tableauUrl = result[0]["POTableauURL"];
                    return res.status(200).json({ group_name: group_name, tableauUrl: tableauUrl });
                }//else
            });//query
        }//else
    });//connect
}

export function getBoardData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);
    
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {

            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {  
                    var result_table_name = result[0]["ProductMasterTable"];
                     var group_name = result[0]["GroupName"];
                     if(req.body.type == 'summary'){
                        var tableauUrl = result[0]["SummaryTableauURL"];
                     }
                     else{
                        var tableauUrl = result[0]["DetailTableauURL"];
                     }   
                    let sqlQuery = 'SELECT DISTINCT "Product" FROM '+result_table_name+' where "IsDeleted"=0 order by "Product"';
                    client.exec(sqlQuery, function (err, result) {
                        if (err) { handleError(res, err) }
                        else { 
                            return res.status(200).json({ result: result, group_name: group_name, tableauUrl: tableauUrl });           
                        }//else
                    });//query
                   
                }//else
            });//connect

        }//else
    });//connect
}


export function getMaterialData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    var result_table_name = result[0]["ResultTable"];
                    var group_name = result[0]["GroupName"];
                    var tableauUrl = result[0]["DetailTableauURL"];
                    client.exec('SELECT DISTINCT "MaterialDescription" FROM ' + result_table_name + ' where "IsDeleted" =0 ORDER BY "MaterialDescription" asc', function (error, data) {
                        if (error)
                        {
                            handleError(res, error)
                        }
                        else{                            
                            client.end();                           
                            return res.status(200).json({ result: data, group_name: group_name, tableauUrl: tableauUrl });
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
}

export function getTableData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {   
                    var result_table_name = result[0]["ResultTable"]
                    //let material_code = req.body.material_code;
                    let material_description = req.body.material_description;
                    let month = req.body.month;
                    let sqlQuery = 'select distinct * from ' + result_table_name + ' where "IsDeleted" =0 and '+result_table_name+'."MaterialDescription"=\'' + material_description + '\' and '+result_table_name+'."MonthRun"=\'' + month + '\'' ;
                    client.exec( sqlQuery, function (error, data) {
                        if (error)
                        {
                            handleError(res, error)
                        }
                        else{
                            client.end();
                            let materialDataArray = [];
                           
                            return res.status(200).json({ result: data });
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
}

export function getTableDataForForecast(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let filterType = 'forecast';
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {   
                                    
                    var result_table_name = result[0]["ResultTable"]
                    let material_description = req.body.material_description;
                    let month = req.body.month;
                    
                    let sqlQuery = '';
                    if(material_description && material_description != 'All'){
                        sqlQuery = 'select distinct * from ' + result_table_name + ' where "IsDeleted" =0 and '+result_table_name+'."MaterialDescription"=\'' + material_description + '\' and '+result_table_name+'."MonthRun"=\'' + month + '\' and '+result_table_name+'."Type"=\'' + filterType + '\' order by "Month"';
                    }
                    else{
                        sqlQuery = 'select distinct * from ' + result_table_name + ' where "IsDeleted" =0 '+result_table_name+'."MonthRun"=\'' + month + '\' and '+result_table_name+'."Type"=\'' + filterType + '\' order by "Month"';
                    }
                    client.exec( sqlQuery, function (error, data) {
                        if (error)
                        {
                            handleError(res, error)
                        }
                        else{
                            client.end();
                            return res.status(200).json({ result: data });
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
}

export function getRefreshForecast(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let filterType = 'forecast';
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {   
                    //===========================
                    var extractArr = result[0]["Extract"].split("|"); 
                  
                        
                         if (shell.exec ('/opt/tableau/tabcmd/bin/tabcmd refreshextracts -s '+extractArr[0]+' -u external\\\\admAnalytics -p 3FGMyRfL0qYo9KZG -t '+extractArr[1]+' --project "'+extractArr[2]+'" --parent-project-path "'+extractArr[3]+'" --datasource "'+extractArr[4]+'"').code !== 0) {                                                   
                          shell.exit(1);
                          return res.status(200).json({ responce: 'error' });
                        }
                        else{
                            //=====================
                            var result_table_name = result[0]["ResultTable"]
                            let material_description = req.body.material_description;
                            let month = req.body.month;
                            
                            let sqlQuery = '';
                            if(material_description && material_description != 'All'){
                                sqlQuery = 'select distinct * from ' + result_table_name + ' where "IsDeleted" =0 and '+result_table_name+'."MaterialDescription"=\'' + material_description + '\' and '+result_table_name+'."MonthRun"=\'' + month + '\' and '+result_table_name+'."Type"=\'' + filterType + '\' order by "Month"';
                            }
                            else{
                                sqlQuery = 'select distinct * from ' + result_table_name + ' where "IsDeleted" =0 '+result_table_name+'."MonthRun"=\'' + month + '\' and '+result_table_name+'."Type"=\'' + filterType + '\' order by "Month"';
                            }
                            client.exec( sqlQuery, function (error, data) {
                                if (error)
                                {
                                    handleError(res, error)
                                }
                                else{
                                    client.end();
                                    return res.status(200).json({ result: data , responce: "success"});
                                }
                            })
                    }
                }//else
            });//query
        }//else
    });//connect
}



export function editTableData(req, res) {
    try {
      
        let totalExec = 0;
        var connString = getConnString();
        var client = new hdb.createClient(connString);
        client.connect(function (err) {
            if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {                
                                    
            var SAPTable = result[0]["ResultTable"]
            if (err) { handleError(res, err) }
            else {  
                req.body.rows.forEach(row => {
                    totalExec = totalExec+1;
                    let MonthRun = (!row.MonthRun)? '' : row.MonthRun;
                    let MaterialCode = (!row.MaterialCode)? '' : row.MaterialCode;
                    let MaterialDescription = (!row.MaterialDescription)? '' : row.MaterialDescription;
                    let MonthIn = (!row.Month)? '' : row.Month;
                    let Fit = (!row.Fit)? null : row.Fit;
                    let AdjustedForecast = (!row.AdjustedForecast)? null : row.AdjustedForecast;
                    let AdjustmentReason = (!row.AdjustmentReason)? '' : row.AdjustmentReason;
                    let UploadDate = (!row.UploadDate)? '' : row.UploadDate;
                    let ForecastModel = (!row.ForecastModel)? '' : row.ForecastModel;
                    let ForecastError = (!row.ForecastError)? null : row.ForecastError;
                    let Actual = (!row.Actual)? null : row.Actual;
                    let TypeIn = (!row.Type)? '' : row.Type;
                    let Username = (!row.Username)? '' : row.Username;                    
                    

                    client.prepare('call "ZP_ANALYTICS"."ZPINVESTIGATOR::FORECAST_RESULTS_UPDATE"(?,?,?,?,?,?,?,?,?,?,?,?,?,?)', function (err, statement) {
                        if (err) {
                            return console.error('Prepare error:', err);
                        }
                        statement.exec([SAPTable, MonthRun, MaterialCode, MaterialDescription, MonthIn, Fit, AdjustedForecast, AdjustmentReason, UploadDate, ForecastModel, ForecastError, Actual, TypeIn, Username],
                            function (err, parameters, dummyRows, tableRows) {
                                if (err) {
                                    client.end();
                                    console.error('Exec error:', err)
                                    return res.status(200).json({ error: err });
                                }
                                else{
                                    
                                }                               
                               
                            });
                    });
                    if(totalExec == req.body.rows.length){
                        return res.status(200).json({ result: 'Success' })
                    }
                })                       
            }//else 
            });
            }
       
        });//connect
    }
    catch (err) {
        return res.status(400).json({ error: err.message });
    }
}


export function editRestrictionData(req, res) {
    try {
        let directExec = 0;
        let indirectExec = 0;
        let directrow = req.body.rows.length;
        let indirectrow = req.body.inDiRows.length;
        var connString = getConnString();
        var client = new hdb.createClient(connString);
        client.connect(function (err) {
            try{ 
                let WebGroup = req.body.WebGroup;
                let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
                client.exec(sqlQuery, function (err, result) {                
                var SAPTable = result[0].ConstraintTable;
                        
                req.body.rows.forEach(row => {                                    
                    directExec = directExec+1;
                    let Model = (!row.Model)? '' : row.Model;
                    let PrincipalName = (!row.PrincipalName)? '' : row.PrincipalName;
                    let PrincipalCode = (!row.PrincipalCode)? '' : row.PrincipalCode;
                    let Product = (!row.Product)? '' : row.Product;
                    let MaterialCode = (!row.MaterialCode)? '' : row.MaterialCode;
                    let MaterialDescription = (!row.MaterialDescription)? '' : row.MaterialDescription;
                    let ShelfLife = (!row.shelf_no)? '' : row.shelf_no+'-'+row.shelf_name;
                    let RestockPlant = (!row.RestockPlant)? '' : row.RestockPlant;
                    let RestockPlant_LeadTime = (!row.plant_no)? '' : row.plant_no+' '+row.plant_name;
                    let RestockPlant_MOQ = (!row.RestockPlant_MOQ)? '' : row.RestockPlant_MOQ;
                    let RestockPrincipal_LeadTime = (!row.leadTime_no)? '' : row.leadTime_no+' '+row.leadTime_name;
                    let RestockPrincipal_MOQ = (!row.RestockPrincipal_MOQ)? '' : row.RestockPrincipal_MOQ;
                    let RestockPlant_BufferTime = (!row.RestockPlant_BufferTime)? '' : row.RestockPlant_BufferTime;
                    let RestockPrincipal_BufferTime =(!row.bafferTime_no)? '' : row.bafferTime_no+' '+row.bafferTime_name;
                    let Username =(!row.Username)? '' : row.Username;


                    client.prepare('call "ZP_ANALYTICS"."ZPINVESTIGATOR::FORECAST_CONSTRAINTS_UPDATE"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', function (err, statement) {
                        if (err) {
                            return console.error('Prepare error:', err);
                        }
                        statement.exec([SAPTable, Model, PrincipalName, PrincipalCode, Product, MaterialCode, MaterialDescription, ShelfLife, RestockPlant, RestockPlant_LeadTime, RestockPlant_MOQ, RestockPrincipal_LeadTime, RestockPrincipal_MOQ, RestockPlant_BufferTime, RestockPrincipal_BufferTime, Username],
                            function (err, parameters, dummyRows, tableRows) {
                                if (err) {
                                    client.end();
                                    console.error('Exec error:', err)
                                    return res.status(200).json({ error: err });
                                }
                                
                               
                            });
                    });
                })
                })
            } catch (err){
                Sentry.captureException(err);
            }

            // ----------For indirect Data
            if(req.body.inDiRows){
                try{ 
                    let WebGroup = req.body.WebGroup;
                    let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
                    client.exec(sqlQuery, function (err, result) {                
                                            
                    var SAPTable = result[0]["ConstraintTable"]                
                    req.body.inDiRows.forEach(row => {                                    
                        indirectExec = indirectExec+1;
                        let Model = (!row.Model)? '' : row.Model;
                        let PrincipalName = (!row.PrincipalName)? '' : row.PrincipalName;
                        let PrincipalCode = (!row.PrincipalCode)? '' : row.PrincipalCode;
                        let Product = (!row.Product)? '' : row.Product;
                        let MaterialCode = (!row.MaterialCode)? '' : row.MaterialCode;
                        let MaterialDescription = (!row.MaterialDescription)? '' : row.MaterialDescription;
                        let ShelfLife = (!row.shelf_no)? '' : row.shelf_no+'-'+row.shelf_name;
                        let RestockPlant = (!row.RestockPlant)? '' : row.RestockPlant;
                        let RestockPlant_LeadTime = (!row.plant_no)? '' : row.plant_no+'-'+row.plant_name;
                        let RestockPlant_MOQ = (!row.RestockPlant_MOQ)? '' : row.RestockPlant_MOQ;
                        let RestockPrincipal_LeadTime = (!row.leadTime_no)? '' : row.leadTime_no+' '+row.leadTime_name;
                        let RestockPrincipal_MOQ = (!row.RestockPrincipal_MOQ)? '' : row.RestockPrincipal_MOQ;
                        let RestockPlant_BufferTime = (!row.RestockPlant_BufferTime)? '' : row.RestockPlant_BufferTime;
                        let RestockPrincipal_BufferTime =(!row.bafferTime_no)? '' : row.bafferTime_no+' '+row.bafferTime_name;
                        let Username =(!row.Username)? '' : row.Username;


                        client.prepare('call "ZP_ANALYTICS"."ZPINVESTIGATOR::FORECAST_CONSTRAINTS_UPDATE"(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', function (err, statement) {
                            if (err) {
                                return console.error('Prepare error:', err);
                            }
                            statement.exec([SAPTable, Model, PrincipalName, PrincipalCode, Product, MaterialCode, MaterialDescription, ShelfLife, RestockPlant, RestockPlant_LeadTime, RestockPlant_MOQ, RestockPrincipal_LeadTime, RestockPrincipal_MOQ, RestockPlant_BufferTime, RestockPrincipal_BufferTime, Username],
                                function (err, parameters, dummyRows, tableRows) {
                                    if (err) {
                                        client.end();
                                        console.error('Exec error:', err)
                                        return res.status(200).json({ error: err });
                                    }
                                    //client.end();
                                    //return res.status(200).json({ result: 'Success' })
                                });
                        });
                    })
                })
                } catch (err){
                    Sentry.captureException(err);
                }
            }
            if(directExec == directrow || indirectExec == indirectrow){
                return res.status(200).json({ result: 'Success' });
            }
           
            //----------For indirect Data
        });//connect
    }
    catch (err) {
        return res.status(400).json({ error: err.message });
    }
}


export function getInvestigatorMaterialData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_MYCOMSOL_FORECAST_CONSTRAINTS ORDER BY "MaterialDescription" asc' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    client.end();
                    let materialDataArray = [];  
                    for (let i=0; i < result.length; i++)
                    {

                        var material_code = result[i]["MaterialCode"];
                        var material_description = result[i]["MaterialDescription"];
                        var data_string = "";

                        if (validateString(material_code) && validateString(material_description))
                        {
                            data_string = material_code+" + "+material_description;
                        }
                        else{
                            if (!validateString(material_code))
                            {
                                data_string = material_description;
                            }
                            else
                            {
                                if (!validateString(material_description))
                                {
                                    data_string = material_code;
                                }
                            }
                        }
                        
                        if (data_string != "")
                        {   
                            if (!materialDataArray.includes(data_string))
                            {
                                materialDataArray.push(data_string);
                            }
                        }
                    }
                    return res.status(200).json({ result: materialDataArray});
                   
                }//else
            });//query
        }//else
    });//connect
}

export function getPrincipalData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);
    
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {            
                let WebGroup = req.body.WebGroup;
                let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
                client.exec(sqlQuery, function (err, result) {
                    if (err) { handleError(res, err) }
                    else {                    
                        if(result[0]){
                            var result_table_name = result[0].ConstraintTable;
                            let material_description = req.body.material_description;
                            let manager_name = req.body.manager;
                            let brand_name = req.body.product;
                            let sqlDirect ="";
                            let sqlIndirect ="";



                            if(material_description!="" && material_description!="All"){
                                if (brand_name!="" && brand_name!="All")
                                {
                                    sqlDirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Direct\' and "MaterialDescription"=\'' + material_description + '\' and "Product"=\'' + brand_name + '\'' ;
                                
                                    sqlIndirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Indirect\' and "MaterialDescription"=\'' + material_description + '\' and "Product"=\'' + brand_name + '\'' ; 
                                }
                                else{
                                    sqlDirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Direct\' and "MaterialDescription"=\'' + material_description + '\'' ;
                                
                                    sqlIndirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Indirect\' and "MaterialDescription"=\'' + material_description + '\'' ; 

                                }
                                
                            }
                            else{
                                // sqlDirect = 'SELECT DISTINCT * FROM '+result_table_name+'where "Model"=\'Direct\'' ;
                                // sqlIndirect = 'SELECT DISTINCT * FROM '+result_table_name+'where "Model"=\'Indirect\'' ;  
                                if (brand_name!="" && brand_name!="All")
                                {
                                    sqlDirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Direct\' and "Product"=\'' + brand_name + '\'' ;
                                
                                    sqlIndirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Indirect\' and "Product"=\'' + brand_name + '\'' ; 
                                }
                                else{
                                     sqlDirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Direct\'' ;
                                     sqlIndirect = 'SELECT DISTINCT * FROM '+result_table_name+' where "IsDeleted" =0 and "Model"=\'Indirect\'' ; 

                                }
                            }

                           
                        client.exec(sqlDirect, function (error, data) {
                            if (error) { handleError(res, error) }
                            else{
                                 client.exec(sqlIndirect, function (error, inDirect) {
                                if (error) { handleError(res, error) }
                                else{    
                                            client.end();   
                                            return res.status(200).json({ result: data, inDirectdata: inDirect}); 
                                        }
                                    })
                                
                            }
                        })
                         
                        }
                        
                    }//else
                });//query


        }//else
    });//connect
}

export function getProductMaterialData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {                    
                    if(result[0]){
                        var result_table_name = result[0].ProductMasterTable;
                        var group_name = result[0].GroupName;
                     client.exec('SELECT DISTINCT "Product" FROM ' + result_table_name + ' where "IsDeleted" =0 ORDER BY "Product" asc', function (error, data) {
                         if (error) { handleError(res, error) }
                        else{
                            return res.status(200).json({ result: data, group_name:group_name});                                           
                            
                        }
                    })

                    }
                     
                }//else
            });//query
        }//else
    });//connect

}
//'SELECT DISTINCT "Manager" FROM ' + result_table_name + ' ORDER BY "Manager" asc
export function getManagerData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let product = req.body.product;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {                    
                    if(result[0]){
                        var result_table_name = result[0].ProductMasterTable;
                        var sqlQuery = '';
                        if(product && product!= 'All'){
                            sqlQuery = 'SELECT DISTINCT "Manager" FROM ' + result_table_name + ' where "IsDeleted" =0 and "Product" =\'' + product + '\' ORDER BY "Manager" asc';
                        }
                        else{
                            sqlQuery = 'SELECT DISTINCT "Manager" FROM ' + result_table_name + ' where "IsDeleted" =0 ORDER BY "Manager" asc';
                        }
                     client.exec(sqlQuery, function (error, managers) {
                         if (error) { handleError(res, error) }
                        else{
                            return res.status(200).json({ managers: managers});                                        
                        }
                    })

                    }
                     
                }//else
            });//query
        }//else
    });//connect

}

export function getProductlData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);

    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;
            let product = req.body.product;
            let manager = req.body.manager;
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {                    
                    if(result[0]){
                        var result_table_name = result[0].ProductMasterTable;
                        var option = "";
                        if(product != 'All' && manager != 'All'){
                            option = ' WHERE "IsDeleted" =0 and "Product"=\'' + product + '\' and "Manager"=\'' + manager + '\'';
                        }
                        if(product != 'All' && manager == 'All'){
                            option = ' WHERE "IsDeleted" =0 and "Product"=\'' + product + '\'';
                        }
                        if(product == 'All' && manager != 'All'){
                            option = ' WHERE "IsDeleted" =0 and "Manager"=\'' + manager + '\'';
                        }
                        if(product == 'All' && manager == 'All'){
                            option = ' WHERE "IsDeleted" =0';
                        }

                        sqlQuery = 'SELECT * FROM ' + result_table_name + option + ' order by "Product" asc';
                        client.exec(sqlQuery, function (error, data) {
                         if (error) { handleError(res, error) }
                         else{
                            client.end();                                                    
                            return res.status(200).json({ result: data});
                        }
                    })

                    }
                     
                }//else
            });//query
        }//else
    });//connect
}

export function addProductData(req, res) {
    try {
        var connString = getConnString();
        var client = new hdb.createClient(connString);
        client.connect(function (err) {
            if (err) { handleError(res, err) }
        else {
            let WebGroup = req.body.WebGroup;           
            let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
            client.exec(sqlQuery, function (err, result) {
            var SAPTable = result[0].ProductMasterTable;
            let MaterialDescription = (!req.body.row.MaterialDescription)? '' : req.body.row.MaterialDescription.toString();
            let MaterialCode = (!req.body.row.MaterialCode)? '' : req.body.row.MaterialCode.toString();
            let AlternateMaterialCode = (!req.body.row.AlternateMaterialCode)? '' : req.body.row.AlternateMaterialCode.toString();
            let Product = (!req.body.row.Product)? '' : req.body.row.Product.toString();
            let Manager = (!req.body.row.Manager)? '' : req.body.row.Manager.toString();
            let SampleConverted = (!req.body.row.SampleConverted)? null : req.body.row.SampleConverted;
            let Username = (!req.body.username)? '' : req.body.username.toString();
            try{

                
                    client.connect(function (err) {                        
                        client.prepare('call "ZP_ANALYTICS"."ZPINVESTIGATOR::FORECAST_PRODUCTMASTER_SAVE"(?,?,?,?,?,?,?,?)', function (err, statement) {
                            if (err) {
                                return console.error('Prepare error:', err);
                            }
                            statement.exec([SAPTable, MaterialDescription, MaterialCode, AlternateMaterialCode, Product, Manager, SampleConverted, Username],
                                function (err, parameters, dummyRows, tableRows) {
                                    if (err) {
                                        client.end();
                                        console.error('Exec error:', err)
                                        return res.status(200).json({ error: err });
                                    }
                                    client.end();
                                    return res.status(200).json({ result: 'Success' })
                                });
                        });
                    });//connect
                } catch (err){
                    Sentry.captureException(err);
                }
            });
        }
        
        });//connect
    }
    catch (err) {
        return res.status(400).json({ error: err.message });
    }
}

export function editProductData(req, res) {
    try {
        var connString = getConnString();
        var client = new hdb.createClient(connString);
        client.connect(function (err) {
            if (err) { handleError(res, err) }
            else {
                let WebGroup = req.body.WebGroup;           
                let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
                client.exec(sqlQuery, function (err, result) {
                var SAPTable = result[0].ProductMasterTable;

                    req.body.rows.forEach(row => {
                        let MaterialDescription = (!row.MaterialDescription)? '' : row.MaterialDescription;
                        let MaterialCode = (!row.MaterialCode)? '' : row.MaterialCode;
                        let AlternateMaterialCode = (!row.AlternateMaterialCode)? '' : row.AlternateMaterialCode;
                        let Product = (!row.Product)? '' : row.Product;
                        let Manager = (!row.Manager)? '' : row.Manager;
                        let SampleConverted = (!row.SampleConverted)? null : row.SampleConverted;
                        let Username = (!row.Username)? '' : row.Username;

                        try{
                                client.connect(function (err) {
                                    client.prepare('call "ZP_ANALYTICS"."ZPINVESTIGATOR::FORECAST_PRODUCTMASTER_UPDATE"(?,?,?,?,?,?,?,?)', function (err, statement) {
                                        if (err) {
                                            return console.error('Prepare error:', err);
                                        }
                                        statement.exec([SAPTable, MaterialDescription, MaterialCode, AlternateMaterialCode, Product, Manager, SampleConverted, Username],
                                            function (err, parameters, dummyRows, tableRows) {
                                                if (err) {
                                                    client.end();
                                                    console.error('Exec error:', err)
                                                    return res.status(200).json({ error: err });
                                                }
                                                
                                            });
                                    });
                                });//connect
                            } catch (err){
                                Sentry.captureException(err);
                            }
                        //------------------------------
                
                })
        return res.status(200).json({ result: 'Success' })
        });
   
    }
    });//connect
    }
    catch (err) {
        return res.status(400).json({ error: err.message });
    }
}

export function deleteProduct(req, res) {
    try {
        var connString = getConnString();
        var client = new hdb.createClient(connString);
        client.connect(function (err) {
            if (err) { handleError(res, err) }
            else {
                let WebGroup = req.body.WebGroup;           
                let sqlQuery = 'SELECT DISTINCT * FROM ZP_ANALYTICS.ZPINVESTIGATOR_FORECAST where "ZP_ANALYTICS"."ZPINVESTIGATOR_FORECAST"."WebGroup"=\'' + WebGroup + '\'' ;
                client.exec(sqlQuery, function (err, result) {
                var SAPTable = result[0].ProductMasterTable;

                let MaterialDescription = (!req.body.row.MaterialDescription)? '' : req.body.row.MaterialDescription;
                let MaterialCode = (!req.body.row.MaterialCode)? '' : req.body.row.MaterialCode;
                let AlternateMaterialCode = (!req.body.row.AlternateMaterialCode)? '' : req.body.row.AlternateMaterialCode;
                let Product = (!req.body.row.Product)? '' : req.body.row.Product;
                let Manager = (!req.body.row.Manager)? '' : req.body.row.Manager;
                let SampleConverted = (!req.body.row.SampleConverted)? null : req.body.row.SampleConverted;
                let Username = (!req.body.row.Username)? '' : req.body.row.Username;
                try{
                        client.connect(function (err) {
                            client.prepare('call "ZP_ANALYTICS"."ZPINVESTIGATOR::FORECAST_PRODUCTMASTER_DELETE"(?,?,?,?,?,?,?,?)', function (err, statement) {
                                if (err) {
                                    return console.error('Prepare error:', err);
                                }
                                statement.exec([SAPTable, MaterialDescription, MaterialCode, AlternateMaterialCode, Product, Manager, SampleConverted, Username],
                                    function (err, parameters, dummyRows, tableRows) {
                                        if (err) {
                                            client.end();
                                            console.error('Exec error:', err)
                                            return res.status(200).json({ error: err });
                                        }
                                        client.end();
                                        return res.status(200).json({ result: 'Success' })
                                    });
                            });
                        });//connect
                    } catch (err){
                        Sentry.captureException(err);
                    }
            });
        }
        });//connect
    }
    catch (err) {
        return res.status(400).json({ error: err.message });
    }
}

function validateString(str)
{
    if (str == null || str == "")
    {
        return false;
    }
    else{
        return true;
    }
}

function getConnString() {
    var port = '32415';
    var host = 'zpsgbdphanadb';
    var user = 'ZP_CSM';
    var pwd = 'ZPcsmhana02';

    var connString = { host: host, port: port, user: user, password: pwd };
    return connString;
}


function handleError(res, err) {
    return res.status(500).json(err);
}



//Simple version, without validation or sanitation
export function test(req, res) {
    res.send('Greetings from the Investogator controller!');
};