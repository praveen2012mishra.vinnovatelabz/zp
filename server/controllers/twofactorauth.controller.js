'use strict';

import User from '../models/user.model';

var http = require("https");
var speakeasy = require('speakeasy');
var QRCode = require('qrcode');


export function getQRCode(req, res) {

  var user_name = req.body.username;

  User.findOne({
    name: user_name
  }, function (err, user) {
    if (err) {
      return res.redirect('https://analytics.zuelligpharma.com');
    } else {

      if (user.secret && user.otp_auth_url) {
        QRCode.toDataURL(user.otp_auth_url, function (err, image_data) {
          return res.json({ qr_uri: image_data, authorized: true });
        });
      } else {
        var secret = speakeasy.generateSecret({ length: 20 });
        var display_secret = secret.base32;
        var otp_url = "otpauth://totp/SecretKey?secret=" + display_secret + "&issuer=analytics.zuelligpharma.com";

        User.update({ name: user_name }, { secret: display_secret, otp_auth_url: otp_url }, { multi: true }, function (err, numberAffected) {
          if (err) {
            console.log(err);
          }
          else {
            QRCode.toDataURL(otp_url, function (err, image_data) {
              return res.json({ qr_uri: image_data, authorized: false });
            });
          }
        });

      }
    }
  })

}


export function verifyToken(req, res) {
  var user_name = req.body.username;
  var user_token = req.body.token;

  User.findOne({
    name: user_name
  }, function (err, user) {
    if (err) {
      return res.send("failure");
    } else {
      if (user.secret) {
        var secret = user.secret;
        var verified = speakeasy.totp.verify({
          secret: secret,
          encoding: 'base32',
          token: user_token
        });

        if (verified) {
          return res.status(200).json({ authorized: true , msg:"Otp matched" });
        }
        else {
          return res.status(200).json({ authorized: false , msg:"Otp not matched"});
        }
      } else {
        return res.status(200).json({ authorized: false, msg: "2FA not enabled." });
      }
    }
  })
}








