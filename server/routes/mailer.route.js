const express = require('express');
const router = express.Router();
const hsts = require('hsts');

const mailer_controller = require("../controllers/mailer.controller");

router.use(hsts({
    maxAge: 15552000,  // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true
  }))
  router.get('/test', mailer_controller.test);
router.post('/sendInfo', mailer_controller.sendInformation);
router.post("/sendAuto", mailer_controller.automatedMail);
module.exports = router;
