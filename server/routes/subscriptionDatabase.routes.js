const express = require('express');
const router = express.Router();

const subscriptionDatabase_controller = require('../controllers/subscriptionDatabase.controller');

router.post('/subsDatabase', subscriptionDatabase_controller.getSubscriptionData);
router.get('/test', subscriptionDatabase_controller.test);

module.exports = router;