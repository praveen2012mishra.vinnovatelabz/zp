const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const investigator_controller = require('../controllers/investigator.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', investigator_controller.test);

router.post('/getMaterial', investigator_controller.getMaterial);
router.post('/getCurrentMonthList', investigator_controller.getCurrentMonthList);
router.post('/getProductList', investigator_controller.getProductList);
router.post('/getOrderMaterialData', investigator_controller.getOrderMaterialData);
router.post('/getGroupSummaryData', investigator_controller.getGroupSummaryData);
router.post('/getGroupData', investigator_controller.getGroupData);
router.post('/getBoardData', investigator_controller.getBoardData);
router.post('/getMaterialData', investigator_controller.getMaterialData);
router.post('/getTableData', investigator_controller.getTableData);
router.post('/getTableDataForForecast', investigator_controller.getTableDataForForecast);
router.post('/getRefreshForecast', investigator_controller.getRefreshForecast);
router.post('/editTableData', investigator_controller.editTableData);
router.post('/editRestrictionData', investigator_controller.editRestrictionData);
router.post('/getInvestigatorMaterialData', investigator_controller.getInvestigatorMaterialData);
router.post('/getPrincipalData', investigator_controller.getPrincipalData);
router.post('/getProductMaterialData', investigator_controller.getProductMaterialData);
router.post('/getManagerData', investigator_controller.getManagerData);
router.post('/getProductlData', investigator_controller.getProductlData);
router.post('/addProductData', investigator_controller.addProductData);
router.post('/editProductData', investigator_controller.editProductData);
router.post('/deleteProduct', investigator_controller.deleteProduct);


module.exports = router;