const express = require("express")
const router = express.Router()

// Require the controllers WHICH WE DID NOT CREATE YET!!
const user_controller = require("../controllers/user.controller")

// a simple test url to check that all of our files are communicating correctly.
router.get("/test", user_controller.test)
router.get("/getUserProfile/:userName", user_controller.getUserProfileByName)
router.post("/updateUserProfile", user_controller.updateUserProfile)
router.post("/saveUserRoles", user_controller.saveUserRoles)
module.exports = router
