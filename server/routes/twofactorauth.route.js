const express = require('express');
const router = express.Router();
const hsts = require('hsts');
router.use(hsts({
    maxAge: 15552000,  // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true
  }))
// Require the controllers WHICH WE DID NOT CREATE YET!!
const two_factor_auth_controller = require('../controllers/twofactorauth.controller');


// a simple test url to check that all of our files are communicating correctly.
// Routes for new QR code based 2fa
router.post('/getQRCode', two_factor_auth_controller.getQRCode);
router.post('/verifyToken', two_factor_auth_controller.verifyToken)

module.exports = router;