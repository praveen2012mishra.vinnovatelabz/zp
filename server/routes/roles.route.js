const express = require("express")
const router = express.Router()
const hsts = require("hsts")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
// Require the controllers WHICH WE DID NOT CREATE YET!!
const role_controller = require("../controllers/roleManagement.controller")

// a simple test url to check that all of our files are communicating correctly.
router.get("/getRolesBySolution/:solution", role_controller.getRolesBySolution)
router.post("/updateSolutionRoles", role_controller.saveRoles)

module.exports = router
