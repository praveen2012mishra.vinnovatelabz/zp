const express = require("express")
const router = express.Router()
import authenticateToken from "../auth/jwt/index"
const hsts = require("hsts")
const supplyChainController = require("../controllers/supplychain.controller")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
router.get(
  "/getMaterialDesc/:limit",
  supplyChainController.getMaterialDescription
)
router.get(
  "/getDataByMaterialDescription/:code",
  supplyChainController.getDataByMaterialDescription
)
router.get("/getBrands", supplyChainController.getBrands)
router.get("/getMonths", supplyChainController.getMonths)
router.get(
  "/getDataByCurrentMonth/:date/:limit",
  supplyChainController.getDataByCurrentMonth
)
router.post("/getFilterData/:limit", supplyChainController.getFilterData)

router.get(
  "/getInventoryPreview/:code",
  supplyChainController.getInventoryPreview
)
router.post("/updateInventory", supplyChainController.updateInventory)

module.exports = router
