'use strict';
var saml2 = require('saml2-js');
var fs = require('fs');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const request = require('request');
const url = require('url');
const config= require('../../config/environment/index')
import User from "../../models/user.model"
import Cookie from "js-cookie"
var jwt = require('jsonwebtoken');
const ActivityLog=require("../../models/activitylogger.model")
//cors error fix
var cors = require('cors')
var corsOptions = {
  origin: 'https://analytics.zuelligpharma.com',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

var router = express.Router()
app.use(express.urlencoded({
  extended: true
}));

// Create service provider
var sp_options = {
  entity_id: "analytics.zuelligpharma.com",
  private_key: fs.readFileSync('server/auth/saml/certificates/analytics.zuelligpharma.com.key').toString(),
  certificate: fs.readFileSync('server/auth/saml/certificates/analytics.zuelligpharma.com.cert').toString(),
  assert_endpoint: "https://analytics.zuelligpharma.com/api/saml/postResponse",
  force_authn: true,
  sign_get_request: true
};
var sp = new saml2.ServiceProvider(sp_options);

var idp_options = {
  sso_login_url: "https://sso.zuelligpharma.com/adfs/ls/",
  sso_logout_url: "https://sso.zuelligpharma.com/adfs/ls/",
  certificates: fs.readFileSync('server/auth/saml/certificates/sso-adfs.cert').toString(),
  sign_get_request: true,
  allow_unencrypted_assertion: true
};
var idp = new saml2.IdentityProvider(idp_options);
// ------ Define express endpoints ------
router
  .get("/metadata.xml", function (req, res) {
    res.type('application/xml');
    res.send(sp.create_metadata());
  })
  .get('/', function (req, res) {

    sp.create_login_request_url(idp, {}, function (err, login_url, request_id) {
      if (err != null) {
        console.log(err);
        return res.send(500);
      }
      // res.clearCookie("saml_response");
      console.log("no error man");
      return res.json({ url: login_url, requestid: request_id });
      // res.redirect(login_url)
    });
  })
  .post("/postResponse", async function (req, res) {
    let options = { request_body: req.body, allow_unencripted_assertion: true };
    sp.post_assert(idp, options, function (err, saml_response) {
      if (err != null) {
        console.log(err, "____________ERROR");
        return res.send(500);
      }
      let name_id = saml_response.user.name_id;
      let session_index = saml_response.user.session_index;
      let in_response= saml_response.response_header.in_response_to
      var token = jwt.sign(saml_response,config.secrets.ACCESS_TOKEN_SECRET);
      console.log(saml_response)
      let cookieOptions={
          maxAge: 1000 * 60 * 60 * 2, // would expire after 2 days
      }
      res.cookie("saml_response",{...saml_response,token:token},cookieOptions)
      request(`${config.api}/session/checkSSOStatus/` + name_id, { 'auth':{ 'bearer':`${token}`}}, (errp, respo, body) => {
        if (!errp) {
		console.log("called");
          if (respo.body == 'enforced') {
            User.findOne({
              name: name_id
            }, function (err, user) {
              if (err) {
                res.clearCookie("saml_response");
                res.Cookie('error_reason',{reason:'Already Logged in'},{maxAge:4000})
                return res.redirect('https://analytics.zuelligpharma.com/');
              }
              else if (user !== null && user.active) // This indicates that user is logged in somewhere
              {
                var storedTime = user.session_initiated;
                var currentTime = new Date();
                if (storedTime) {
                  var seconds = (currentTime.getTime() - storedTime.getTime()) / 1000;
                  if (seconds > 1800) // Allow user to login despite of active status
                  {
                    user.set({ session: session_index })
                    user.set({ in_response: in_response })
                    user.set({ active: true })
                    user.set({ session_initiated: new Date(new Date().toUTCString()) })


                    user.save(function (err, updatedUser) {
                      if (err){
                        res.clearCookie("saml_response");
                        res.Cookie('error_reason',{reason:'Already Logged in'},{maxAge:1000*60*3})
                        return res.redirect('https://analytics.zuelligpharma.com');
                      }
                      else
                        return res.redirect('https://analytics.zuelligpharma.com/');
                    });
                  }
                  else {
                    res.clearCookie("saml_response");
                    res.redirect('https://analytics.zuelligpharma.com')
                  }
                }
                else {
                  res.clearCookie("saml_response");
                  res.redirect('https://analytics.zuelligpharma.com')
                }
              }
              else if (user !== null) {
                user.set({ session: session_index })
                user.set({ in_response: in_response })
                user.set({ active: true })
                user.set({ session_initiated: new Date(new Date().toUTCString()) })
                
                user.save(function (err, updatedUser) {
                  if (err){
                    res.clearCookie("saml_response");
                    return res.redirect('https://analytics.zuelligpharma.com');
                  }
                  else
                    return res.redirect('https://analytics.zuelligpharma.com');
                });

              } else {
                var newUser = new User({
                  name: name_id,
                  provider: 'activedirectory',
                  session: session_index,
                  in_response: in_response,
                  active: true,
                  session_initiated: new Date(new Date().toUTCString())
                })

                newUser.save(
                  function (err) {
                    if (err) {
                      res.clearCookie("saml_response");
                      return res.redirect('https://analytics.zuelligpharma.com');
                    }
                    return res.redirect('https://analytics.zuelligpharma.com/is_saml');
                  })
              }
              // new api call to be ended
            })
          }
          else {    // If user does not have SSO activated
            User.findOne({
              name: name_id
            }, function (err, user) {
              if (err) {
                res.clearCookie("saml_response");
                return res.redirect('https://analytics.zuelligpharma.com');
              }
              else if (user !== null) {
                user.set({ session: session_index })
                user.set({ in_response: in_response })
                user.set({ active: true })
                user.set({ session_initiated: new Date(new Date().toUTCString()) })

                user.save(function (err, updatedUser) {
                  if (err){
                    res.clearCookie('saml_response')
                    return res.redirect('https://analytics.zuelligpharma.com');}
                  else
                    return res.redirect('https://analytics.zuelligpharma.com');
                });
              }
              else {
                var newUser = new User({
                  name: name_id,
                  provider: 'activedirectory',
                  session: session_index,
                  in_response: in_response,
                  active: true,
                  session_initiated: new Date(new Date().toUTCString())
                })

                newUser.save(
                  function (err) {
                    if (err) {
                      res.clearCookie("saml_response");
                      return res.redirect('https://analytics.zuelligpharma.com');
                    }
                    return res.redirect('https://analytics.zuelligpharma.com');
                  })
              }

            }) // find user end   
          } //SSO check end
        }else console.log(errp)
      })
    });
  })
  .post('/singleLogout', function (req, res) {
    var logout_details = {};
    logout_details.action = "logout";
    logout_details.name = req.body.name;
    // logout_details.IP = req.body.clientIp.ip;
    logout_details.logout_time = new Date(new Date().toUTCString());
    logout_details.provider = "activedirectory";
    logout_details.logout_successful = true;



    var options = { name_id: req.body.name, session_index: req.body.session }
    sp.create_logout_request_url(idp, options, function (err, logout_url) {
      if (err !== null)
        res.send(500, err)
      else {
        res.clearCookie("saml_response");
        // The following part will store the logout details as log in database
        User.update({
          name: req.body.name
        }, { active: false }, function (error, db_res) {
          if (!error) {
            if (!req.body.status.includes("2FA failed")) {
              // Connect to the db
              var activityLog = new ActivityLog(logout_details);
              activityLog.save();
            } else {
              ActivityLog.findOneAndUpdate({ name: req.body.name, action: 'login' }, { $set: { login_successful: false, reason: '2FA not passed' } }, { new: true, sort: { 'login_time': -1 } }, function (er, docu) {
                console.log("Unsuccessful 2FA stored into logs");
              })

            }
          }
        })
        return res.json({ url: logout_url,redirect:"https://analytics.zuelligpharma.com/" });
      }
    })
  })
  .get('/logout', function (req, res) {
    console.log("came back from ADFS. LOGGED OUT !")

    var options = { in_response_to: req.body.in_resp }
    sp.create_logout_response_url(idp, options, function (err, logout_url) {
      if (err !== null)
        res.send(500, err)
      else {
        console.log(err)
        // console.log(logout_url)
        res.clearCookie("saml_response")
        return res.json({ url: logout_url,redirect:'https://analytics.zuelligpharma.com/' })
      }
    })
  })


module.exports = router;
