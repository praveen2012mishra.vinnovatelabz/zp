"use strict"
/*eslint no-process-env:0*/

import path from "path"
import _ from "lodash"

/*function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}*/

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(`${__dirname}/../../..`),

  // dev client port
  clientPort: process.env.CLIENT_PORT || 3000,

  // Server port
  port: process.env.PORT || 9000,

  // Server IP
  ip: process.env.IP || "0.0.0.0",

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: "ng5template-secret",
    ACCESS_TOKEN_SECRET: "vlorbyrvkzhn3mrgzyijadtupzojvvlk4tioon4fykhzvclohjqq",
  },

  // MongoDB connection options
  mongo: {
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    },
  },

  google: {
    clientID: process.env.GOOGLE_ID || "id",
    clientSecret: process.env.GOOGLE_SECRET || "secret",
    callbackURL: `${process.env.DOMAIN || ""}/auth/google/callback`,
  },

  ldap: {
    ldaphost: "192.168.34.150", //LDAP needs the port. AD doesn't need the port
    ldapuser: "svczolplus", //only used for AD
    ldappwd: "S^46wc,ub{@Ec^E",
    ldapcn: "VNZP-Zscaler Internet Access Standard", //only used in AD
    ldapbasedn: "DC=zuelligpharma,DC=interpharma,DC=local", // used in both LDAP and AD
    // ldapbinddn: 'uid=admin,ou=people,dc=hadoop,dc=apache,dc=org' //only used for LDAP

    // ldaphost: 'hdp2.qubida.io:33389', //LDAP needs the port. AD doesn't need the port
    // ldapuser: 'admin', //only used for AD
    // ldappwd: 'admin-password',
    // ldapcn: 'Admin', //only used in AD
    // ldapbasedn: 'dc=hadoop,dc=apache,dc=org', // used in both LDAP and AD
    // ldapbinddn: 'uid=admin,ou=people,dc=hadoop,dc=apache,dc=org' //only used for LDAP

    // ldaphost: '172.16.165.130',
    // ldapuser: 'qubida-ldap-user',
    // ldappwd: 'Welcome1!',
    // ldapcn: 'HDP24-02-GROUP',
    // ldapbasedn: 'OU=HDP24-02,DC=hdp24,DC=dcdomain'

    internal: {url:'ldap://azisgadc07.zuelligpharma.interpharma.local:389',
    baseDN: 'dc=zuelligpharma,dc=interpharma,dc=local',
    username: 'svcZIPInternal@zuelligpharma.interpharma.local',
    password: 'FE`C)^n/:8h!((jQ'},

    external:{ url: 'ldap://azisgextadc02.external.interpharma.local:389',
    baseDN: 'DC=external,DC=interpharma,DC=local',
    username: 'svcZIPExternal@external.interpharma.local',
    password: '[>dF^d}9e`WtcJw{' }

  },
  

  tableau: {
    // internalurl: 'https://192.168.34.16',
    internalurl: "https://192.168.34.16",
    externalurl: "https://tradeinfrared.zuelligpharma.com",
  },
  // Zuellig Mail API
  zuelligMailer: {
    //api: 'https://l6088-iflmap.hcisbp.ap1.hana.ondemand.com/http/email/trigger/v1',
    //user: 'info_zip@zuelligpharma.com',
    user: "no-reply-CSM@zuelligpharma.com",
    authorization: "Basic UzAwMTYwOTMxNzc6RGFuaWVsMSE=",
    // send_from ="no-reply-CSM@zuelligpharma.com",
    //subject = "[alert][server][insidermail]" + subject,
    api:
      "https://l6088-iflmap.hcisbp.ap1.hana.ondemand.com/http/automatedemail/trigger/v1",
    //headers = { "Content-Type": "text/html", "Authorization": "Basic UzAwMTYwOTMxNzc6RGFuaWVsMSE=", "sender": send_from, "receiver": sendmailto, "subject": subject, "bodytext": bodytext }
  },
  // Nodemailer Settings
  nodemailer: {
    service: "Gmail",
    user: "info@qubida.com",
    pass: "Qubida15",
  },

  //zptestserver - tab04
  /*postgres: {
        server: '10.10.22.84',
        port: 8060,
        username: 'readonly',
        pwd: 'Password123',
        database: 'workgroup'
    }*/

  //zplive - tab03
  postgres: {
    server: "10.10.22.69",
    port: 8060,
    username: "readonly",
    pwd: "Password123",
    database: "workgroup",
  },
  //API
  api: "https://analytics.zuelligpharma.com/api",
  /*
    //zp korea
    postgres3: {
        server: '192.168.35.91',
        port: 8060,
        username: 'readonly',
        pwd: 'Password123',
        database: 'workgroup'
    },

    //tradeinfrared - tab01
    postgres4: {
        server: '192.168.34.16',
        port: 8060,
        username: 'readonly',
        pwd: 'TFw#36428',
        database: 'workgroup'
    }
    */
}

// Export the config object based on the NODE_ENV
// ==============================================

module.exports =
  _.merge(all, require("./shared"), require(`./${process.env.NODE_ENV}.js`)) ||
  {}
