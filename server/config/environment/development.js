"use strict"
/*eslint no-process-env:0*/

// Development specific configuration
// ==================================
console.log(process.env.MONGODB_URI_DEV)
module.exports = {
  mongo: {
    uri: process.env.MONGODB_URI_DEV,
  },
  twofactorauth: {
    authheader: "Basic c211a2hlcmplZTpEYW5pZWwzIQ==",
    apiUrl: "zpmt.zuelligpharma.com",
  },

  // Seed database on startup
  seedDB: true,
}
