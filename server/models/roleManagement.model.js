const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let roleManagementSchema = new Schema({
    solution:{
        type: String,
        index: {
            unique: true,
        }
    },
    modules:Array,
    options:Array,
    roles:Array
    
}, { collection: 'roleManagement', timestamps: true, versionKey: false });

module.exports = mongoose.model('roleManagement', roleManagementSchema);