'use strict';

import mongoose, { Schema } from 'mongoose';

var SystemLoggerSchema = new Schema({
	time: Date,
        details: Schema.Types.Mixed
}, { collection: 'SystemLogs', strict: false });

  
  
module.exports = mongoose.model('SystemLog', SystemLoggerSchema);