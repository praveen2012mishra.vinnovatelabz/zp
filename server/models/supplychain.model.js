import mongoose, { Schema } from "mongoose"

const SupplyChain = new Schema(
  {
    Product: String,
    SafetyStock: [{ key: String, value: Number }],
    InventoryHealth: String,
    ModelParameter: {
      ForecastGranularity: [],
      ForecastHorizon: [],
      ReplenishModel: [],
    },
    SafetyStockTime: String,
    PrincipalCode: String,
    MaterialCode: String,
    HistoricalInventoryFlow: {
      LastUpdatedTime: String,
      BeginningInventory: [{ key: String, value: Number }],
      EndingInventory: [{ key: String, value: Number }],
      Outgoing: {
        ActualSales: [],
        OthersOutflow: {
          ExpiredStock: [{ key: String, value: Number }],
          Scrap: [{ key: String, value: Number }],
          RTPWriteOff: [{ key: String, value: Number }],
        },
      },
      Incoming: {
        GoodsReceived: [{ key: String, value: Number }],
        PendingStock: [{ key: String, value: Number }],
      },
    },
    isDeleted: Boolean,
    Manager: String,
    MaterialDescription: String,
    ForecastInventoryFlow: [
      {
        CreatedBy: String,
        CreatorRole: String,
        Outgoing: {
          ModelForecast: [{ key: String, value: Number }],
          ConsensusForecast: [],
          ForecastAdjustment: [{ key: String, value: Number }],
        },
        MonthRun: String,
        Incoming: {
          SuggestedPO: [{ key: String, value: Number }],
          PlannedPO: [],
        },
        ApprovalStatus: String,
        Approver: String,
        CreatedOn: String,
        UpdatedOn: String,
        BeginningInventory: [{ key: String, value: Number }],
        EndingInventory: [{ key: String, value: Number }]
      },
    ],
    PrincipalName: String,
    LeadTime: String,
    MOQ: Number,
    ApprovalStatus: String,
  },
  { collection: "InventoryPlanner", timestamps: true, versionKey: false }
)

module.exports = mongoose.model("InventoryPlanner", SupplyChain)
