"use strict"

import mongoose, { Schema } from "mongoose"

var GroupSchema = new Schema(
  {
    group_name: String,
    mappingId: String,
    type: String,
    country: String,
    forecasting: Boolean,
    optimization: Boolean,
    segmentation: Boolean,
    single_session_enforced: Boolean,
    two_factor_authentication_enforced: Boolean,
    ui_elements_list: [{ name: String, url: String }],
    options: Array,
  },
  { collection: "group", timestamps: true, versionKey: false }
)

module.exports = mongoose.model("Group", GroupSchema)
