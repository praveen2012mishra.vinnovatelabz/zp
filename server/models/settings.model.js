'use strict';

import mongoose, { Schema } from 'mongoose';


var SettingsSchema = new Schema({
  maintenance_message: String,
  maintenance_mode_status: Boolean,
  tableau_service_username: String,
  tableau_service_password: String
}, { collection: 'Settings', timestamps: true, versionKey: false });



module.exports = mongoose.model('Settings', SettingsSchema);