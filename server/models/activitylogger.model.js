'use strict';

import mongoose, { Schema } from 'mongoose';

var ActivityLoggerSchema = new Schema({
	action: String,
        name: String,
        IP: String,
        login_time: Date,
        logout_time: Date,
        provider: String,
        login_successful: Boolean,
        logout_successful: Boolean,
        reason: String,
        data: Schema.Types.Mixed,
        updated_by: String,
        added_by: String,
        deleted_by: String,
        updation_time: String,
        insertion_time: String,
        deletion_time: String,
        group_name: String
        
        


}, { collection: 'ActivityLogs'},{strict: false });

  
module.exports = mongoose.model('ActivityLog', ActivityLoggerSchema);
