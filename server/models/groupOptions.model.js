"use strict"

import mongoose, { Schema } from "mongoose"
const DatabaseLog = require("../models/databaselogger.model")

var groupOptionSchema = new Schema(
  {
    group_name: String,
    mappingId: String,
    type: String,
    country: String,
    options: Array,
    solutions: Array,
  },
  { collection: "groupOptions", timestamps: true, versionKey: false }
)

groupOptionSchema.post("save", function (next) {
  var log_record = {}
  log_record.collection_name = "groupOptions"
  log_record.operation = "update"
  log_record.data_modified = JSON.stringify(this)
  log_record.timestamp = new Date(new Date().toUTCString())

  var log_object = new DatabaseLog(log_record)
  log_object.save()
})

module.exports = mongoose.model("groupOptions", groupOptionSchema)
