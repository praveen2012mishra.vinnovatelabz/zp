// Loading environment variables
const result = require("dotenv").config()
var cors = require("cors")
import express from "express"
const bodyParser = require("body-parser")
const pino = require("express-pino-logger")()
import mongoose from "mongoose"
import registerRoutes from "./routes"
const config = require("./config/environment")
mongoose.Promise = require("bluebird")

const PORT = config.port

// import config from './config/environment';
const app = express()

// Set up mongoose connection
// const mongoose = require('mongoose');
mongoose.connect(config.mongo.uri, config.mongo.options)

mongoose.connection.on("error", function (err) {
  console.error(`MongoDB connection error: ${err}`)
  process.exit(-1) // eslint-disable-line no-process-exit
})

mongoose.connection.once("open", function () {
  console.log("Connected to Mongodb")
  app.emit("ready")
})

app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }))
app.use(bodyParser.json({ limit: "10mb", extended: true })) // to parse application/json
app.use(pino)
// Specifying API routes
registerRoutes(app)

app.on("ready", function () {
  app.listen(PORT, () =>
    console.log(`Express server is running on localhost:${PORT}`)
  )
})
