"use strict";

module.exports = {
  siteMetadata: {
    title: "ZP-Project",
    description: "ZP-Project",
    author: "@cbnits"
  },
  plugins: ["gatsby-plugin-react-helmet", {
    resolve: "gatsby-source-filesystem",
    options: {
      name: "images",
      path: "".concat(__dirname, "/src/images")
    }
  }, "gatsby-plugin-sharp", "gatsby-transformer-sharp", {
    resolve: "gatsby-plugin-manifest",
    options: {
      icon: "src/images/Logomark.png"
    }
  }, {
    resolve: "gatsby-plugin-sass"
  }, {
    resolve: "gatsby-plugin-svgr-svgo",
    options: {
      urlSvgOptions: [{
        test: /\.svg$/
      }]
    }
  }, {
    resolve: "gatsby-plugin-layout",
    options: {
      component: require.resolve("./src/layouts")
    }
  }, {
    resolve: "gatsby-source-graphql",
    options: {
      typeName: "WPGraphQL",
      fieldName: "wpgraphql",
      url: "https://ezpz.zuelligpharma.com/graphql"
    }
  }],
  proxy: {
    prefix: "/api",
    url: "http://localhost:3050"
  }
};