import React, { useState, useEffect } from "react"
import { NavLink } from "reactstrap"
import Styles from "./style.module.scss"
import { FaCaretDown } from "react-icons/fa"
import axios from "../../../../../axiosConfig"
import { connect } from "react-redux"
import Cookies from "js-cookie"
import { setUserGroupName } from "../../../../services/Redux/actions/useGroupAction"
const logout = [{ label: "Sign in", type: null, href: "/Login" }]
function Dropdown({
  authData = null,
  innerOptions = [],
  setSelectedGroupName,
  userGroup,
}) {
  const [toggle, setToggle] = React.useState(false)
  const [options, setOptions] = useState([
    { label: "Sign in", type: null, href: "/Login" },
  ])
  const [innerLabel, setInnerLabel] = useState("")
  const [notificationNumber, setNotificationNumber] = useState("5")
  useEffect(() => {
    if (authData) {
      console.log(userGroup, "hello")
      if (innerOptions.length) {
        if (!Cookies.get("inner_option")) {
          console.log(Cookies.get("inner_option"))
          Cookies.set("inner_option", innerOptions[0], { expires: 1 })
          setOptions([
            { label: `Notifications`, type: null, href: null },
            innerOptions[0],
            { label: "Admin", type: null, href: "/auth/admin" },
            { label: "Profile", type: null, href: "/auth/profile" },
            { label: "Sign Out", type: null, href: null },
          ])
        } else {
          console.log(Cookies.get("inner_option"))
          setOptions([
            { label: `Notifications`, type: null, href: null },
            JSON.parse(Cookies.get("inner_option")),
            { label: "Admin", type: null, href: "/auth/admin" },
            { label: "Profile", type: null, href: "/auth/profile" },
            { label: "Sign Out", type: null, href: null },
          ])
        }
      } else {
        setOptions([
          { label: `Notifications`, type: null, href: null },
          { label: "Admin", type: null, href: "/auth/admin" },
          { label: "Profile", type: null, href: "/auth/profile" },
          { label: "Sign Out", type: null, href: null },
        ])
      }
    } else {
      console.log(authData)
      setOptions([{ label: "Sign in", type: null, href: "/Login" }])
    }
  }, [])
  const handleInnerDropDown = item => {
    // setSelectedGroupName(item)
    Cookies.set("inner_option", item, { expires: 1 })
    setOptions([
      { label: `Notifications`, type: null, href: null },
      item,
      { label: "Admin", type: null, href: "/auth/admin" },
      { label: "Profile", type: null, href: "/auth/profile" },
      { label: "Sign Out", type: null, href: null },
    ])
   window.location.reload();
  }
  return (
    <div className={Styles.dropdownToggle}>
      {options.map((item, key) =>
        item.type != null && item.type === "dropdown" ? (
          <div className={Styles.dropList}>
            <div
              className={Styles.innerToggle}
              onClick={() => setToggle(!toggle)}
              key
            >
              <span style={{ width: "80%", overflowWrap: "break-word" }}>
                {item.label}
              </span>
              <span>
                <FaCaretDown style={{ marginBottom: 3, marginLeft: 5 }} />
              </span>
            </div>
            {toggle ? (
              <div style={{ paddingLeft: 13 }}>
                {innerOptions.map((innerItem, key) => {
                  if (item.id !== innerItem.id)
                    return (
                      <NavLink
                        style={{ paddingTop: 2, overflowWrap: "break-word" }}
                        onClick={() => handleInnerDropDown(innerItem)}
                        key={key}
                      >
                        {innerItem.label}
                      </NavLink>
                    )
                })}
              </div>
            ) : null}
          </div>
        ) : item.label === "Notifications" ? (
          <NavLink href={item.href ? item.href : "#"} key>
            <span className={Styles.notification}>({notificationNumber}) </span>
            {item.label}
          </NavLink>
        ) : (
          <NavLink
            href={item.href ? item.href : "#"}
            onClick={() => {
              if (item.label === "Sign Out") {
                axios
                  .post("/saml/singleLogout", {
                    name: authData.nameId,
                    session: authData.sessionIndex,
                    in_resp: authData.inResponse,
                    status: "not known",
                  })
                  .then(res => {
                    Cookies.remove("inner_option")
                    window.location.href = res.data.redirect
                  })
                  .catch(e => console.log(e))
                  Cookies.remove("2fa")
              }
            }}
            key
          >
            {item.label}
          </NavLink>
        )
      )}
    </div>
  )
}
const mapStateToProps = state => ({
  notification: state.notification,
  userGroup: state.userGroup.name,
})

const mapDispatchToProps = dispatch => ({
  setSelectedGroupName: name => dispatch(setUserGroupName(name)),
})

// export default AdminGroupMgmtTab
export default connect(mapStateToProps, mapDispatchToProps)(Dropdown)
