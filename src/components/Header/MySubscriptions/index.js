import React, { useState } from "react"
import Dropdown from "./admin_dropdown"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
import { FiSearch } from "react-icons/fi"

function MySubscriptions({
  searchBoxDesktop,
  user,
  userHover,
  search,
  searchToggle,
  authData,
  innerOptions,
}) {
  const [toggle, setToggle] = useState(false)
  return (
    <Nav
      onMouseLeave={() => setToggle(false)}
      className={!searchBoxDesktop ? "ml-auto custom-right-navbar wdt" : "hide"}
      navbar
      className="subBack"
      style={{
        position: "relative",
        backgroundColor: toggle ? "#04434B" : "transparent",
        borderBottom: toggle ? "0.8px solid #2d3436" : "none",
      }}
    >
      {toggle && (
        <div onMouseLeave={() => setToggle(false)}>
          <Dropdown authData={authData} innerOptions={innerOptions} />
        </div>
      )}
      <NavItem onMouseEnter={() => setToggle(false)}>
        <NavLink href="/auth/subscription" className="nav-subscription">
          My Subscriptions
        </NavLink>
      </NavItem>
      <NavItem className="nav-icon">
        <NavLink className="user-nav-icon" onMouseEnter={() => setToggle(true)}>
          <img src={user} className="user" />
          <img src={userHover} className="user-hover" />
          <div
            onTouchStart={() => {
              setToggle(!toggle)
            }}
            className="userAccount"
          >
            Account
          </div>
        </NavLink>
      </NavItem>
      <NavItem onMouseEnter={() => setToggle(false)} className="nav-icon">
        {/* <img src={search} className="search" onClick={searchToggle} /> */}
        <FiSearch className="icon" onClick={searchToggle} />
      </NavItem>
    </Nav>
  )
}

export default MySubscriptions
