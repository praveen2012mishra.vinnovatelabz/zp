import React, { Component } from "react"
import {
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
} from "reactstrap";
import { Modal, Button } from "react-bootstrap";
import logo from "../../assets/images/ZHA-Logo.png"
import user from "../../assets/images/Account.png"
import userHover from "../../assets/images/Account-lime.png"
import search from "../../assets/images/Search.png"
import "./style.scss"
import dot from "../../assets/images/dot.png"
import vertDot from "../../assets/images/ZHA Home for dev-26.png"
import arrowDown from "../../assets/images/ZHA Home for dev-27.png"
import arrowUp from "../../assets/images/ZHA Home for dev-28.png"
import plus from "../../assets/images/ZHA Home for dev-29.png"
import minus from "../../assets/images/ZHA Home for dev-30.png"
import ReactDOM from "react-dom"
import ContactModal from "./contactModal"
import Close from "../../assets/images/svg/close.svg"
import limeSearch from "../../assets/images/svg/limeSearch.svg"
import Solutions from "./Solutions"
import DigitalOperations from "./DigitalOperations"
import Search from "./Search"
import About from "./About"
import MySubscriptions from "./MySubscriptions"
import { searchData } from "./data"
import Cookie from "js-cookie"
import cogo from "cogo-toast"
import axios from "../../../axiosConfig"
import { basePath } from "../basePath";
import { twofactor, twofactorOtpVerify, twofactorEnforceCheck } from "../../services/Api/2FA.api";
import { samlLogout } from "../../services/Api/auth.api";
import { isAdmin } from "../../services/authService";
const _ = require("underscore");
class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      getUserName: false,
      signStatus: false,
      modalOpen: false,
      fixedHeader: false,
      isOpen: false,
      selector: {
        commercialExcellence: false,
        supplyChain: false,
        insider: false,
        businessIntelligence: false,
        internal: false,
      },
      innerWidth: window.innerWidth,
      displayType: "",
      dropDownId: "",
      dropDownIdInsight: "",
      selectedLink: "",
      mobileToggle: false,
      searchBoxDesktop: false,
      displaySearchData: [],
      selectedSearchValue: "",
      authData: null,
      samlToken: null,
      innerOptions: [],
      errorReason: "",
      twoFactorResponse: "",
      qrModalOpen: false,
      twoFaToken: ""
    }
    this.inputWidth = React.createRef()
    this.iconDisplayRef = React.createRef()
    this.handleClick = this.handleClick.bind(this)
    // let token=samlCookie?JSON.parse(samlCookie.substr(2,samlCookie.length)):null;
  }

  //On click on get user down arraw
  getUser = event => {
    this.setState({ getUserName: true })
  }

  //On hover in sign element
  onToggleOpen = event => {
    this.setState({ signStatus: true })
  }

  onToggleLeave = event => {
    this.setState({ signStatus: false })
  }

  onUserLeave = event => {
    this.setState({ getUserName: false })
  }

  // for web navigation toggle
  toggle = id => {
    if (window.innerWidth > 992) {
      this.setState({
        isOpen: true,
        dropDownId: id,
      })
    }
  }

  modalToggle = () => {
    this.setState({ modalOpen: true })
  }
  modalClose = () => {
    this.setState({ modalOpen: false })
    this.noDropdownSideBrackets("")
  }

  twofactorAuth = async (name) => {
    let enforced = await twofactorEnforceCheck(name);
    console.log(enforced,"---------------------------TRUE")
    if (enforced) {
      let twoFa = await twofactor(name);
      this.setState({ twoFactorResponse: twoFa });
      console.log("i am qr")
      _.debounce(this.setState({ qrModalOpen: true }), 2000);
    }
  }
  // on scroll header fixed
  componentDidMount() {
    // window.addEventListener("scroll", this.handleScroll)
    window.addEventListener("click", this.handleClickOutside)
    window.addEventListener('scroll', (event) => {
      const top = event.target.scrollingElement.scrollTop;
      document.getElementById('zp-home-header').style.background = `rgba(9, 70, 88, ${top > 200 ? 1 : top * 0.005})`
    })
    if (!this.props.toggle) {
      this.setState({ dropDownId: "" })
    }
    console.log(Cookie.get("error_reason"), "erroor")
    this.setState({
      errorReason: Cookie.get("error_reason") ? Cookie.get("error_reason") : "",
    })
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null

    if (samlToken) {
      console.log(Cookie.get("saml_response"))
      this.setState({
        authData: {
          nameId: samlToken.user.name_id,
          sessionIndex: samlToken.user.session_index,
          inResponse: samlToken.response_header.in_response_to,
        },
      })
      let twoFaCookie= Cookie.get("2fa")?Cookie.get("2fa"): null
      if(twoFaCookie===null)
      { console.log("qr---------------------------------------------")
        this.twofactorAuth(samlToken.user.name_id)
      }

      axios
        .get(`/group/getADGroups/user/${samlToken.user.name_id}`)
        .then(res => {
          let ar = []
          res.data.data.groupnames.map((item, key) => {
            ar.push({
              label: item.group_name,
              id: item._id,
              href: null,
              type: "dropdown",
            })
          })
          console.log('groups map',ar);
          this.setState({ innerOptions: ar })
        })
        .catch(err => {
          console.log(err)
          this.setState({ innerOptions: [] })
        })
    } else {
       console.log(samlToken)
      this.setState({ authData: null })

      // this.setState({
      //   authData: {
      //     nameId: 'skularathna',
      //     sessionIndex:'_d51f49a3-386e-4b53-89b1-7a7df4e1ed08',
      //     inResponse: '_95347bb7ea730ab3b4176f3ef80c907edf881c492c',
      //   },
      // })
    }
  }
  handleClickOutside = e => {
    this.inputWidth && this.inputWidth.current &&
      !this.inputWidth.current.contains(e.target) &&
      this.setState({ displaySearchData: [] })
  }
  noDropdownSideBrackets = id => {
    this.setState({ dropDownIdInsight: id })
    this.setState({ dropDownId: id })
  }
  handleClick(event) {
    const { name } = event.target
    let selectorStatus = this.state.selector
    if (window.innerWidth <= 992) {
      Object.keys(selectorStatus).forEach(function (key) {
        key === name && !selectorStatus[name]
          ? (selectorStatus[key] = true)
          : (selectorStatus[key] = false)
      })
    } else {
      let _this = this
      Object.keys(selectorStatus).forEach(function (key) {
        key === name
          ? (selectorStatus[key] = true)
          : (selectorStatus[key] = false)
      })
    }
    if (selectorStatus[name]) {
      this.setState({
        selector: selectorStatus,
        displayType: name,
      })
    } else {
      this.setState({
        selector: selectorStatus,
        displayType: "",
      })
    }
  }
  innerDropDownItems = e => {
    const { name } = e.target
    this.setState({ selectedLink: name })
  }
  removeDropdownItems = () => {
    this.setState({ selectedLink: "" })
  }
  removeToggle = (e, id) => {
    if (window.innerWidth > 992) {
      let selector = {
        commercialExcellence: false,
        supplyChain: false,
        insider: false,
        businessIntelligence: false,
        internal: false,
      }
      this.setState({
        dropDownId: "",
        isOpen: false,
        selector,
        displayType: "",
      })
    }
  }
  //for mobile navigation toggle
  mobileToggle = () => {
    this.setState({ mobileToggle: !this.state.mobileToggle }, () => {
      if (!this.state.mobileToggle) {
        let selectorStatus = { ...this.state.selector }
        Object.keys(selectorStatus).forEach(function (key) {
          selectorStatus[key] = false
        })
        this.setState({
          displayType: "",
          selector: selectorStatus,
          dropDownId: "",
        })
      }
    })
  }
  mobileToggClick = (e, id) => {
    //if (window.innerWidth <= 1052) {
      if (id === this.state.dropDownId)
        this.setState({ isOpen: false, dropDownId: "" })
      else this.setState({ isOpen: true, dropDownId: id })
    //}
  }
  filterSearch = e => {
    let searchItems = [...searchData]
    let searchValue = []
    if (e.target.value.length >= 2) {
      searchValue = searchItems.filter((a, i) => {
        if (a.label.toLowerCase().includes(e.target.value.toLowerCase())) {
          return a
        } else {
          return false
        }
      })
      if (!searchValue.length) {
        searchValue = [{ label: "No Data Found", url: "#", clickable: false }]
      }
    }

    this.setState({
      displaySearchData: searchValue,
      selectedSearchValue: e.target.value,
    })
  }
  searchToggle = () => {
    this.setState({
      searchBoxDesktop: !this.state.searchBoxDesktop,
      displaySearchData: [],
      selectedSearchValue: "",
    })
  }
  handleClose = () => this.setState({ qrModalOpen: false });
  handelChange = (e) => {
    this.setState({ twoFaToken: e.target.value })
  }
  submitOtp = async () => {
    let otpResponse = await twofactorOtpVerify(this.state.authData.nameId, this.state.twoFaToken);
    if (otpResponse.authorized) {
	console.log(otpResponse.authorized);
      // isAdmin(true)
      Cookie.set("2fa",{authorized:true},{expires:1})
      this.setState({ qrModalOpen: false });
    } else {
	console.log(otpResponse.authorized);
      samlLogout("2FA", this.state.authData)
    }
  }
  render() {
    if (this.state.searchBoxDesktop) {
      this.inputWidth.current.style.width = "16em"
      this.inputWidth.current.style.transition = "width 1s"
      this.inputWidth.current.style.padding = "0 0 0 10px"
      this.iconDisplayRef.current.style.visibility = "visible"
      this.iconDisplayRef.current.style.opacity = 1
    } else {
      if (this.inputWidth.current) {
        this.inputWidth.current.style.width = "0"
        this.inputWidth.current.style.padding = "0"
      }
      if (this.iconDisplayRef.current) {
        this.iconDisplayRef.current.style.visibility = "hidden"
        this.iconDisplayRef.current.style.opacity = 0
        this.iconDisplayRef.current.style.transition =
          "visibility 0.5s, opacity 0.5s linear"
      }
    }
    return (
      <div id='zp-home-header'>
        {this.state.errorReason !== ""
          ? cogo.error(this.state.errorReason)
          : null}
        <Modal show={this.state.qrModalOpen}>
          <Modal.Header closeButton>
            <Modal.Title>2FA QRcode</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img src={this.state.twoFactorResponse ? this.state.twoFactorResponse.qr_uri : ""} />
            <input type="text" onChange={this.handelChange}></input>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.submitOtp}>
              Submit
              </Button>
          </Modal.Footer>
        </Modal>
        <Navbar
          className={
            this.state.fixedHeader
              ? "custom-navbar fixed-navbar"
              : "custom-navbar"
          }
          style={{
            backgroundColor:
              this.state.isOpen ||
                this.state.fixedHeader ||
                this.state.mobileToggle ||
                this.props.urlRoute === "/insights/" ||
                this.props.urlRoute === "/ReadArticle/"
                ? "#094658"
                : "transparent",
          }}
          light
          expand="lg"
        >
          <NavbarBrand href="/">
            <img src={logo} className="w-100" alt="logo" />
          </NavbarBrand>
          <img
            src={this.state.mobileToggle ? dot : vertDot}
            className={
              this.state.mobileToggle
                ? "openMenuForMobile"
                : "openMenuForMobileVert"
            }
            alt=""
            onClick={this.mobileToggle}
          />

          <Collapse
            isOpen={this.state.mobileToggle}
            navbar
            className="navbar-col"
          >
            <Nav className="ml-8" navbar>
              <div className="searchPos" ref={this.inputWidth}>
                <input
                  type="text"
                  className="mobileSearch"
                  onChange={this.filterSearch}
                  value={this.selectedSearchValue}
                />
                <img src={search} className="imgPos" />
              </div>
              <Nav className="nav-search" navbar>
                <Search
                  searchBoxDesktop={this.state.searchBoxDesktop}
                  displaySearchData={this.state.displaySearchData}
                  basePath={basePath}
                />
              </Nav>
              <Solutions
                {...this.props}
                toggle={id => this.toggle(id)}
                removeToggle={(e, id) => this.removeToggle(e, id)}
                mobileToggClick={(e, id) => this.mobileToggClick(e, id)}
                isOpen={this.state.isOpen}
                dropDownId={this.state.dropDownId}
                arrowUp={arrowUp}
                arrowDown={arrowDown}
                selector={this.state.selector}
                handleClick={this.handleClick}
                minus={minus}
                plus={plus}
                displayType={this.state.displayType}
                selectedLink={this.state.selectedLink}
                innerDropDownItems={this.innerDropDownItems}
                removeDropdownItems={this.removeDropdownItems}
                basePath={basePath}
              />
              <DigitalOperations
                {...this.props}
                toggle={id => this.toggle(id)}
                removeToggle={(e, id) => this.removeToggle(e, id)}
                mobileToggClick={(e, id) => this.mobileToggClick(e, id)}
                isOpen={this.state.isOpen}
                dropDownId={this.state.dropDownId}
                arrowUp={arrowUp}
                arrowDown={arrowDown}
                selectedLink={this.state.selectedLink}
                innerDropDownItems={this.innerDropDownItems}
                removeDropdownItems={this.removeDropdownItems}
                basePath={basePath}
              />
              <About
                {...this.props}
                toggle={id => this.toggle(id)}
                removeToggle={(e, id) => this.removeToggle(e, id)}
                mobileToggClick={(e, id) => this.mobileToggClick(e, id)}
                isOpen={this.state.isOpen}
                dropDownId={this.state.dropDownId}
                arrowUp={arrowUp}
                arrowDown={arrowDown}
                selectedLink={this.state.selectedLink}
                innerDropDownItems={this.innerDropDownItems}
                removeDropdownItems={this.removeDropdownItems}
                basePath={basePath}
              />
              <NavItem
                className="dropdown"
                onMouseEnter={() => this.noDropdownSideBrackets(4)}
                onMouseLeave={() => this.noDropdownSideBrackets("")}
              >
                <ContactModal
                
                  open={this.state.modalOpen}
                  onClose={this.modalClose}
                  basePath={basePath}
                />
                <NavLink
                  //href="#"
                  onClick={() => this.modalToggle()}
                  className={this.state.dropDownId === 4 ? "active-nav" : ""}
                >
                  Contact
                </NavLink>
              </NavItem>
              <NavItem
                className="dropdown"
                onMouseEnter={() => this.noDropdownSideBrackets(5)}
                onMouseLeave={() => this.noDropdownSideBrackets("")}
              >
                <NavLink
                  href="/insights"
                  id="insight"
                  className={
                    this.state.dropDownIdInsight === 5 ||
                      this.props.urlRoute === "/insights/"
                      ? "active-nav"
                      : ""
                  }
                >
                  Insights
                </NavLink>
              </NavItem>
            </Nav>
            <MySubscriptions
              searchBoxDesktop={this.state.searchBoxDesktop}
              user={user}
              userHover={userHover}
              search={search}
              searchToggle={this.searchToggle}
              authData={this.state.authData}
              innerOptions={this.state.innerOptions}
              basePath={basePath}
            />
            <Nav className="custom-right-navbar show searchList" navbar>
              <NavItem className="nav-icon position">
                <input
                  ref={this.inputWidth}
                  type="text"
                  style={{ width: "-webkit-fill-available" }}
                  onChange={this.filterSearch}
                  value={this.state.selectedSearchValue}
                  className="searchToggle"
                  placeholder="Type to search..."
                />
                <div ref={this.iconDisplayRef}>
                  <img
                    src={Close}
                    className="close"
                    onClick={this.searchToggle}
                  />
                  <img
                    src={limeSearch}
                    className="limeSearch"
                    onClick={this.searchToggle}
                  />
                </div>
              </NavItem>
              <Nav className="overflow-scroll" navbar>
                <Search
                  searchBoxDesktop={this.state.searchBoxDesktop}
                  displaySearchData={this.state.displaySearchData}
                  basePath={basePath}
                />
              </Nav>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    )
  }
}

export default Header
