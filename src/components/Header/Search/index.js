import React, { useState, useEffect, useRef } from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function Search({ searchBoxDesktop, displaySearchData }) {
      return displaySearchData.map((item, index) => {
        return (
          <NavItem
            key={index}
            style={{
              backgroundColor: "#378889",
              color: "white",
              padding: "5px",
              cursor: "pointer",
            }}
          >
            <NavLink
              href={item.url}
            >
              {item.label}
            </NavLink>
          </NavItem>
        )
      })
}

export default Search
