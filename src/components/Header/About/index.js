import React from "react"
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from "reactstrap"
import AboutSubDropdown from "./DropdownRight"
import "../style.scss"
function About({
  toggle,
  removeToggle,
  mobileToggClick,
  isOpen,
  dropDownId,
  arrowUp,
  arrowDown,
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
}) {
  // alert(dropDownId)
  // alert(isOpen)
  return (
    <UncontrolledDropdown
      nav
      inNavbar
      id="3"
      onMouseEnter={() => toggle("3")}
      //onClick={()=>toggle("3")}
      onMouseLeave={e => removeToggle(e, "3")}
      isOpen={dropDownId === "3" && isOpen}
    >
      <DropdownToggle
        nav
        id="3"
        className={dropDownId === "3" ? "active-nav" : ""}
        onClick={e => mobileToggClick(e, "3")}
      >
        About
        <img
          src={dropDownId === "3" ? arrowUp : arrowDown}
          id="3"
          className="imageArrow"
        />
      </DropdownToggle>
      <DropdownMenu right>
        <AboutSubDropdown
          selectedLink={selectedLink}
          innerDropDownItems={innerDropDownItems}
          removeDropdownItems={removeDropdownItems}
        />
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default About
