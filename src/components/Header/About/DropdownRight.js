import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function DropdownRight({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
}) {
  return (
    <div className="menu-wrap">
      <div className="custom-left-nav">
        <a className="nav-left-link mob active">About</a>
        {/* active */}
      </div>
      <div className="custom-right-nav">
        <Nav right className="sub-dropdown">
          <NavItem>
            {" "}
            <NavLink
              href="/auth/careers"
              name="career"
              className={selectedLink === "career" ? "inner-nav-active" : ""}
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              Careers
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="/auth/digitalEmpowerment"
              name="empowerment"
              className={
                selectedLink === "empowerment" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              Digital Empowerment
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="/auth/caseStudies"
              name="studies"
              className={selectedLink === "studies" ? "inner-nav-active" : ""}
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              Case Studies
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="/auth/ourTeams"
              name="ourteams"
              className={selectedLink === "ourteams" ? "inner-nav-active" : ""}
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              Our Teams
            </NavLink>
          </NavItem>
        </Nav>
      </div>
    </div>
  )
}

export default DropdownRight
