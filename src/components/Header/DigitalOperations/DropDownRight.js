import React from "react"
import "../style.scss"
import { Nav, NavItem, NavLink } from "reactstrap"
function DropDownRight({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
}) {
  return (
    <div className="menu-wrap">
      <div className="custom-left-nav">
        <a className="nav-left-link mob active">Digital Operations</a>
        {/* //active */}
      </div>
      <div className="custom-right-nav">
        <Nav right className="sub-dropdown">
          <NavItem>
            {" "}
            <NavLink
              href="#"
              name="createorder"
              className={
                selectedLink === "createorder" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              Create Order
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="https://www.eztracker.io/"
              name="tracker"
              target="_blank"
              className={selectedLink === "tracker" ? "inner-nav-active" : ""}
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              ezTracker
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="#"
              name="realworld"
              className={
                selectedLink === "realworld"
                  ? "inner-nav-active lastChild"
                  : "lastChild"
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
              Real World Data
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="#"
              name="passwordMaintenance"
              className={selectedLink === "passwordMaintenance" ? "inner-nav-active" : ""}
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
            Password Maintenance
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="#"
              name="tablaeuCommunity"
              className={
                selectedLink === "tablaeuCommunity" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
            Tableau Community
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="#"
              name="infectionWatchInitiative"
              className={selectedLink === "infectionWatchInitiative" ? "inner-nav-active" : ""}
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
            >
            Infection Watch Initiative
            </NavLink>
          </NavItem>
        </Nav>
      </div>
    </div>
  )
}

export default DropDownRight
