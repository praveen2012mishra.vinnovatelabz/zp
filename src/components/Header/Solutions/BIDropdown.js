import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function BIDropdown({ selectedLink, innerDropDownItems, removeDropdownItems, basePath }) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Capability and process development</h5>
      <NavItem>
        {" "}
        <NavLink
          href={`${basePath}/businessInt/datamanagement`}
          name="Management"
          className={selectedLink === "Management" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
         Business Intelligence
        </NavLink>
        {selectedLink === "Management" ? (
          <span className="heading_tagline">
            Customized Data Management Solutions
          </span>
        ) : (
          ""
        )}
      </NavItem>
      {/* <NavItem>
        {" "}
        <NavLink
          href="#"
          name="Feed"
          className={selectedLink === "Feed" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Data Feeds
        </NavLink>
        {selectedLink === "Feed" ? (
          <span className="heading_tagline">
            Customized Data Feed Solutions
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="https://ezpz.zuelligpharma.com/"
          name="Training"
          target="_blank"
          className={selectedLink === "Training" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Training
        </NavLink>
        {selectedLink === "Training" ? (
          <span className="heading_tagline">
            Upgrade and acquire new skills
          </span>
        ) : (
          ""
        )}
      </NavItem> */}
    </Nav>
  )
}

export default BIDropdown
