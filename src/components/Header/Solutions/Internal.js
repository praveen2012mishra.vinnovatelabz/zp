import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function Internal({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Zuellig Pharma Employee Portal</h5>
      <NavItem>
        {" "}
        <NavLink
          href="/auth/internal"
          name="internalHome"
          className={selectedLink === "internalHome" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        Internal home
        </NavLink>
        {selectedLink === "internalHome" ? (
          <span className="heading_tagline">Internal Home</span>
        ) : (
          ""
        )}
      </NavItem>
      {/* 
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="finance"
          className={
            selectedLink === "finance" ? "inner-nav-active" : ""
          }
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Finance
        </NavLink>
        {selectedLink === "finance" ? (
          <span className="heading_tagline">Financial Dashboards</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="procurement"
          className={selectedLink === "procurement" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        Procurement
        </NavLink>
        {selectedLink === "procurement" ? (
          <span className="heading_tagline">
          Procurement Dashboards
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="hr"
          className={selectedLink === "hr" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          HR
        </NavLink>
        {selectedLink === "hr" ? (
          <span className="heading_tagline">
          HR Dashboards
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="operations"
          className={selectedLink === "operations" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        Operations
        </NavLink>
        {selectedLink === "operations" ? (
          <span className="heading_tagline">Quality and Operations Dashboards</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="qbr"
          className={selectedLink === "qbr" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        QBR
        </NavLink>
        {selectedLink === "qbr" ? (
          <span className="heading_tagline">Dashboards for QBRs</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="crm"
          className={selectedLink === "crm" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        CRM
        </NavLink>
        {selectedLink === "crm" ? (
          <span className="heading_tagline">CRM Dashboards</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="ims"
          className={selectedLink === "ims" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        IMS
        </NavLink>
        {selectedLink === "ims" ? (
          <span className="heading_tagline">IMS Sales Information</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="templates"
          className={selectedLink === "templates" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
        Templates
        </NavLink>
        {selectedLink === "templates" ? (
          <span className="heading_tagline">Tableau Templates</span>
        ) : (
          ""
        )}
      </NavItem>
      */}
    </Nav>
  )
}

export default Internal
