import React from 'react'
import "../style.scss"
function DropDownRightWeb({
    selector,
    handleClick,
    basePath
}) {
  console.log(basePath);
    return (
        <div className="custom-left-nav">
            <a
              className={
                selector.insider
                  ? "active nav-left-link m-child"
                  : "nav-left-link m-child"
              }
              name="insider"
              onMouseEnter={handleClick}
            >
              Insider
            </a>
            <a
              className={
                selector.commercialExcellence
                  ? "active nav-left-link m-child"
                  : "nav-left-link m-child"
              }
              name="commercialExcellence"
              onMouseEnter={handleClick}
            >
              Commercial Excellence
            </a>
            <a
              className={
                selector.supplyChain
                  ? "active nav-left-link m-child"
                  : "nav-left-link m-child"
              }
              name="supplyChain"
              onMouseEnter={handleClick}
            >
              Supply Chain Analytics
            </a>
            <a
            className={
              selector.businessIntelligence
                ? `active nav-left-link ${basePath==="/auth" ? "m-child" : "lastChild"}`
                : `nav-left-link ${basePath==="/auth" ? "m-child" : "lastChild"}`
            }
              name="businessIntelligence"
              onMouseEnter={handleClick}
            >
              Business Intelligence
            </a>
            {basePath==="/auth" ? 
            <a
              className={
                selector.internal
                  ? "active nav-left-link lastChild"
                  : "nav-left-link lastChild"
              }
              name="internal"
              onMouseEnter={handleClick}
            >
              Internal
            </a>
            : null }
          </div>
    )
}

export default DropDownRightWeb
