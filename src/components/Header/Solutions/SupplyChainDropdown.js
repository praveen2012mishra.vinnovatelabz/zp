import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function SupplyChainDropdown({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath,
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Sales and Operations Planning</h5>
      <NavItem>
        {" "}
        <NavLink
          href={`${basePath}/supplyChain/controlTower`}
          name="supplyChain"
          className={selectedLink === "supplyChain" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Supply Chain Control Tower
        </NavLink>
        {selectedLink === "supplyChain" ? (
          <span className="heading_tagline">
            Visibility into your Supply Chain
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          
          href={basePath==='auth'?`${basePath}/supplyChain/inventoryPlanner`:'/app/supplyChain/planner'}
          name="SupplyChainPlanner"
          className={selectedLink === "SupplyChainPlanner" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
         Supply Chain Planner
        </NavLink>
        {/* {selectedLink === "Forecasting" ? (
          <span className="heading_tagline">
            Enhanced Forecasts and Replenishments
          </span>
        ) : (
          ""
        )} */}
      </NavItem>
      {/* <NavItem>
        {" "}
        <NavLink
          href="#"
          name="Inventory"
          className={selectedLink === "Inventory" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Inventory Optimization
        </NavLink>
        {selectedLink === "Inventory" ? (
          <span className="heading_tagline">Optimized Inventory Decisions</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="Replenishment"
          className={selectedLink === "Replenishment" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Replenishment Optimization
        </NavLink>
        {selectedLink === "Replenishment" ? (
          <span className="heading_tagline">
            Optimized Replenishment Schedules
          </span>
        ) : (
          ""
        )}
      </NavItem> */}
    </Nav>
  )
}

export default SupplyChainDropdown
