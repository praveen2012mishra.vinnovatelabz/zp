import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function CommercialExcDropDown({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Understanding our customers</h5>
      <NavItem>
        {" "}
        <NavLink
          href={`${basePath}/commercialExp/InfraredSales`}
          name="InfraredSales"
          className={selectedLink === "InfraredSales" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Infrared
        </NavLink>
        {selectedLink === "InfraredSales" ? (
          <span className="heading_tagline">Understand your customers</span>
        ) : (
          ""
        )}
      </NavItem>
      {/*
      <NavItem>
        {" "}
        <NavLink
          href="#"
          name="InfraredGross"
          className={selectedLink === "InfraredGross" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Investigator
        </NavLink>
        {selectedLink === "InfraredGross" ? (
          <span className="heading_tagline">
            Solutions to your unique challenges
          </span>
        ) : (
          ""
        )}
      </NavItem>
        */}
    </Nav>
  )
}

export default CommercialExcDropDown
