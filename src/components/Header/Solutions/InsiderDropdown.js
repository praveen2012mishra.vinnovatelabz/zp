import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import "../style.scss"
function InsiderDropdown({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath,
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Accessing the data you need</h5>
      <NavItem>
        {" "}
        <NavLink
          // href={`${basePath}/insider`}
          href="/auth/insider"
          name="insider"
          className={selectedLink === "insider" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Insider
        </NavLink>
        {selectedLink === "insider" ? (
          <span className="heading_tagline">Live access to your data</span>
        ) : (
          ""
        )}
      </NavItem>
      {/* <NavItem>
        {" "}
        <NavLink
          href={`${basePath}/insider/insiderDetails`}
          name="insiderOffline"
          className={
            selectedLink === "insiderOffline" ? "inner-nav-active" : ""
          }
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Insider Offline
        </NavLink>
        {selectedLink === "insiderOffline" ? (
          <span className="heading_tagline">Data delivered to your needs</span>
        ) : (
          ""
        )}
      </NavItem> */}
      <NavItem>
        {" "}
        <NavLink
          href="https://csm.zuelligpharma.com/login"
          name="csm"
          target="_blank"
          className={selectedLink === "csm" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          CSM
        </NavLink>
        {selectedLink === "csm" ? (
          <span className="heading_tagline">
            Customized Matrix to organize your Sales Reps
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          // href={`${basePath}/insider/clinicalReach`}
          href="/auth/insider/clinicalReach"
          name="reach"
          className={selectedLink === "reach" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Clinical Reach
        </NavLink>
        {selectedLink === "reach" ? (
          <span className="heading_tagline">
            Live access to your Clinical data
          </span>
        ) : (
          ""
        )}
      </NavItem>
      {/* <NavItem>
        {" "}
        <NavLink
          href="#"
          name="reports"
          className={selectedLink === "reports" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
        >
          Customized Reports
        </NavLink>
        {selectedLink === "reports" ? (
          <span className="heading_tagline">Custom access to your data</span>
        ) : (
          ""
        )}
      </NavItem> */}
    </Nav>
  )
}

export default InsiderDropdown
