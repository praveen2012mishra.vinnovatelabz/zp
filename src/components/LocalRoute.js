import React from "react"
import { navigate } from "gatsby"

import { isAdmin } from "../services/authService"

const LocalRoute = ({ component: Component, ...rest }) => {
  return <Component {...rest} />
}

export default LocalRoute
