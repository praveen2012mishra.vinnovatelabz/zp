import React from 'react'
import styles from "./style.module.scss"
import { Table } from "react-bootstrap"
import ButtonItem from "../Button/Button"
import {FiEdit, FiSave} from "react-icons/fi"
import {ImCancelCircle} from "react-icons/im"
import {GiSave} from "react-icons/gi"
import {BsCheckBox, BsInfoCircle} from "react-icons/bs"
import {BiEditAlt} from "react-icons/bi"

function HistogramTable({
    inventoryHeader,
    beginningInventory,
    changeInventoryInputs,
    incomingListData,
    getDropdown,
    dropdownData,
    outgoingListData,
    endingInventory,
    saveUpdate,
    previewChanges,
    cancelPreview,
    previewData={}

}) {
  const dropDownView = (mainItem,type) => (
   mainItem.dropdown?.map((ddItem,ddIndex)=>(
     ddItem && <div key={ddIndex} className={styles.dateHeader}>
    <div className={styles.dateHeading}>
      <input type="radio" checked={previewData.id===ddIndex && previewData.heading===mainItem.heading} onClick={()=>previewChanges(ddIndex,mainItem.heading)} style={{margin: "auto 0.5em auto auto"}}/>
    <input type="text" value={ddItem.Label} name="title" disabled={previewData.id!==ddIndex || previewData.heading!==mainItem.heading} className={previewData.id===ddIndex && previewData.heading===mainItem.heading?styles.histoInput:styles.histoInputSea} style={{textTransform:"capitalize"}} onChange={e=>changeInventoryInputs(e,{},"dd",ddIndex,mainItem.heading)}/>
    </div>
    <tr className={styles.dateContent}>
      {
        ddItem.Series?.map((series,ind)=>(
          <td key={ind}>
          {setContent(series,changeInventoryInputs,"dd",ddIndex,mainItem.heading,previewData)}
        </td>
        ))
      }
      {/* <td onClick={(e)=>getDropdown(e,mainItem,type,false,ddItem.Label)}>{minusSign()}</td> */}
      <td className={styles.buttonId} style={{marginLeft:"5em"}}>
        <BsCheckBox {...{title: "Approve"}} onClick={(e)=>saveUpdate(e,ddItem,ddIndex,mainItem.heading,"approve")}/>
        </td>
      <td className={styles.buttonId}>
        <FiSave {...{title: "Save"}} onClick={(e)=>saveUpdate(e,ddItem,ddIndex,mainItem.heading,"save")}/>
        </td>
      <td className={styles.buttonId}>
        <FiEdit {...{title: "Preview"}}/>
        </td>
      {/* <td className={styles.buttonId}>
        <ImCancelCircle {...{title: "Cancel"}} onClick={()=>cancelPreview()}/>
        </td> */}
    </tr>
    </div>
    ))
  )
    return (
            <Table className={styles.graphDetailsContainer}>
          <thead className={styles.dateHeader}>
            <div className={styles.dateHeading}></div>
            <tr className={styles.dateContent}>
              {inventoryHeader?.map((item, id) => (
                <th key={id}><div className={styles.w_90}>{item.x}</div></th>
              ))}
              <th style={{marginLeft:"5em"}}><div className={styles.w_90}></div>Action</th>
            </tr>
          </thead>
          <tbody>
          <div className={styles.biginnerHeader}>
            <div className={styles.dateHeading} style={{paddingLeft:"20px"}}>Beginning Inventory </div>
            <tr className={styles.dateContent} style={{marginRight:"17.8em"}}>
              {beginningInventory?.map((item, id) => (
                <td key={id}>{setContentXY(item)}</td>
              ))}
            </tr>
          </div>
          <div className={styles.resultContainer}>
            <div className={styles.incomingContainer}>
              <div
                style={{
                  fontSize: ".9em",
                  fontWeight: "200",
                  paddingLeft: "18px",
                }}
              >
                INCOMING
              </div>
              <div className={styles.resultContainer}>
                {incomingListData?.map((mainItem, id) => (
                  <>
                  <div className={styles.dateHeader}>
                    <div className={styles.dateHeading}>
                      <div onClick={(e)=>getDropdown(e,mainItem,"incoming",true)}> {!dropdownData.incoming?.includes(mainItem.heading) || mainItem.heading==="Planned Purchase Orders"?plusSign():minusSign()}</div>
                      <div>{mainItem.heading}</div>
                    </div>
                    <tr className={styles.dateContent}>
                      {mainItem.number?.map((item, id2) => (
                          <td key={id2}>
                          {setContent(item, id2, changeInventoryInputs,"incoming",mainItem)}
                        </td>
                      ))}
                      {
                      mainItem.heading==="Planned Purchase Orders"?<td className={styles.buttonId} style={{marginLeft:"5em"}}><BsInfoCircle {...{title: "This is an Editable field"}}/></td>:""
                    }
                    </tr>
                  </div>
                  {/* Incoming Dropdown */}
                  {
                    mainItem.heading==="Planned Purchase Orders"?dropDownView(mainItem,"incoming"):""
                  }
                  </>
                ))}
              </div>
            </div>
            <div style={{
              width:"100%",
              display:"flex",
              justifyContent:"flex-end"
            }}>
            {/* {rodCircleSign(
                  "You may run out of stock if orders are placed here"
                )} */}
            </div>
            {/* <div className={styles.buttonId}>
              <ButtonItem
                title={"Confirm orders with Zuellig"}
                BColor={"#c3d422"}
                Color={"#094658"}
              />
            </div> */}
            <div className={styles.incomingContainer}>
              <div
                style={{
                  fontSize: ".9em",
                  fontWeight: "200",
                  paddingLeft: "18px",
                }}
              >
                OUTGOING
              </div>
              <div className={styles.resultContainer}>
                {outgoingListData?.map((mainItem, id) => (
                  <>
                  
                  <div className={styles.dateHeader}>
                    <div className={styles.dateHeading}>
                      <div onClick={(e)=>getDropdown(e,mainItem,"outgoing",true)}> {!dropdownData.outgoing?.includes(mainItem.heading)  || mainItem.heading==="Consensus Forecast"?plusSign():minusSign()}</div>
                      <div>{mainItem.heading}</div>
                    </div>
                    <tr className={styles.dateContent}>
                      {mainItem.number?.map((item, id2) => (
                        <td key={id2}>
                          
                          {setContent(item, changeInventoryInputs,"outgoing",mainItem)}
                        </td>
                      ))}
                      {
                        mainItem.heading==="Consensus Forecast"?<td className={styles.buttonId} style={{marginLeft:"5em"}}><BsInfoCircle {...{title: "This is an Editable field"}}/></td>:""
                      }
                    </tr>
                  </div>
                  {/* Outgoing Dropdown */}
                  {
                    mainItem.heading==="Consensus Forecast"?
                    dropDownView(mainItem,"outgoing")
                    :""
                  }
                  </>
                ))}
              </div>
            </div>
            <div className={styles.biginnerHeader}>
              <div className={styles.dateHeading}>Ending Inventory </div>
              <tr className={styles.dateContent} style={{marginRight:"17.8em"}}>
                {endingInventory?.map((item, id) => (
                  <td key={id}>{setContentXY(item)}</td>
                ))}
              </tr>
            </div>
          </div>
          </tbody>
        </Table>
    )
}
const setContent = (item, changeInventoryInputs,stateId,ddIndex,head,previewData) => {
  let inputDisabled = stateId!=="dd" || previewData.id!==ddIndex || previewData.heading!==head
    // if (item.value === 0) {
    //   if (id === 0) {
    //     return <div className={styles.horizontalLine}></div>
    //   } else {
    //     if (beginnerList[id - 1]?.value === 0) {
    //       return <div className={styles.horizontalLine}></div>
    //     } else {
    //       return <div className={styles.spaceLine}></div>
    //     }
    //   }
    // } else {
      return <input type="number" value={item.value===null?0:item.value} disabled={inputDisabled} className={inputDisabled?styles.histoInputSea:styles.histoInput} onChange={e=>changeInventoryInputs(e,item,stateId,ddIndex,head)}/>
    //}
}
const setContentXY = (item) => {
  
  // if (item.value === 0) {
  //   if (id === 0) {
  //     return <div className={styles.horizontalLine}></div>
  //   } else {
  //     if (beginnerList[id - 1]?.value === 0) {
  //       return <div className={styles.horizontalLine}></div>
  //     } else {
  //       return <div className={styles.spaceLine}></div>
  //     }
  //   }
  // } else {
    return <input type="number" value={item.value===null?0:item.value} disabled className={styles.histoInput}/>
  //}
}
const plusSign = () => <div className={styles.plusSign}><div className={styles.signs}>+</div></div>
const minusSign = () => <div className={styles.plusSign}><div className={styles.signs}>-</div></div>
// const rodCircleSign = item => (
//   <div className={styles.keyContainer}>
//     <div className={styles.rod}>{item}</div>
//     <div className={styles.circle}>!</div>
//   </div>
// )
const horizontalLine = () => <div className={styles.horizontalLine}></div>
const spaceLine = () => <div className={styles.spaceLine}></div>


export default HistogramTable
