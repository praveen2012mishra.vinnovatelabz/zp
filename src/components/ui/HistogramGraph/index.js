import React from "react"
import styles from "./style.module.scss"
import HistogramTable from "./Table"
import '../../../../node_modules/react-vis/dist/style.css';
import { 
  XYPlot, 
  LineSeries, 
  VerticalBarSeries, 
  VerticalGridLines,
  HorizontalGridLines,
  VerticalBarSeriesCanvas, 
  YAxis, 
  XAxis } from 'react-vis';
import axios from "../../../../axiosConfig"
import {format} from "d3"

let data = [
  { x: "Jan 2018", y: 0 },
  { x: "June 2018", y: 0 },
  { x: "Jan 2019", y: 0 },
  { x: "June 2019", y: 0 },
  { x: "Jan 2020", y: 0 },
  { x: "June 2020", y: 0 },
  { x: "Jan 2021", y: 0 },
]

let beginnerList = [0, 10645, 19458, 16233, 12535, 10642]

let incomingList = [
  { heading: "Pending Stock", number: [0, 0, 0, 3800, 2620, 0] },
  { heading: "Goods Received", number: [9980, 0, 0, 0, 2620, 0] },
  { heading: "Suggested Purchase Orders", number: [0, 0, 0, 0, 7750, 0] },
  { heading: "Planned Purchase Orders", number: [0, 0, 0, 0, 0, 7550] },
]

let outgoingList = [
  { heading: "Actual Sales", number: [3960, 2460, 2890, 3800, 0, 0] },
  { heading: "Other Outflows", number: [0, 0, 0, 0, 2620, 0] },
  {
    heading: "Model Forecast",
    number: [4022, 2500, 3550, 4500, 4500, 0],
  },
  { heading: "Consensus Forecast", number: [0, 0, 0, 0, 3500, 3680] },
]
class HistogramGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      graphData: this.props.graphData,
      inventoryHeader: data,
      beginningInventory: beginnerList,
      incomingListData: incomingList,
      outgoingListData: outgoingList,
      endingInventory: [],
      graphPlottingData:{},
      dropdownData: {
        incoming: [],
        outgoing: []
      },
      selectedDate: "",
      previewData:{}
    }

  }
  componentDidMount() {
    let { searchData, monthArray } = this.props
    const { graphData } = this.state
    let begInventory = graphData[0].HistoricalInventoryFlow.BeginningInventory
    let endInventory = graphData[0].HistoricalInventoryFlow.EndingInventory
    let selectedDate = ""
    selectedDate = searchData.MonthRun ? searchData.MonthRun : monthArray[0]
    let beginningInventory = this.getFilteredArray(begInventory, selectedDate)
    let endingInventory = this.getFilteredArray(endInventory, selectedDate)
    let incomingListData = this.getIncomingList(graphData[0], selectedDate)
    let outgoingListData = this.getOutgoingList(graphData[0], selectedDate)
    let actualData = this.getInventoryHeader(selectedDate, outgoingListData, "Actual Sales")
    let forecastData = this.getInventoryHeader(selectedDate, outgoingListData, "model forecast")
    let actualGraphData = actualData?.filter(a=>a.y>0)
    let actualEndingIndex = forecastData.findIndex(a=>a.x===actualGraphData[actualGraphData.length-1].x)
    let forecastGraphData = forecastData.slice(actualEndingIndex+1)
    console.log(forecastGraphData)
    // let beginningInventoryObj = this.getInventoryHeader(selectedDate,begInventory,false)
    // let endingInventoryObj = this.getInventoryHeader(selectedDate,endingInventory,false)
    let safetyStock = this.getInventoryHeader(selectedDate,graphData[0].SafetyStock,false)
    this.setState({
      inventoryHeader: actualData,
      beginningInventory,
      incomingListData,
      outgoingListData,
      endingInventory,
      selectedDate,
      graphPlottingData:{
        forecastGraphData:forecastGraphData,
        actualGraphData:actualGraphData,
        safetyStock:safetyStock
      }
    })
  }

  componentWillUnmount() {
    this.props.eraseGraphData()
    this.mounted = false
  }
  // returns last 6 months data array
  getFilteredArray = (arr, selectedDate) => {
    let selectedIndex = arr.findIndex(a => a.key === selectedDate)
    console.log(selectedIndex)
    if(selectedIndex!==-1){
    return arr.slice(selectedIndex - 3, selectedIndex + 12)
    }
    // return arr
  }

  // returns months Array for(graph and table header)
  getInventoryHeader = (selectedDate, begInventory, heading) => {
    var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
    let dataGraph = []
    if(heading !== false){
      let data = begInventory.find(a => a.heading.toLowerCase() === heading.toLowerCase())
      dataGraph = this.getFilteredArray(data.number,selectedDate)
    }else{
      dataGraph = this.getFilteredArray(begInventory,selectedDate)
    }
      return dataGraph.map((item,index)=>{
        // console.log()
        let obj = {x:"",y:0}
        let allMonth = parseInt(item.key.slice(4))
        let allYear = item.key.slice(0,4)
        obj.x = months[allMonth-1]+" "+allYear
        obj.y = item.value
        return obj
      })
    }

  //returns complete incoming data list
  getIncomingList = (graphData, selectedDate) => {
    let suggestedPo = []
    let plannedPo = []
    let goodsReceivedArray = graphData.HistoricalInventoryFlow.Incoming.GoodsReceived
    let pendingStockArray = graphData.HistoricalInventoryFlow.Incoming.PendingStock
    let incomingListData = [...this.state.incomingListData]
    graphData.ForecastInventoryFlow.forEach(item => {
      if (item.MonthRun === selectedDate) {
        suggestedPo = item.Incoming.SuggestedPO
        plannedPo = item.Incoming.PlannedPO
      }
      return;
    })
    let obj = {}
    obj.suggestedPoNumber = this.getFilteredArray(suggestedPo, selectedDate)
    obj.goodsReceived = this.getFilteredArray(goodsReceivedArray, selectedDate)
    obj.pendingStock = this.getFilteredArray(pendingStockArray, selectedDate)
    incomingListData.forEach(item => {
      item.dropdown = []
      if (item.heading === "Suggested Purchase Orders") {
        item.number = obj.suggestedPoNumber;
      } else if (item.heading === "Planned Purchase Orders") {
        let data = plannedPo.map(a=>{a.Series=this.getFilteredArray(a.Series,selectedDate);return a})
        let palannedNumber = data.find(a=>a.Status==="Approved")
        item.number = palannedNumber.Series
        item.dropdown = data.filter(a=>a.Status==="Pending");
      }
      else if (item.heading === "Goods Received") {
        item.number = obj.goodsReceived;
      }
      else if (item.heading === "Pending Stock") {
        item.number = obj.pendingStock;
      }
      return item
    })
    
    return incomingListData
  }

  // returns complete outgoing data list
  getOutgoingList = (graphData, selectedDate) => {
    let consensusForecast = []
    let modelForecast = []
    let actualSales = graphData.HistoricalInventoryFlow.Outgoing.ActualSales
    let otherOutflows = graphData.HistoricalInventoryFlow.Outgoing.OthersOutflow
    let outgoingListData = [...this.state.outgoingListData]
    graphData.ForecastInventoryFlow.forEach(item => {
      if (item.MonthRun === selectedDate) {
        consensusForecast = item.Outgoing.ConsensusForecast
        modelForecast = item.Outgoing.ModelForecast
      }
      return;
    })
    let obj = {}
    obj.actualSales = this.getFilteredArray(actualSales, selectedDate)
    obj.modelForecast = this.getFilteredArray(modelForecast, selectedDate)
    obj.otherOutflowsData = this.getOtherOutflows(otherOutflows, selectedDate)
    outgoingListData.forEach(item => {
      item.dropdown = []
      if (item.heading === "Actual Sales") {
        item.number = obj.actualSales;
      } if (item.heading === "Other Outflows") {
        item.number = obj.otherOutflowsData;
        item.dropdown = { Label:"expired",Series:obj.otherOutflowsData.ExpiredStock, Label:"scrapped",Series:obj.otherOutflowsData.Scrap }
      }
      if (item.heading === "Model Forecast") {
        item.number = obj.modelForecast;
      }
      if (item.heading === "Consensus Forecast") {
        let data = consensusForecast.map(a=>{a.Series=this.getFilteredArray(a.Series,selectedDate);return a})
        let concensusNumber = data.find(a=>a.Status==="Approved")
        item.number = concensusNumber.Series;
        item.dropdown = data.filter(a=>a.Status==="Pending")
      }
      return item
    })
    return outgoingListData
  }

  // returns otherOutFlow Array for outgoing list
  getOtherOutflows = (otherOutflows, selectedDate) => {
    let expired = this.getFilteredArray(otherOutflows.ExpiredStock, selectedDate)
    let scrapped = this.getFilteredArray(otherOutflows.Scrap, selectedDate)
    let otherOutFlowArray = []
    expired.forEach(exp => {
      scrapped.forEach(scr => {
        let obj = { key: "", value: 0 }
        if (exp.key === scr.key) {
          obj.key = exp.key;
          obj.value = exp.value + scr.value
          otherOutFlowArray.push(obj)
        }
      })
    })
    return otherOutFlowArray;
  }


  //returns the dropdown list
  getDropdown = (e, item, key, toggle, mainIndex) => {
    let dropdownData = { ...this.state.dropdownData }
    let stateData = []
    let mainItem = { ...item }
    if (mainItem.heading !== "Planned Purchase Orders" && mainItem.heading !== "Consensus Forecast") {
      if (!dropdownData[key].includes(mainItem.heading)) {
        dropdownData[key].push(mainItem.heading)
      } else {
        dropdownData[key].splice(dropdownData[key].indexOf(mainItem.heading), 1)
      }
    } else {
      let dropdownData = {
        ApprovedBy: null,
        ApprovedDate: null,
        Label: "Change Title",
        ModifiedBy: "",
        ModifiedDate: "",
        Series: [],
        Status: "Pending"
      }
      if (mainItem.heading == "Planned Purchase Orders") {
        stateData = [...this.state.incomingListData]
        let headingIndex = stateData.findIndex(a => a.heading === mainItem.heading)
        if (toggle) {
          dropdownData.Series=JSON.parse(JSON.stringify(mainItem.number))
          mainItem.dropdown = [ ...mainItem.dropdown, dropdownData ]
        }
        stateData.splice(headingIndex, 1, mainItem)
        this.setState({ incomingListData: stateData })
      } else {
        stateData = [...this.state.outgoingListData]
        let headingIndex = stateData.findIndex(a => a.heading === mainItem.heading)
        if (toggle) {
          dropdownData.Series=JSON.parse(JSON.stringify(mainItem.number))
          mainItem.dropdown = [ ...mainItem.dropdown, dropdownData ]
        } 
        stateData.splice(headingIndex, 1, mainItem)
        this.setState({ outgoingListData: stateData })
      }
    }
    this.setState({ dropdownData})
  }

  changeInventoryInputs = (e, item, stateId, ddIndex, head) => {
    let { name, value } = e.target
    let stateData;
    // if (stateId === "incoming") {
    //   stateData = [...this.state.incomingListData]
    //   let dataIndex = stateData.findIndex(a => a.heading === mainItem.heading)
    //   let data = stateData.find(a => a.heading === mainItem.heading)
    //   data.number[id].value = value ? parseInt(value) : ""
    //   stateData.splice(dataIndex, 1, data)
    //   this.setState({ incomingListData: stateData })
    // }
    // if (stateId === "outgoing") {
    //   stateData = [...this.state.outgoingListData]
    //   let dataIndex = stateData.findIndex(a => a.heading === mainItem.heading)
    //   let data = stateData.find(a => a.heading === mainItem.heading)
    //   data.number[id].value = value ? parseInt(value) : ""
    //   stateData.splice(dataIndex, 1, data)
    //   this.setState({ outgoingListData: stateData })
    // }
    // if (stateId === "beginning") {
    //   stateData = [...this.state.beginningInventory]
    //   let dataIndex = stateData.findIndex(a => a.key === item.key)
    //   let data = stateData.find(a => a.key === item.key)
    //   data.value = value ? parseInt(value) : ""
    //   stateData.splice(dataIndex, 1, data)
    //   this.setState({ beginningInventory: stateData })
    // }
    // if (stateId === "ending") {
    //   stateData = [...this.state.endingInventory]
    //   let dataIndex = stateData.findIndex(a => a.key === item.key)
    //   let data = stateData.find(a => a.key === item.key)
    //   data.value = value ? parseInt(value) : ""
    //   stateData.splice(dataIndex, 1, data)
    //   this.setState({ endingInventory: stateData })
    // } 
    if (stateId === "dd") {
      if (head === "Consensus Forecast") {
        stateData = [...this.state.outgoingListData]
        let data = this.ddInputsFor(stateData, head, ddIndex, item, value, name)
        console.log(data)
        this.setState({ outgoingListData: data })
      } else {
        stateData = [...this.state.incomingListData]
        let data = this.ddInputsFor(stateData, head, ddIndex, item, value, name)
        this.setState({ incomingListData: data })
      }
    }
  }
  ddInputsFor = (stateData, heading, ddIndex, item, value, name) => {
    let dataIndex = stateData.findIndex(a => a.heading === heading)
    let data = stateData.find(a => a.heading === heading)
    data.dropdown.forEach((ddItem,ddInd)=>{
      if (ddInd === ddIndex) {
        if(name==="title"){
          ddItem.Label=value
        }else{
          let changedObjectIndex = ddItem.Series.findIndex(a => a.key === item.key)
          let changedObject = ddItem.Series.find(a => a.key === item.key)
          changedObject.value = value? parseFloat(value) :""
          ddItem.Series.splice(changedObjectIndex, 1, changedObject)
        }
      }
    })
    stateData.splice(dataIndex, 1, data)
    return stateData
  }
  graphHeader = (leadTime) => (
    <div style={{ overflowX: "auto" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div className={styles.graphHeading}>
          <div >SKU CODE</div>
          <div>{this.props.graphData[0]?.MaterialCode}</div>
        </div>
        <div className={styles.graphHeading}>
          <div >MANAGER</div>
          <div>{this.props.graphData[0]?.Manager}</div>
        </div>
        <div className={styles.graphHeading}>
          <div >LEAD TIME (MONTHS)</div>
          <div>{leadTime}</div>
        </div>
      </div>
      <div style={{ margin: "3em 6em 0 6em", display: "flex" }}>
        <span style={{ marginLeft: "5em", cursor: "pointer" }} 
        // onClick={() => this.setState({ inventoryHeader: this.getInventoryHeader(this.state.selectedDate, this.state.outgoingListData, "Actual Sales") })}
        >
          <span style={{ color: "#06333f", fontWeight: "bolder", paddingRight: "5px" }}>-----</span>

          <span style={{ color: "#06333f", fontWeight: "bolder" }}>Safety Stock</span>
        </span>
        {/* <span style={{ marginLeft: "1em", cursor: "pointer" }} onClick={() => this.setState({ inventoryHeader: this.getInventoryHeader(this.state.selectedDate, this.state.outgoingListData, "Model Forecast") })}>
          <span style={{ color: "#c3d422", fontWeight: "bolder", paddingRight: "5px" }}>-----</span>

          <span style={{ color: "#c3d422", fontWeight: "bolder" }}>Forecast</span>
        </span> */}
        <span style={{ marginLeft: "1em", cursor: "pointer", display: "flex" }}>
          <div style={{ height: "12px", width: "35px", backgroundColor: "#ccd8d8", marginTop: "auto", marginBottom: "auto", marginRight: "5px" }}></div>

          <span style={{ color: "#ccd8d8", fontWeight: "bolder" }}>Actual Inventory</span>
        </span>
        <span style={{ marginLeft: "1em", cursor: "pointer", display: "flex" }}>
          <div style={{ height: "12px", width: "35px", backgroundColor: "#c3d422", marginTop: "auto", marginBottom: "auto", marginRight: "5px" }}></div>

          <span style={{ color: "#c3d422", fontWeight: "bolder" }}>Forecast Inventory</span>
        </span>
      </div>
    </div>
  )
  saveUpdate = (e,obj,i,heading,type) =>{
    let graphData = JSON.parse(JSON.stringify(this.state.graphData))
    let flow = graphData[0].ForecastInventoryFlow[0]
    let message = ""
    let arr = []
    let flowType = ""
    if(type==="save"){
      if(heading==="Planned Purchase Orders"){
        let pIndex = flow.Incoming.PlannedPO.findIndex(a=>a.Label===obj.Label && a.Status !== "Approved")
        if(obj.Label && pIndex !== -1) flow.Incoming.PlannedPO.splice(pIndex,1,obj)
        else flow.Incoming.PlannedPO.push(obj)
        arr = flow.Incoming.PlannedPO
        flowType="planned"
      }else if(heading==="Consensus Forecast"){
        let pIndex = flow.Outgoing.ConsensusForecast.findIndex(a=>a.Label===obj.Label && a.Status !== "Approved")
        if(obj.Label && pIndex !== -1) flow.Outgoing.ConsensusForecast.splice(pIndex,1,obj)
        else flow.Outgoing.ConsensusForecast.push(obj)
        arr = flow.Outgoing.ConsensusForecast
        flowType="consensus"
      }
      message="Inventory Updated Successfully"
    }else{
      if(heading==="Planned Purchase Orders"){
        let approvedPlannedIndex = flow.Incoming.PlannedPO.findIndex(a=>a.Status==="Approved")
        obj.Status="Approved"
        flow.Incoming.PlannedPO.splice(approvedPlannedIndex,1,obj)
        let pendingPlannedData = flow.Incoming.PlannedPO.filter(a=>a.Status==="Approved")
        arr = pendingPlannedData
        flowType="planned"
      }else if(heading==="Consensus Forecast"){
        let approvedConsensusIndex = flow.Outgoing.ConsensusForecast.findIndex(a=>a.Status==="Approved")
        obj.Status="Approved"
        flow.Outgoing.ConsensusForecast.splice(approvedConsensusIndex,1,obj)
        let pendingConsensusData = flow.Outgoing.ConsensusForecast.filter(a=>a.Status==="Approved")
        arr = pendingConsensusData
        flowType="consensus"
      }
      message="Status Approved Successfully"
    }
    this.updateInventory(graphData,arr,message,flowType)
  }
  updateInventory = (graphData,flow,message,flowType) => {
    axios.post("/supplyChain/updateInventory",{_id:graphData[0]._id,obj:flow,MonthRun:this.state.selectedDate,flowType}).then(res=>{
      if(res.data.status){
        alert(message)
        this.props.setGraphStateData(res.data.updatedObject)
      }
    }).catch(err=>{
      alert(JSON.stringify("Please try again"))
    })
  }
  render() {
    let leadTime = this.props.graphData[0]?.LeadTime.slice(0, 1)
    return (
      <div className={styles.graphContainer}>
        {this.graphHeader(leadTime)}
        <GraphPlots
          actual={this.state.graphPlottingData.actualGraphData}
          forecast={this.state.graphPlottingData.forecastGraphData}
          safetyStockBelowInventory={this.state.graphPlottingData.safetyStock}
          safetyStockAboveInventory={this.state.graphPlottingData.safetyStock}
        />
        <div className={styles.graphFooterContent}>INVENTORY LEVEL DETAILS</div>
        <HistogramTable
          inventoryHeader={this.state.inventoryHeader}
          beginningInventory={this.state.beginningInventory}
          changeInventoryInputs={this.changeInventoryInputs}
          incomingListData={this.state.incomingListData}
          getDropdown={this.getDropdown}
          dropdownData={this.state.dropdownData}
          outgoingListData={this.state.outgoingListData}
          endingInventory={this.state.endingInventory}
          saveUpdate={(e,obj,i,heading,type)=>this.saveUpdate(e,obj,i,heading,type)}
          previewChanges={(id,heading)=>this.setState({previewData:{id,heading}})}
          cancelPreview={()=>this.setState({previewData:{}})}
          previewData={this.state.previewData}
        />
      </div>
    )
  }
}

const GraphPlots = ({
  actual,
  forecast,
  safetyStockBelowInventory,
  safetyStockAboveInventory
}) => {
  return <div className={styles.graphDiv}>
    <XYPlot
      xType="ordinal"
      height={500} width={1000}>
      <XAxis/>
      <YAxis tickFormat={tick => format('.2s')(tick)}/>
      <HorizontalGridLines/>
      <VerticalBarSeries barWidth={0.5} color="#ccd8d8" data={actual} />
      {/* ?.map((item,i)=>{
        console.log(safetyStockBelowInventory)
        return {...item, style:{fill:"red"}}
      }) */}
      <VerticalBarSeries barWidth={0.5} stroke={10} color="#c3d422" data={forecast}/>
      <LineSeries color={"#06333f"} data={safetyStockBelowInventory} />
      {/* <LineSeries color="#c3d422" data={forecast} /> */}
    </XYPlot>
  </div>
}


export default HistogramGraph