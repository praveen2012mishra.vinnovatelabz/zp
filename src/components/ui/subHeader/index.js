import React from 'react'
import Styles from './style.module.scss'
import helpicon from '../../../assets/images/helpicon2.png'

export const SubHeader = ({title, subTitle, onBtnClick}) => {
    const [hover, setHover] = React.useState(false)
    return (
        <div className={Styles.tabDiv}>
            <div className={Styles.tabHeader}>
                <div>
                    <span style={{ color: "#094658" }} className={Styles.title}>
                        {title}
        </span>{" "}
                    <br />
                    <span style={{ color: "#378889", fontSize: 15 }}>
                        {subTitle}
        </span>
                </div>
                <span onMouseOver={() => setHover(true)} onMouseOut={() => setHover(false)} role="button">
                    <div style={{width:"70px"}}>{hover ?<div className={Styles.help}>Help</div>:  <img src={helpicon} className={Styles.helpicon}/>}</div>
                  
                </span>
            </div>
        </div>
    )
}

export default SubHeader