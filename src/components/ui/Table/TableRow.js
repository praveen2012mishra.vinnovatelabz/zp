import React, { useState } from "react"
import styles from "./style.module.scss"
import { FaCalendarTimes } from "react-icons/fa"
import { GiCalendar } from "react-icons/gi"

import { AiOutlineCaretDown, AiOutlineCaretUp } from "react-icons/ai"
import { Dropdown } from "react-bootstrap"
import HistogramGraph from "../HistogramGraph/index"

const TableRow = props => {
  // const lengthRow = props.tableValues[0].length;
  // const [dropStatus, setStatus] = useState(false)
  const [dropdownOpen, setDropdownOpen] = useState(false)
  let averageSales = (props.tableValues.averageSales / 6).toFixed(1)
  let currentStock = props.tableValues.currentStock?.value
  let stockOutIn = (currentStock/averageSales).toFixed(1)
  let stockTime = props.tableValues.SafetyStockTime
  let safetyStockTime = stockTime?.slice(0,stockTime.indexOf('-'))
  // const toggle = () => setDropdownOpen(prevState => !prevState)
  return (
    <div
      className={`${styles.tableRowContainer} ${styles.effect6} mb-3 p-2`}
      onClick={() =>
        props.toggleChange(props.id, props.tableValues.MaterialCode)
      }
    >
      <div className={`${styles.rowContent} mr-3 ${styles.skuColumn}`}>
        <span style={{ marginTop: "auto", marginBottom: "auto" }}>
          {props.toggleOpen && props.toggleOpen.id === props.id ? (
            <AiOutlineCaretUp />
          ) : (
            <AiOutlineCaretDown />
          )}
        </span>
        <div style={{ marginLeft: "0.5em" }}>
          {props.tableValues.MaterialDescription}
        </div>
      </div>
      <div
        className={`${styles.rowContent}  mr-3`}
        style={{
          color:
            props.tableValues.ApprovalStatus === "Pending" ? "#f75c5c" : "",
        }}
      >
        <div>{props.tableValues.ApprovalStatus}</div>
      </div>
      <div
        className={`${styles.rowContent} p-2  mr-3`}
        style={{
          backgroundColor:
            props.tableValues.InventoryHealth === "Urgent" ? "#f75c5c" : "",
          color:
            props.tableValues.InventoryHealth === "Urgent" ? "#ffffff" : "",
          margin: "auto",
        }}
      >
        <div>{props.tableValues.InventoryHealth}</div>
      </div>
      <div className={`${styles.rowContent}  mr-3`}>
      <div>{currentStock}</div>
      </div>
      <div className={`${styles.rowContent}  mr-3`}>
        <div>{averageSales}</div>
      </div>
      <div className={`${styles.rowContent}  mr-3`}>
        <div>{stockOutIn}</div>
      </div>
      <div className={`${styles.rowContent}  mr-3 ${styles.safetyStock}`}>
      <div>{safetyStockTime}</div>
        <div
          style={{
            border: "1px solid",
            width: "30px",
            height: "30px",
            borderRadius: "50%",
            backgroundColor: "#378889",
            textAlign: "center",
          }}
        >
          <GiCalendar
            style={{ color: "#fff", margin: "auto" }}
            className={`${styles.caret}`}
          />
        </div>
      </div>
    </div>
  )
}

export default TableRow
