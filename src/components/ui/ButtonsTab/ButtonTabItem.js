import React from "react"

const ButtonTabItem = ({ onClick, active, activeClassname, title }) => (
  <div className={active ? activeClassname : ""} onClick={onClick}>
    <p>{title}</p>
  </div>
)

export default ButtonTabItem
