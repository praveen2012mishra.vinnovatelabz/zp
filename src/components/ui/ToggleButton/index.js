import React from "react"

import styles from "./style.module.scss"

const ToggleButton = ({ checked, onChange }) => {
  if (typeof onChange !== "function") {
    onChange = () => {}
  }

  return (
    <label className={styles.switch}>
      <input onChange={onChange} type="checkbox" checked={checked || false} />
      <span className={[styles.slider, styles.round].join(" ")}></span>
    </label>
  )
}

export default ToggleButton
