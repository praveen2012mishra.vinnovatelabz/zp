import React, { useState, useEffect } from "react"
import styles from "./style.module.scss"
import { Row,Col, Button } from "react-bootstrap"
import { Bar } from "react-chartjs-2"
import TableauReport from 'tableau-react';
import { navigate } from "gatsby";
function SalesFlashBody({url,label,backListener}) {
  const option={
    height:"150vh",
    width:`100%`,
    hideTabs:true,
    marginTop:20,
    overflowX: "hidden",
    borderStyle: "none"
  }
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col col-12">
          <div className={`${styles.headingColor}`}>{label}</div>
        </div>
      </div>
      <div className="row">
        <div className="col col-12 row">
        { backListener ? 
        <div className="col" style={{ position: "absolute", zIndex: 1 }}>
        <Button className={styles.btn} onClick={backListener}>Back</Button>
        </div>
        : null
        }
          <div className={`flex-1 ${styles.subHeadingColor}`}>
            Analyse your historical monthly and yearly sales here
          </div>
        </div>
      </div>
      <hr />
      {url ? <TableauReport
        // url="https://zplive.zuelligpharma.com/t/ZiP-Demo/views/TableauTemplate/WhiteDashboard?:isGuestRedirectFromVizportal=y&:embed=y"
        url={url}
        options={option}
      /> : <div>Invalid url</div>}
      <Row style={{ textAlign: "center",marginTop:20 }}>
        <Col>
          <button 
            className={`btn btn-sm ${styles.button}`}
            onClick={() => navigate('/auth/commercialExp/InfraredSales')}
          >ANALYSE SALES REPORT</button>
        </Col>
      </Row>
    </div>
  )
}



export default SalesFlashBody
