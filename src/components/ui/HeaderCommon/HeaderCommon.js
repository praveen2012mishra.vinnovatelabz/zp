import React, { useState } from "react";
import styles from "./style.module.scss"
import Tab from "../../Tab"
export default function HeaderCommonComponent(props) {

  return (
    <div
      style={{
        backgroundImage: props.HeaderImage ? `url(${props.HeaderImage})` : '',
        backgroundRepeat: "no-repeat",
        backgroundSize: `100% ${props.height}`,
        backgroundPosition: "0 75px",
        backgroundRepeat: "no-repeat"
      }}
      className={`${styles.main}`}
    >
      <h1
        style={{ marginBottom: "5%" }}
        className={`text-center ${styles.title}`}
      >
        {props.heading}
      </h1>
      {props.sub_heading}
      {props.tabs && props.tabs.length > 0 && (<Tab
        items={props.tabs}
        activeTab={props.tabSelected}
        onTabChange={props.onTabChange}
      />)}

      {props.children}
    </div>
  )
}