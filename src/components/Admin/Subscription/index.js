import React, { useState, useEffect } from "react"
import { Accordion, Card, Dropdown, ButtonGroup } from "react-bootstrap"
import styles from "../style.module.scss"
import HeaderCommonComponent from '../../ui/HeaderCommon/HeaderCommon'
import Axios from '../../../../axiosConfig'
import Cookie from 'js-cookie';

const SubscriptionPage = () => {
  const tabs = []
  const [fields, setFields] = useState([]);
  const [authData,setAuthData]= useState({
    nameId: "Dummy",
    sessionIndex: "",
    inResponse: "",
  })
  useEffect(() => {
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    let group=Cookie.get("inner_option")?JSON.parse(Cookie.get("inner_option")):null
    if (samlToken) {
      console.log(Cookie.get("saml_response"))
      setAuthData({
          nameId: samlToken.user.name_id,
          sessionIndex: samlToken.user.session_index,
          inResponse: samlToken.response_header.in_response_to,
        })}
    Axios.post("/subs/subsDatabase",{
      groupName: group?group.label:null
    })
    .then((res) => {
      setFields(res.data);
    })
    .catch((err) => {
      console.error("Failed to get the data", err);
    })
  }, []);                 

  return (
    <div>
      <HeaderCommonComponent heading={'My Subscriptions'} sub_heading={<SubHeading authData={authData}/>} tabs={[]} />
      <div
        className="container-fluid p-0"
        style={{ marginBottom: "20px", overflow: "hidden" }}
      >
        <div className="row">
          <div className="col col-12">
            <Accordion defaultActiveKey="0">
              <AccordianItems header="COMMERCIAL EXCELLENCE" 
              tableHeader={[
                "Contract",
                "Product",
                "ATC",
                "Country",
                "Frequency",
                "Start Date",
                "End Date"
              ]}
              fields={fields}
              eventKey="0"
              />
              <AccordianItems header="INSIDER" 
              tableHeader={[
                "Contract",
                "Product",
                "ATC",
                "Country",
                "Frequency",
                "Start Date",
                "End Date"
              ]}
              fields={fields}
              eventKey="1"
              />
            </Accordion>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SubscriptionPage


const SubHeading=({authData})=>(<div
  style={{
    marginTop: "30px",
    paddingBottom: "30px",
    textAlign: "center",
    marginLeft: "25%",
    marginRight: "25%",
    color: "#FFFFFF",
    fontSize: "20px",
  }}
>
  <p>
    Hello <strong style={{ color: "#c3d422" }}>{authData.nameId}</strong> (not 
      {` ${authData.nameId}`} <span role="button" onClick={
        ()=>Axios
        .post("/saml/singleLogout", {
          name: authData.nameId,
          session: authData.sessionIndex,
          in_resp: authData.inResponse,
          status: "not known",
        })
        .then(res => {
          Cookie.remove("inner_option")
          window.location.href = res.data.redirect
        })
        .catch(e => console.log(e))
      } style={{color: "#c3d422"}}>Sign out</span>). From
    your account dashboard you can view your recent orders, manage your
    shipping and billing addresses and
    </p>
  <p style={{ color: "#c3d422", marginTop: "-10px" }}>
    edit your password and account details.
    </p>
</div>)

const AccordianItems = ({header, tableHeader, fields, eventKey}) => {
  const [noData, setNoData] = useState(false);
  // current date
  const getStatus = (endDate, startDate) => {
  let currentDate = new Date();
  let dd = String(currentDate.getDate()).padStart(2, '0');
  let month = String(currentDate.getMonth() + 1).padStart(2, '0');
  let mm = (month).length === 1 ? "0" + 
  String(currentDate.getMonth() + 1).padStart(2, '0') : String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
  let yyyy = currentDate.getFullYear();
  let presrntDate = yyyy + '-' + mm + '-' + dd;
  let startMonth = Number.parseInt(startDate.substring(5,7))
  let currentMonth = Number.parseInt(presrntDate.substring(5,7))
  let endMonth = Number.parseInt(endDate.substring(5,7))

  let status =  (currentMonth - endMonth === 3) ? "Expiring Soon" : (currentMonth > endMonth) ? "Expired" : 
  (currentMonth < endMonth) ? "active" : (currentMonth < startMonth) ? "Starting Soon" : null  
  return status;
  }
  
  return (
              <Card>
                <Accordion.Toggle
                  as={Card.Header}
                  className="bg-light-teal"
                  eventKey={eventKey}
                >
                  <div className="d-flex">
                    <div className="flex-grow-1">
                      <span
                        style={{
                          marginLeft: "60px",
                          fontWeight: "bold",
                          fontSize: "18px",
                          color: "#094658",
                        }}
                      >
                        {header}
                      </span>
                    </div>
                    <div>
                      <span
                        style={{
                          marginLeft: "30px",
                          fontWeight: "bold",
                          fontSize: "18px",
                          color: "#094658",
                        }}
                      >
                        <Dropdown as={ButtonGroup}>
                          <Dropdown.Toggle
                            variant="transparent"
                            style={{
                              color: "#094658",
                              marginBottom: "-15px",
                              marginRight: "150px",
                            }}
                          />
                        </Dropdown>
                      </span>
                    </div>
                  </div>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={eventKey}>
                  <Card.Body>
                    <div className="row">
                      <div className="col col-12">
                        <table
                          className="table"
                          style={{
                            marginTop: "15px",
                            textAlign: "center",
                            marginLeft: "30px",
                            width: "95%",
                            marginBottom: "15px",
                          }}
                        >
                          <thead>
                            <tr>
                              {tableHeader.map(t => <th>{t}</th>)}
                            </tr>
                          </thead>
                          <tbody> 
                          {noData === "No Others" ?
                          <tr>
                                <td></td>
                                <td>No Subscription</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                <button
                                  className="btn btn-sm"
                                  style={{
                                    backgroundColor: "#008080",
                                    paddingLeft: "30px",
                                    color: "#ffffff",
                                    paddingRight: "30px",
                                    fontWeight: "700",
                                    borderRadius: "20px",
                                  }}
                                >
                                  INQUIRE
                                </button>
                              </td>
                                </tr> :
                            fields.map((m, key) => (
                            m.Solution !== "Other"? setNoData("No Others") : (
                            <tr key={key}>
                              <td>{m.ATC}</td>
                              <td>{m.Product}</td>
                              <td>
                              {getStatus(m.EndDate, m.StartDate)}
                              </td>
                              <td style={{ fontWeight: "bold" }}>-</td>
                              <td>{m.EndDate}</td>
                              <td>
                                <button
                                  className="btn btn-sm"
                                  style={{
                                    backgroundColor: "#008080",
                                    paddingLeft: "30px",
                                    color: "#ffffff",
                                    paddingRight: "30px",
                                    fontWeight: "700",
                                    borderRadius: "20px",
                                  }}
                                >
                                  RENEW
                                </button>
                              </td>
                            </tr>
                            )
                          )) 
                        }  
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
  )
}