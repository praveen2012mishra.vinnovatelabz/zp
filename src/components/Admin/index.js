import React, { useState } from "react"

import styles from "./style.module.scss"

import Tab from "../Tab"
import AdminPageSearch from "./AdminPageSearch"
import AdminGroupMgmtTab from "./AdminGroupMgmtTab/index"
import Admin2FAAuthMgmtTab from "./Admin2FAAuthMgmtTab"
import AdminEnforceSingleSessionMgmtTab from "./AdminEnforceSignleSessionMgmtTab"
import AdminSettingsTab from "./AdminSettingsTab"
import AdminLogsTab from "./AdminLogsTab"
import AdminSolutionMgmtTab from "./AdminSolutionMgmtTab"
import { ToggleButton } from "react-bootstrap"
import Axios from "axios"

const AdminPage = () => {
  const [activeTab, setActiveTab] = useState(0)

  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
    }
  }
  return (
    <>
      <div className={`${styles.header}`}>
        <h1 className={`text-center ${styles.title}`}>Administration</h1>

        <Tab
          items={[
            "Group Management",
            "Solution Management",
            "Enforced Single Session Management",
            "Two Factor Authentication Management",
            "Settings",
            "Logs",
          ]}
          activeTab={activeTab}
          onTabChange={onTabChange}
        />
      </div>
      <div className={`py-4 px-lg-5 ${styles.contentWrapper}`}>
        {activeTab === 0 ? (
            <AdminGroupMgmtTab />
        ) : null}
        {activeTab === 1 ? (
          <>
          <AdminSolutionMgmtTab />
          </>
        ): null} 
        {activeTab === 2 ? (
          <>
            <AdminPageSearch />
            <AdminEnforceSingleSessionMgmtTab />
          </>
        ) : null}
        {activeTab === 3 ? (
          <>
            <AdminPageSearch />
            <Admin2FAAuthMgmtTab/>
          </>
        ) : null}
        {activeTab === 4 ? (
          <AdminSettingsTab />
        ) :null}
        {
          activeTab ===5 ?(
          <AdminLogsTab data={[]} />
          ):null
        }
      </div>
    </>
  )
}

export default AdminPage
