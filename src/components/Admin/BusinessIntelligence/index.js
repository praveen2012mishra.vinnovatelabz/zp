import React from "react"

function BusinessIntelligence({ component: Component, ...rest }) {
  return <Component {...rest} />
}

export default BusinessIntelligence
