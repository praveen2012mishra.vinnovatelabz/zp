import React, { useState } from "react"
import DataManagement from "../../../../../assets/images/Asset 15.png"
import DataFeed from "../../../../../assets/images/Asset 17.png"
import DSTIcon from "../../../../../assets/images/Asset 18.png"
import CustomizeReport from "../../../../../assets/images/Asset 19.png"
import RPAIcon from "../../../../../assets/images/Asset 20.png"
import Styles from "./style.module.scss"
import { Container, Row, Col } from "reactstrap"
import { SubHeader } from "../../../../ui/subHeader"

const circleData = [
  {
    head: "Data Management and Integration",
    icon: DataManagement,
    description: `Reports tailored to the requirements of
      our clients, including analysis of data
      from non-ZP sources.`,
  },
  {
    head: "Robotic Process Automation (RPA)",
    icon: RPAIcon,
    description: `Reports tailored to the requirements of
      our clients, including analysis of data
      from non-ZP sources.`,
  },
  {
    head: "Customised Reports",
    icon: CustomizeReport,
    description: `Reports tailored to the requirements of
      our clients, including analysis of data
      from non-ZP sources.`,
  },
  {
    head: "Datafeed",
    icon: DataFeed,
    description: `Reports tailored to the requirements of
      our clients, including analysis of data
      from non-ZP sources.`,
  },
  {
    head: "Data Skills Training",
    icon: DSTIcon,
    description: `Reports tailored to the requirements of
      our clients, including analysis of data
      from non-ZP sources.`,
  },
]
function ServiceOffering(props) {
  const [show, setShow] = useState("")
  return (
    <div className={Styles.main}>
      {/* <TabDiv /> */}
      <SubHeader
        title="Service Offering"
        subtitle="Offer services to meet the business intelligence needs."
      />
      <Container className={`text-center ${Styles.diagramContainer}`}>
        <span className={Styles.centerHead}>
          <span>Services</span>
        </span>

        <ul className={Styles.businnessContainer}>
          {/* <img src={Pentagon} className={Styles.pentagon} /> */}
          {circleData.map((item, key) => (
            <li key={key} style={{ textAlign: "center" }}>
              <span
                role="button"
                onMouseEnter={() => setShow(key)}
                onMouseLeave={() => setShow(false)}
                key={key}
                heading={item.head}
              >
                {/* <Link
                  to={
                    key === 1
                      ? "/auth/commercialExp/InfraredSales/doctorPrescription"
                      : props.location.pathname
                  }
                  state={{ buttonItems: doctorButtons, heading: heading }}
                  style={{ color: "#094658" }}
                > */}
                <Circles icon={item.icon} />
                {/* </Link> */}
                <div
                  className={Styles.item_description}
                  style={{
                    position: "absolute",
                    transition: "all 0.2s",
                    zIndex: "-3",
                    transform: key === show ? "scale(1)" : "scale(0)",
                  }}
                >
                  <span>
                    <div className={Styles.underline}>
                      <p style={{ fontWeight: "bold" }}>{item.head}</p>
                    </div>
                    <p>{item.description}</p>
                  </span>
                </div>
              </span>
            </li>
          ))}
        </ul>
      </Container>
    </div>
  )
}
const Circles = props => (
  <div className={Styles.businessBorder}>
    {/* <svg style={{ height: "3rem" }}>{props.icon}</svg> */}
    <img src={props.icon} style={{ height: "3rem" }} />
  </div>
)

export default ServiceOffering
