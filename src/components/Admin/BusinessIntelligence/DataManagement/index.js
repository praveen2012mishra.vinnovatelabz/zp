import React, { useState, useEffect } from "react"
import TabOptions from "../../../ui/taboption/index"
import styles from "./style.module.scss"
import ServiceOffering from "./ServiceOffering/index"
import Tab from "../../../Tab"
import HeaderCommonComponent from '../../../ui/HeaderCommon/HeaderCommon'
import bussinessBanner from '../../../../assets/images/bussiness_intelligent.png'

function DataManagement(props) {
  const lime = "#c3d422"
  const white = "#ffffff"
  const [tabSelected, setTab] = useState("")
  const tabs = [
    "Data Management and Integration",
    "Robotic Process Automation (RPA)",
    "Customised Reports",
    "Datafeed",
    "Data Skills Training",
  ]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
}
  const { location, buttonItems, heading } = props
  return (
    <HeaderCommonComponent height={'15%'} HeaderImage={bussinessBanner} heading={'Business Intelligence'} previousProps={props} tabs={tabs} tabSelected={tabSelected} onTabChange={onTabChange} ><ServiceOffering {...props} /></HeaderCommonComponent>
    
  )
}

export default DataManagement
