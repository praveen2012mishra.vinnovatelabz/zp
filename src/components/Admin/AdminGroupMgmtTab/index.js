import React, { useState, useEffect } from "react"
import { Table, Container, Row, Col } from "react-bootstrap"
import { Plus, PencilSquare, Trash } from "react-bootstrap-icons"
import { connect } from "react-redux"
import cogo from "cogo-toast"
import LoadingOverlay from "react-loading-overlay"
import { changeSingleSessionStatus } from "../../../services/Api/auth.api"
import {options} from "./constant"
import AdminPageSearch from "../AdminPageSearch"
import styles from "./style.module.scss"
import ToggleButton from "../../ui/ToggleButton"
import GropuAddEditModal from "../GroupAddEditModal"
import {
  getAllGroupData,
  deleteGroupData,
  toggle,
} from "../../../services/Redux/actions/admin"
import Axios from "../../../../axiosConfig"
import { set } from "js-cookie"
const _= require('underscore')
const AdminGroupMgmtTab = ({
  data,
  err,
  getAllGroupMgmtData,
  deleteGroupMgmtData,
  notification,
  switched,
}) => {
  const [modalOpen, toggleModalOpen] = useState(false)
  const [getData, setGetData] = useState({})
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    getAllGroupMgmtData()
  }, [])
  const [fromEdit, setFromEdit] = useState(false)
  const [update,setUpdate]= useState("")
  const modalOnClose = () => {
    toggleModalOpen(false)
  }

  const modalOnSaveAndExit = data => {
     // do something to save the data
    let dataObj = {
      group_name: data.groupName,
      mappingId: Date.now(),
      type: data.type,
      country: data.country,
      options: data.data,
    }
    console.log(data.data);
    if (fromEdit === false) {
      Axios.post("/group/createGroup", { group_name: data.groupName })
      Axios.post("/group/updateGroupOptions", dataObj)
      window.location.reload(false)
      // getAllGroupMgmtData()
    } else {
      if (fromEdit.group_name!==data.groupName){
        Axios.post("/group/updateGroup",{_id:fromEdit._id,group_name:data.groupName})
      }
      Axios.post("/group/updateGroupOptions", {
        ...dataObj,
        mappingId: fromEdit.mappingId,
      })
      // .then((res)=>window.location.reload(false))
    }
    toggleModalOpen(false)
  }

  // delete handler
  const onDelete = (id, username, groupName) => {
    setLoading(true)
    // group name and username is required for saving in activity log
    // currently username is hardcoded
    // TODO: update it to use current username in future
    deleteGroupMgmtData(id, "CPhoa", groupName)
  }

  // handle error
  if (err) {
    if (loading) setLoading(false)
  }

  //handle toggle
  const handleStatus = (mode, name, key, button) => {
    setLoading(true)
    switched(button, name, !mode, key)
  }
  // handle notifications
  if (notification.notify) {
    if (notification.type === "error") {
      cogo.error(notification.message)
      if (loading) setLoading(false)
    } else if (notification.type === "success") {
      cogo.success(notification.message)
      if (loading) setLoading(false)
    }

    // prevent showing notification in future render without any
    // actions
    notification.notify = false
  }
  // structurize the data from the raw data array
  if (data.length > 0) {
    data = data.map(data => ({
      _id: data._id,
      name: data.group_name,
      mappingId: data.mappingId ? data.mappingId : "",
      enforcedSingleSessionStatus: data.single_session_enforced,
      enforced2FAStatus: data.two_factor_authentication_enforced,
      type:data.type,
      country:data.country,
      options:options
    }))
    // check if loading is true, otherwise it is causing infinite render loops
    if (loading) setLoading(false)
  }
  return (
    <Container fluid>
      <AdminPageSearch />
      {modalOpen ? (
        <GropuAddEditModal
          open={modalOpen}
          onClose={modalOnClose}
          onSaveAndExit={modalOnSaveAndExit}
          fromEdit={fromEdit}
          getData={getData}
        />
      ) : (
          ""
        )}

      <Row className="justify-content-between mt-5 mb-3">
        <Col>
          <p className="h1">
            <b>‘Singapore’</b>
          </p>
        </Col>
        <Col>
          <button
            className={`btn rounded-pill ${styles.btnRight}`}
            onClick={() => {
              toggleModalOpen(true)
              setFromEdit(false)
            }}
          >
            <span>
              <Plus />
            </span>
            <span>ADD GROUP</span>
          </button>
        </Col>
      </Row>

      <LoadingOverlay active={loading} spinner>
        <Table className={styles.table}>
          <thead>
            <tr>
              <th>Group Name</th>
              <th>Enforced Single Session Status</th>
              <th>Enforced 2FA Status </th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((d, key) => (
                <tr key={d._id}>
                  <td>{d.name}</td>
                  <td>
                    <div className="d-flex justify-content-center align-items-center">
                      <ToggleButton
                        checked={d.enforcedSingleSessionStatus}
                        onChange={() => {
                          // setLoad(!load)
                          handleStatus(
                            d.enforcedSingleSessionStatus,
                            d.name,
                            key,
                            "session"
                          )
                        }}
                      />
                    </div>
                  </td>
                  <td>
                    <div className="d-flex justify-content-center align-items-center">
                      <ToggleButton
                        checked={d.enforced2FAStatus}
                        onChange={() =>
                          handleStatus(d.enforced2FAStatus, d.name, key, "2FA")
                        }
                      />
                    </div>
                  </td>
                  <td>
                    {/* edit button ################################################################ */}
                    <div className="d-flex justify-content-center align-items-center">
                      <span
                        role="button"
                        onClick={() => {
                          Axios.get(`/group/fetchGroupOptions/${d.mappingId}`)
                          .then(res => {
                            setFromEdit({ mappingId: d.mappingId,
                              _id:d._id,group_name:d.name  })
                            _.debounce(toggleModalOpen(true),2000)
                          }).catch(err => {
                            let payload={
                              group_name: d.name,
                              mappingId: d.mappingId,
                              type: d.type,
                              country: d.country,
                              options: d.options,
                            }
                            console.log(payload);
                            Axios.post(`/group/updateGroupOptions`, payload).then(res=>{
                              setFromEdit({ 
                                mappingId: d.mappingId,
                                _id:d._id,group_name:d.name })
                                _.debounce(toggleModalOpen(true),2000)
                              }).catch(err=>console.log(err))
                          })
                        }}
                      >
                        <PencilSquare />
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex justify-content-center align-items-center">
                      <Trash
                        className={styles.cursorPointer}
                        onClick={() => onDelete(d._id, "username", d.name)}
                      />
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </LoadingOverlay>
    </Container>
  )
}

const mapStateToProps = state => ({
  data: state.admin.group,
  err: state.admin.err,
  notification: state.notification,
})

const mapDispatchToProps = dispatch => ({
  getAllGroupMgmtData: () => dispatch(getAllGroupData()),
  deleteGroupMgmtData: (id, username, groupName) =>
    dispatch(deleteGroupData(id, username, groupName)),
  switched: (button, name, type, key) =>
    dispatch(toggle(button, name, type, key)),
})

// export default AdminGroupMgmtTab
export default connect(mapStateToProps, mapDispatchToProps)(AdminGroupMgmtTab)
