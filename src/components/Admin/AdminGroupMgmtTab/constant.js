export const options = [
    {
        type: "insider",
        name: "insider",
        items: [
            { type: "Sales Report", elements: [] },
            { type: "Sales Flash", elements: [] },
            { type: "Inventory Balance", elements: [] },
            { type: "Ask ZiP", elements: [] },
            { type: "Detailed Reports", elements: [] },
            { type: "Custom Reports", elements: [] },
        ],
    },
    {
        type: "clinicalReach",
        name: "clinical reach",
        items: [
            { type: "Inbound", elements: [] },
            { type: "Distribution", elements: [] },
            { type: "Inventory", elements: [] },
            { type: "Budget & Expense", elements: [] },
            { type: "Detailed Reports", elements: [] },
            { type: "Custom Reports", elements: [] },
        ],
    },
    {
        type: "commercialExcellence",
        name: "Commercial Excellence",
        items: [
            {
                type: "Infrared", elements: [
                    { lable: "Promotion Optimisation", url: "" },
                    { lable: "Product Bundling", url: "" },
                    { lable: "Patient Journey", url: "" },
                    { lable: "Doctor Prescription Behaviour", url: "" },
                    { lable: "Tender Utilisation", url: "" },
                    { lable: "Order Timing", url: "" },
                    { lable: "Price Sensitivity", url: "" },
                    { lable: "Infrared Base", url: "" },
                ]
            },
            { type: "Investigator", elements: [] },
        ],
    },
    {
        type: "supplyChainAnalytics",
        name: "Supply Chain Analytics",
        items: [{ type: "Control Tower Reports", elements: [] }],
    },
    {
        type: "businessIntelligenceServices",
        name: "Business Intelligence Services",
        items: [{ type: "Data Management", elements: [] }],
    },
    {
        type: "Internal",
        name: "Internal",
        items: [
            { type: "Management Overview", elements: [] },
            { type: "Finance", elements: [] },
            { type: "Procurement", elements: [] },
            { type: "HR", elements: [] },
            { type: "Operations", elements: [] },
            { type: "QBR", elements: [] },
            { type: "CRM", elements: [] },
            { type: "IMS", elements: [] },
        ],
    },
]