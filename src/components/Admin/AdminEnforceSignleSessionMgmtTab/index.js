import React, { useState,useEffect } from "react"
import { Table, Container, Row, Col } from "react-bootstrap"
import { ArrowClockwise } from "react-bootstrap-icons"
import styles from "./style.module.scss"
import { connect } from "react-redux"
import cogo from "cogo-toast"
import LoadingOverlay from "react-loading-overlay"
import {
  getAllSessionData,
  resetSessionCount
} from "../../../services/Redux/actions/adminSession"

const AdminEnforceSingleSessionMgmtTab = ({
  data=[],
  getAllSessionMgmtData,
  resetSessionCountData,
  err,
  notification,
}) => {
  useEffect(() => {
    getAllSessionMgmtData()
  }, [])
  const [loading, setLoading] = useState(true)
  const [checked, setChecked] = useState({})
  // handle error
  if (err) {
    if (loading) setLoading(false)
  }
  // handle notifications
  if (notification.notify) {
    if (notification.type === "error") {
      cogo.error(notification.message)
      if (loading) setLoading(false)
    } else if (notification.type === "success") {
      cogo.success(notification.message)
      if (loading) setLoading(false)
    }

    // prevent showing notification in future render without any
    // actions
    notification.notify = false
  }
  // structurize the data from the raw data array
  if (data.length > 0) {
    data = data.map((data, key) => ({
      _id: key,
      name: data,
      selected: Object.keys(checked).length === 0 ? false : checked[key],
    }))
    // check if loading is true, otherwise it is causing infinite render loops
    if (loading) setLoading(false)
  }
  const initateReset = () => {
    let users = [];
    for (let keys in checked) {
      if (checked[keys]!==false) {
        users.push(checked[keys])
      }
    }
    resetSessionCountData(users);
    setChecked({});
  }
  return (
    <Container fluid>
      <Row className="justify-content-between mt-5 mb-3">
        <Col>
          <p className="h1">
            <b>‘Singapore’</b>
          </p>
        </Col>
        <Col>
          <button className={`btn rounded-pill ${styles.btnRight}`}
            onClick={() => initateReset()}
          >
            <span>
              <ArrowClockwise />
            </span>
            <span>RESET SESSION COUNT</span>
          </button>
        </Col>
      </Row>

      <LoadingOverlay active={loading} spinner>
        <Table className={styles.table}>
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {data.map((d, key) => (
              <tr key={key}>
                <td role="button" key={key}
                  className={d.selected == d.name ? styles.selected : "hello"}
                  onClick={() => {
                    checked.hasOwnProperty(d._id) ?
                      checked[d._id] === false ? setChecked({ ...checked, [d._id]: d.name }) :
                        setChecked({ ...checked, [d._id]: false })
                      :
                      setChecked({ ...checked, [d._id]: d.name })
                  }}
                >{d.name}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </LoadingOverlay>
    </Container>
  )
}
const mapStateToProps = state => ({
  data: state.adminSession.session,
  err: state.adminSession.err,
  notification: state.notification,
})

const mapDispatchToProps = dispatch => ({
  getAllSessionMgmtData: () => dispatch(getAllSessionData()),
  resetSessionCountData: (users) => (
    dispatch(resetSessionCount(users))),
})

// export default AdminGroupMgmtTab
export default connect(mapStateToProps, mapDispatchToProps)(AdminEnforceSingleSessionMgmtTab)
