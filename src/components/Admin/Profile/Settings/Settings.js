import React, { useEffect } from "react"
import { Container, Row, Col } from "react-bootstrap"
import styles from "./style.module.scss"
import Account from "../../../../assets/images/Account.png"
function Settings({
  profileData,
  submitProfile,
  changeProfileData,
  handleValidation,
  error,
  isValidMail,
}) {
  return (
    <div className={styles.main}>
      <Container>
        <Row>
          <div className={styles.mt_10}>Profile Picture</div>
        </Row>
        <Row style={{ marginBottom: "50px" }}>
          <Col md={2}>
            <img
              src={profileData.profile_pic ? profileData.profile_pic : Account}
              alt="profile.png"
              className={styles.image}
            />
          </Col>
          <Col>
            <label style={{ marginTop: "1.2em" }}>
              <input
                type="file"
                style={{ display: "none" }}
                name="profile_pic"
                onChange={e => changeProfileData(e)}
              />
              <a className={styles.inputFile}>Choose Image</a>
            </label>
          </Col>
        </Row>
        <div className={styles.pd_14}>
          <div className={styles.headline}>Full Name</div>
          <input
            name="name"
            className={styles.input}
            value={profileData.name}
            onChange={e => changeProfileData(e)}
            onBlur={handleValidation}
            style={{ border: error["name"] ? "1px solid red" : "" }}
          />
          {error["name"] ? (
            <div style={{ color: "red" }}>{error["name"]}</div>
          ) : (
            ""
          )}
        </div>
        <div className={styles.pd_14}>
          <div className={styles.headline}>Email</div>
          <input
            name="email"
            className={styles.input}
            value={profileData.email}
            onChange={e => changeProfileData(e)}
            onBlur={handleValidation}
            style={{ border: error["email"] ? "1px solid red" : "" }}
          />
          {error["email"] ? (
            <div style={{ color: "red" }}>{error["email"]}</div>
          ) : !isValidMail ? (
            <div style={{ color: "red" }}>Email Id is Invalid.</div>
          ) : (
            ""
          )}
        </div>
        <Row>
          <Col md={4} style={{ padding: "24px", textAlign: "right" }}>
            <button
              className={styles.btn}
              onClick={e => submitProfile(e, "settings")}
            >
              SAVE
            </button>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Settings
