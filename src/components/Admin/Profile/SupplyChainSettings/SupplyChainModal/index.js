import React, { useEffect, useState } from 'react'
import { Modal, Row, Col, Container, Dropdown, Table, Input } from 'react-bootstrap';
import styles from "./style.module.scss";
import { X } from "react-bootstrap-icons";
import { FaCheck, FaTimes } from 'react-icons/fa';
import Simplebar from "simplebar-react"
import { saveUserRoles } from '../../../../../services/Api/supplyChain.api';



const tableHeaderList = [
    { label: 'Module' },
    { label: 'Read' },
    { label: 'Write' },
    { label: 'Approve' },
]

const dummyTableData = new Array(3).fill({
    read: Math.random() < 0.5,
    write: Math.random() < 0.5,
    approve: Math.random() < 0.5
}).map((e, i) => ({ ...e, label: tableHeaderList[i].label }))

const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
        ref={ref}
        onClick={onClick}
        className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
    >
        {children}
    </button>
))


const SupplyChainModal = ({ show, toggleModal, groups, userData, roles }) => {
    const [selectedUser, setSelectedUser] = React.useState()
    const [selectedRole, setSelectedRole] = React.useState()
    const [selectedGroup, setSelectedGroup] = React.useState()
    const [userRoles, setUserRoles] = React.useState([])
    const [searchItem, setSearchItem] = React.useState(groups)
    const [updateRoles, setUpdateRoles] = React.useState([])
    

    useEffect(() => {
        const _roles = roles;
        setUserRoles(_roles ? _roles : []);
        
    }, [roles])

    const getUserRoles=(menuItem)=>{
        if(menuItem.length>0){
            let mapArray=menuItem.map((s,i) => {
                s.type=s.type.trim()
                return s
            });
            let filterArray=mapArray.filter(s =>s.type.length>0 );
            let updateArray=Object.assign([],filterArray)
            setUpdateRoles(updateArray);
        }
        
    }

    const handleSubmit = async () => {
        if (selectedUser && selectedGroup && selectedRole) {
            const payload = {
                userName: selectedUser._id,
                groupName: selectedGroup.group_name,
                roleName: selectedRole.type,
                email: selectedUser.email
            }
            const response = await saveUserRoles(payload);
            toggleModal(false)
        }
    }

    return <Modal size="lg" centered show={show} dialogClassName={styles.modal}>
        <Modal.Header className={styles.header}>
            <Container fluid>
                <Row className="justify-content-between align-items-center w-100">
                    <Col sm={6}>
                        <div className='h2'>User Information</div>
                    </Col>
                    <Col sm={6} className="row pr-0">
                        { /* <div className="row col-5"> */}
                            <button
                                className={`btn col-8 rounded-pill d-block ml-auto ${styles.btn}`}
                                onClick={() => handleSubmit()}
                            >
                                SAVE & EXIT
                            </button>
                            <button
                                className={`btn col-2 ${styles.closeBtn}`}
                                onClick={() => toggleModal(false)}
                            >
                                <X />
                            </button>
                        { /* </div> */}
                    </Col>
                </Row>
            </Container>
        </Modal.Header>
        <Simplebar className={styles.modal_height} autoHide="false">
            <Modal.Body className={styles.modal__body}>
                <div className='row col-sm-6'>
                    <div className='col-sm-12'>
                        <p className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}>
                            group name
                        </p>
                        <Dropdown
                            className={styles.dropdown}
                            onSelect={(_, e) => {
                                setSelectedGroup(groups.find(grp => grp.group_name.toLowerCase() === e.target.innerText.toLowerCase()))
                            }}
                        >
                            <Dropdown.Toggle as={CustomDropdownToggle}>
                                {selectedGroup ? selectedGroup.group_name : 'Select Group'}
                            </Dropdown.Toggle>
                            <Dropdown.Menu className={styles.dropdown_height}>
                            <Dropdown.Header 
                            // style={{position: 'sticky', width: '95%',
                            // margin: 'auto',
                            // top: 0,
                            // display: 'flex',
                            // background: '#ffffff'
                            //}}
                            >
                            <input type="text" placeholder="Search.." id="myInput" className={[styles.input].join(" ")} onChange={e => {
                                if(!e.target.value) {
                                setSearchItem(groups)
                                     return 
                                }    
                                else 
                                {
                                const items = groups.filter((data) => {
                                     if(data.group_name.toLowerCase().includes(e.target.value.toLowerCase()))
                                     return data
                                })
                                setSearchItem(items)
                            }
                            }} autoComplete="off"/>
            
                           </Dropdown.Header>   
                            {searchItem?.map(grp =>
                                    <Dropdown.Item
                                        className="text-uppercase"
                                    >
                                        {grp.group_name}
                                    </Dropdown.Item>
                                )}
                            </Dropdown.Menu>
                        </Dropdown>
                    </div>
                </div>
                <hr />
                <Row>
                    <div className='col-sm-6 my-1'>
                        <Row>
                            <Col xs={2} style={{paddingLeft:"2em"}}>User</Col>
                            <Col xs={10}>
                                <Dropdown
                                    className={styles.dropdown}
                                    onSelect={(_, e) => {
                                        setSelectedUser(userData.find(user => user.name.toLowerCase() === e.target.innerText.toLowerCase()))
                                    }}
                                >
                                    <Dropdown.Toggle as={CustomDropdownToggle}>
                                        {selectedUser ? selectedUser.name : "Select User"}
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_height}>
                                        {userData?.map(user =>
                                            <Dropdown.Item
                                                className="text-uppercase"
                                            >
                                                {user.name}
                                            </Dropdown.Item>
                                        )}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Col>
                        </Row>
                    </div>
                    <div className="col-sm-6 my-1">
                        <Row>
                            <Col xs={2} style={{paddingLeft:"2em"}}>Role</Col>
                            <Col xs={10} onMouseEnter={()=>getUserRoles(userRoles)}>
                                <Dropdown
                                    className={styles.dropdown}
                                    onSelect={(_, e) => {
                                        e.persist()
                                        setSelectedRole(userRoles.find(r => r.type.toLowerCase() === e.target.innerText.toLowerCase()))
                                        
                                    }
                                    }
                                >
                                    <Dropdown.Toggle as={CustomDropdownToggle}>
                                        {selectedRole ? selectedRole.type : 'Select Role'}
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className={styles.dropdown_height}>
                                        {updateRoles ? updateRoles.map(role =>
                                            <Dropdown.Item className="text-uppercase">
                                                {role.type}
                                            </Dropdown.Item>
                                        ):null}
                                    </Dropdown.Menu>
                                </Dropdown>
                            </Col>
                        </Row>
                    </div>
                </Row>
                <div className='row m-0'>
                    <Table className={styles.table}>
                        <thead>
                            <tr>
                                {tableHeaderList.map(e => {
                                    if (!e.children) {
                                        return <th>{e.label}</th>
                                    } else {
                                        return <th>
                                            <tr className='d-flex justify-content-center'>{e.label}</tr>
                                            <tr className='row col-12 px-0 mx-0'>
                                                {e.children.map(child => <th className='col-4'>{child}</th>)}
                                            </tr>
                                        </th>
                                    }
                                })}
                            </tr>
                        </thead>
                        <tbody>
                            {selectedRole?.permissions.map((e, i) => {

                                const perm = e.permission.toLowerCase().replace(/ /g, '');
                                return <tr key={i}>
                                    <td style={{ textTransform: 'capitalize' }}>{e.module}</td>
                                    <td>{perm === 'read' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                    <td>{perm === 'write' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                    <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                </tr>
                            })}
                        </tbody>
                    </Table>
                </div>
            </Modal.Body>
        </Simplebar>
    </Modal>
}

export default SupplyChainModal;