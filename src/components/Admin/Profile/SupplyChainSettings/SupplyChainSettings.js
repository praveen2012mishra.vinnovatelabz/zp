import React, { useState, useEffect } from 'react';
import { Table, Row, Col, Button, Dropdown } from "react-bootstrap";
import styles from "./style.module.scss";
import { FaCheck, FaTimes } from 'react-icons/fa';
import SupplyChainModal from './SupplyChainModal'
import { getGroups, getGroupUsers } from '../../../../services/Api/supplyChain.api';
import Simplebar from "simplebar-react"
import { stubTrue } from 'lodash';


const tableHeaderList = [
    { label: 'User ID' },
    { label: 'Full Name' },
    { label: 'On Zip' },
    { label: 'User Role' },
    { label: 'Product Master', children: ['Read', 'Write', 'Approve'] },
    { label: 'Product Restrictions', children: ['Read', 'Write', 'Approve'] },
    { label: 'Forecasts', children: ['Read', 'Write', 'Approve'] }
]

const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
        ref={ref}
        onClick={onClick}
        className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
    >
        {children}
    </button>
))


const SupplyChainSettings = (props) => {
    const [showModal, setShowModal] = useState(false);
    const [tableData, setTableData] = useState([]);
    const [roles, setRoles] = useState([]);
    const [users, setUsers] = useState([]);
    const [groups, setGroups] = useState([]);
    const [selectedGroup, setSelectedGroup] = useState()
    const [loading, setLoading] = useState(false)
    
    useEffect(() => {
        const groupData = async () => {
            const groups = await getGroups()
            setGroups(groups.data)
            setSelectedGroup(groups.data[0])
        }
        groupData()
    }, [])

    useEffect(() => {
        if(selectedGroup){
            const loadTableData = async () => {
                setLoading(true) 
                try{
                const values =  await getGroupUsers(selectedGroup.group_name)
                const temp = values.rolePermissions.find(e => e.solution == "Supply Chain")
                setTableData(values.userData)
                setRoles(temp?temp.roles:[])
                setUsers(values.userData);
                setLoading(false)
                } catch {
                    setLoading(false)
                }
                }
                loadTableData()
        }
    }, [selectedGroup])
    return <div>
        <SupplyChainModal show={showModal} toggleModal={setShowModal} groups={groups} userData={users} roles={roles}></SupplyChainModal>
        <Row className="justify-content-between mt-5 mb-3">
            <Col xs='3'>
                <Dropdown
                    className={styles.dropdown}
                    onSelect={(_, e) => {
                        setSelectedGroup(groups.find(grp => grp.group_name.toLowerCase() === e.target.innerText.toLowerCase()))
                    }}
                >
                    <Dropdown.Toggle as={CustomDropdownToggle}>
                    {selectedGroup ? selectedGroup.group_name : 'Select Group'}
                    </Dropdown.Toggle>
                    <Dropdown.Menu className={styles.dropdown_height}>
                    {groups?.map(grp => <Dropdown.Item
                        className="text-uppercase"
                    >
                        {grp.group_name}
                        </Dropdown.Item>
                        )}
                        
                    </Dropdown.Menu>
                </Dropdown>
            </Col>
            <Col className='d-flex text-right justify-content-end'>
                <Button
                    className={`align-self-center btn rounded-pill float-right mr-3 ${styles.btnRight}`}
                    onClick={() => {
                        setShowModal(true);
                    }}
                >
                    Add/Edit Users
                </Button>
            </Col>
        </Row>
        {!loading ? tableData.length ?
        <Simplebar style={{ maxHeight: "100%" }} autoHide="false">
        <Table className={styles.table}>
            <thead>
                <tr>
                    {tableHeaderList.map((e, key)=> {
                        if (!e.children) {
                            return <th key={key}>{e.label}</th>
                        } else {
                            return <th key={key}>
                                <tr className='d-flex justify-content-center'>{e.label}</tr>
                                <tr>
                                    {e.children.map(child => <th>{child}</th>)}
                                </tr>
                            </th>
                        }
                    })}
                </tr>
            </thead>
             
            <tbody>
                {tableData.map((data, i) => {
                    const e = data;
                    const rolesData = e.roles && e.roles.length ? e.roles[0].role : null
                    
                    const rolePermissions = roles?.find(e => e.type.toLowerCase().replace(/ /g, "") === rolesData?.toLowerCase().replace(/ /g, ""))
                    const productMaster = rolePermissions?.permissions.find(e => e.module.toLowerCase().replace(/ /g, "") === "productmaster")
                    const productRestrictions = rolePermissions?.permissions.find(e => e.module.toLowerCase().replace(/ /g, "") === "productrestrictions")
                    const forecasts = rolePermissions?.permissions.find(e => e.module.toLowerCase().replace(/ /g, "") === "forecasts")
                    return <tr key={i}>
                        <td>{e._id}</td>
                        <td>{e.name}</td>
                        <td>{e.onZip ? "True" : "False"}</td>
                        <td>{e.onZip ? (e.roles ? e.roles[0].role : "NOT ASSIGNED") : null}</td>
                        <td>
                        {e.onZip ? ( e.roles ? (() => {
                            const perm = productMaster?.permission.toLowerCase().replace(/ /g, '');
                            return <div className={styles.iconWrapperRow}>
                            <td>{perm === 'read' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            <td>{perm === 'write' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            </div>
                        })()
                           : null) : null }
                        </td>
                        <td>
                        {e.onZip ? ( e.roles ? (() => {
                            const perm = productRestrictions?.permission.toLowerCase().replace(/ /g, '');
                            return <div className={styles.iconWrapperRow}>
                            <td>{perm === 'read' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            <td>{perm === 'write' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            </div>
                        })()
                            : null ) : null }
                        </td>
                        <td>
                        { e.onZip ? ( e.roles ?  (() => {
                            const perm = forecasts?.permission.toLowerCase().replace(/ /g, '');
                            return <div className={styles.iconWrapperRow}>
                            <td>{perm === 'read' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            <td>{perm === 'write' || perm === 'readandwrite' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                            </div>
                        })()
                            : null ) : null }
                        </td>
                    </tr>
                })}
            </tbody>
        </Table>
        </Simplebar>
        : <div className="text-center h4">No Data Found</div> : <div className="text-center h4">Loading...</div>}
    </div>
}

export default SupplyChainSettings;