import React, { useState, useEffect } from "react"
import styles from "./style.module.scss"
import Settings from "./Settings/Settings"
import Locale from "./TimeAndLocale/Locale"
import Tab from "../../Tab"
import axios from "../../../../axiosConfig";
import SupplyChainSettings from './SupplyChainSettings/SupplyChainSettings';
import Cookie from "js-cookie"
function ProfileIndex(props) {
  const lime = "#c3d422"
  const white = "#ffffff"
  const [tabSelected, setTab] = useState(0)
  const [profileData, setProfileData] = useState({})
  const [updateMessage, setUpdateMessage] = useState("")
  const [timeLocale, setTimeLocale] = useState({
    country: "Singapore",
    time_zone: "SGT (+08:00)",
  })
  const [error, setError] = useState({})
  const [isValidMail, setIsValidMail] = useState(true)
  const tabs = ["Settings", "Time and Locale", "Supply Chain Settings"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
  }
  function getBase64(file, cb) {
    let reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = function () {
      cb(reader.result)
    }
    reader.onerror = function (error) {
      console.log("Error: ", error)
    }
  }
  const handleValidation = e => {
    const { name, value } = e.target
    let err = {}
    if (!value) {
      err = { ...error, [name]: "Please enter a valid input." }
    } else {
      err = { ...error, [name]: "" }
    }
    setError(err)
  }
  useEffect(() => {
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    const userName = samlToken ? samlToken.user.name_id:""
    
    axios.get(`/users/getUserProfile/${userName}`).then(res => {
      if (!res.data.userProfile) {
        setProfileData({})
      } else {
        setProfileData(res.data.userProfile)
      }
    })
  }, [])
  const submitProfile = async (e, formName) => {
    e.preventDefault()
    let isUpdateValid = false
    for (let item in error) {
      if (error[item]) {
        isUpdateValid = false
        break
      } else {
        isUpdateValid = true
      }
    }
    if (
      (isUpdateValid && isValidMail && formName === "settings") ||
      formName === "locale"
    ) {
      await axios
        .post(
          `/users/updateUserProfile`,
          { profileData },
          {
            headers: {
              "Content-Type": "application/json",
            },
          }
        )
        .then(res => {
          setUpdateMessage(res.data.message)
        })
    }
  }

  function ValidateEmail(mail) {
    if (
      /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(mail)
    ) {
      return true
    }
    return false
  }

  const changeProfileData = (e, val) => {
    const { name, value, files } = e.target
    let time_locale = { ...timeLocale }
    if (name === "name" || name === "email") {
      let obj = { ...error, [name]: "" }
      setError(obj)
      if (name === "email" && value.length > 0) {
        if (ValidateEmail(value)) {
          setIsValidMail(true)
        } else {
          setIsValidMail(false)
        }
      }
    }
    if (name === "profile_pic") {
      getBase64(files[0], result => {
        let pData = { ...profileData, profile_pic: result }
        setProfileData(pData)
      })
    }
    if (name === "country" || name === "time_zone") {
      time_locale = {
        ...time_locale,
        [name]: val,
      }
      setTimeLocale(time_locale)
      let pData = { ...profileData, time_locale }
      setProfileData(pData)
    }
    setProfileData(data => {
      return { ...data, [name]: value }
    })
  }
  const { location, buttonItems, heading } = props
  return (
    <div
      style={{
        // backgroundImage: `url(${HeaderImage})`,
        backgroundSize: "130%",
        backgroundRepeat: "no-repeat",
      }}
      className={`${styles.main}`}
    >
      <h1
        style={{ marginBottom: "5%" }}
        className={`text-center ${styles.title}`}
      >
        User Profile
      </h1>
      <Tab items={tabs} activeTab={tabSelected} onTabChange={onTabChange} />
      {tabSelected === 0 ? (
        <Settings
          {...props}
          profileData={profileData}
          changeProfileData={changeProfileData}
          submitProfile={submitProfile}
          handleValidation={handleValidation}
          error={error}
          isValidMail={isValidMail}
        />
      ) : tabSelected === 1 ? (
        <Locale
          {...props}
          profileData={profileData}
          changeProfileData={(e, val) => changeProfileData(e, val)}
          submitProfile={(e, formName) => submitProfile(e, formName)}
        />
      ) : (
        <div style={{ minHeight: "75vh", background: "#ffffff", padding: 10 }}>
          {/* <h1 style={{ color: "#084658" }}>Coming Soon...</h1> */}
          <SupplyChainSettings />
        </div>
      )}
    </div>
  )
}

export default ProfileIndex
