import React, { useState, useEffect } from "react"
import SalesReport from "../../../../../assets/images/Asset 9.png"
import SalesFlash from "../../../../../assets/images/Asset 10.png"
import InventoryBal from "../../../../../assets/images/Asset 11.png"
import AskZip from "../../../../../assets/images/Asset 12.png"
import DetailReport from "../../../../../assets/images/Asset 13.png"
import CustomReport from "../../../../../assets/images/Asset 14.png"
import Styles from "./style.module.scss"
import { Container, Row, Col } from "reactstrap";
import { getGroupOptionsbyGroupName } from "./../../../../../services/Api/authGroups";
import Body from "./../../../../ui/ReportView/Body";
import ButtonTab from "../../../../ui/ButtonsTab";
import SubHeader from "../../../../ui/subHeader";

const circleData = [
  {
    head: "Sales Flash",
    icon: SalesFlash,
    description: `Get real-time status updates on
transactions, stocks and orders.`,
  },
  {
    head: "Sales Reports",
    icon: SalesReport,
    description: `Get real-time status updates on
transactions, stocks and orders.`,
  },
  {
    head: "Inventory Balance",
    icon: InventoryBal,
    description: `Get real-time status updates on
transactions, stocks and orders.`,
  },
  {
    head: "Ask ZIP",
    icon: AskZip,
    description: `Get real-time status updates on
transactions, stocks and orders.`,
  },
  {
    head: "Detailed Reports",
    icon: DetailReport,
    description: `Get real-time status updates on
transactions, stocks and orders.`,
  },
  {
    head: "Custom Reports",
    icon: CustomReport,
    description: `Get real-time status updates on
transactions, stocks and orders.`,
  },
]
function InsiderReports(props) {
  const [show, setShow] = useState("")
  const [dataSet, setDataSet] = useState([]);
  const [showReport, setShowReport] = useState(false)
  const [linkData, setLinkData] = useState([])
  const [loading, setLoading] = useState(false)
  const [activeTab, setActiveTab] = useState(0)
  const onTabChange = (tab) => {
    setActiveTab(tab);
  }
  const Circles = props => (
    <div onClick={() => {
      if (props.urlItems.length) {
        setLinkData(props.urlItems)
        setShowReport(true)
        onTabChange(0)
      } else {
        alert("No url found")
      }
    }}
      className={Styles.businessBorder}>
      <img src={props.icon} style={{ height: "3rem" }} />
    </div>
  )
  useEffect(() => {
    const func = async () => {
      setLoading(true)
      try {
        const data = await getGroupOptionsbyGroupName("insider")
        setDataSet(
          circleData.map(m => {
            const temp = data?.[0]?.items.find(e => e.type.toLowerCase() === m.head.replace(/ /g, '').toLowerCase())
            console.log(temp)
            m.urlItems = temp ? temp.elements : []
            return m
          })
        )
        setLoading(false)
      } catch (err) {
        setLoading(false)
      }
    }
    func()
  }, [])

  return (
    <div className={Styles.main}>
      <SubHeader title="Insider Reports" subTitle="Provide visibility and optimisation recommendations for supply chains." />
      <Container className={`text-center ${Styles.diagramContainer}`}>
        {!loading && !showReport ? <span className={Styles.centerHead}>
          <span>Insider Reports</span>
        </span> : null}

        {loading ? <div>Loading...</div>
          : showReport ?
            <div className='d-flex flex-column w-100'>
            <ButtonTab items={linkData} activeTab={activeTab} onTabChange={onTabChange} />
            <Body url={linkData[activeTab].url} label={linkData[activeTab].lable} backListener={() => setShowReport(false)} />
            </div> :
              <ul className={Styles.businnessContainer}>
                {dataSet.map((item, key) => (
                  <li key={key} style={{ textAlign: "center" }}>
                    <span
                      role="button"
                      onMouseEnter={() => setShow(key)}
                      onMouseLeave={() => setShow(false)}
                      key={key}
                      heading={item.head}
                    >
                      {/* <Link
                  to={
                    key === 1
                      ? "/auth/commercialExp/InfraredSales/doctorPrescription"
                      : props.location.pathname
                  }
                  state={{ buttonItems: doctorButtons, heading: heading }}
                  style={{ color: "#094658" }}
                > */}
                      <Circles urlItems={item.urlItems} icon={item.icon} />
                      {/* </Link> */}
                      <div
                        className={Styles.item_description}
                        style={{
                          position: "absolute",
                          transition: "all 0.2s",
                          zIndex: "-3",
                          transform: key === show ? "scale(1)" : "scale(0)",
                        }}
                      >
                        <span>
                          <div className={Styles.underline}>
                            <p style={{ fontWeight: "bold" }}>{item.head}</p>
                          </div>
                          <p>{item.description}</p>
                        </span>
                      </div>
                    </span>
                  </li>
                ))}
              </ul>
        }
      </Container>
    </div>
  )
}

export default InsiderReports
