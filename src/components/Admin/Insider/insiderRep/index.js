import React, { useState, useEffect } from "react"
import TabOptions from "../../../ui/taboption/index"
import styles from "./style.module.scss"
import InsiderReports from "./InsiderReports"
import Tab from "../../../Tab"
import HeaderCommonComponent from '../../../ui/HeaderCommon/HeaderCommon'
import insiderBanner from "../../../../assets/images/ZPA-Banners-08.png"
import Msg from "../../../constant"

function DataManagement(props) {
  const lime = "#c3d422"
  const white = "#ffffff"
  const [tabSelected, setTab] = useState(0)
  const tabs = ["Insider Reports", "Insider Offline", "CSM"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
  }
  const { location, buttonItems, heading } = props
  return (
    <>
    <HeaderCommonComponent height={'115%'} HeaderImage={insiderBanner} heading={'Insider'} previousProps={props} tabs={tabs} tabSelected={tabSelected} onTabChange={onTabChange} >
      </HeaderCommonComponent>
      {
        tabSelected===0?<div style={{minHeight:"80vh", justifyContent:"center",display:"flex"}}><InsiderReports {...props} /></div> : 
        <Msg/>
      }
    </>    
    // <div
    //   style={{
    //     backgroundSize: "130%",
    //     backgroundRepeat: "no-repeat",
    //   }}
    //   className={`${styles.main}`}
    // >
    //   <h1
    //     style={{ marginBottom: "5%" }}
    //     className={`text-center ${styles.title}`}
    //   >
    //     Insider
    //   </h1>
    //   <Tab
    //       items={tabs}
    //       activeTab={tabSelected}
    //       onTabChange={onTabChange}
    //     />
    //   <InsiderReports {...props} />
    // </div>
  )
}

export default DataManagement
