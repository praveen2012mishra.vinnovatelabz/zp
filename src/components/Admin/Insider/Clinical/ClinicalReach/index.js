import React, { useState } from "react"
import JourneyIcon from "../../../../../assets/images/svg/journey.svg"
import Styles from "./style.module.scss"
import { Container, Row, Col } from "reactstrap"
import { SubHeader } from "../../../../ui/subHeader"

const circleData = [
  {
    head: "Inbound",
    icon: JourneyIcon,
    description: `Access to comprehensive inbound stock receipt 
    and warehousing location of Clinical Trial Materials.`,
  },
  {
    head: "Distribution",
    icon: JourneyIcon,
    description: `Access to comprehensive distribution and 
    supply of Clinical Trial Materials to sites.`,
  },
  {
    head: "Inventory",
    icon: JourneyIcon,
    description: `Access to comprehensive Inventory information such as level of stock 
    supply/resupply, stock expiration, location, and stock availability.`,
  },
  {
    head: "Budget & Expense",
    icon: JourneyIcon,
    description: `Access to tracking of project budget and expense, with forecast.`,
  },
  {
    head: "Detailed Reports",
    icon: JourneyIcon,
    description: `Browse and download from our library 
    of detailed tabular reports for your project.`,
  },
  {
    head: "Custom Reports",
    icon: JourneyIcon,
    description: `Get ZP to customize specific dashboards and reports for you`,
  },
]
function ClinicalReach(props) {
  const [show, setShow] = useState("")
  return (
    <div className={Styles.main}>
      <SubHeader
        title="Clinical Reach Reports"
        subtitle=""
      />
      <Container className={`text-center ${Styles.diagramContainer}`}>
        <span className={Styles.centerHead}>
          <span>Clinical Reach Reports</span>
        </span>

        <ul className={Styles.businnessContainer}>
          {/* <img src={Pentagon} className={Styles.pentagon} /> */}
          {circleData.map((item, key) => (
            <li key={key} style={{ textAlign: "center" }}>
              <span
                role="button"
                onMouseEnter={() => setShow(key)}
                onMouseLeave={() => setShow(false)}
                key={key}
                heading={item.head}
              >
                {/* <Link
                  to={
                    key === 1
                      ? "/auth/commercialExp/InfraredSales/doctorPrescription"
                      : props.location.pathname
                  }
                  state={{ buttonItems: doctorButtons, heading: heading }}
                  style={{ color: "#094658" }}
                > */}
                <Circles icon={item.icon} />
                {/* </Link> */}
                <div
                  className={Styles.item_description}
                  style={{
                    position: "absolute",
                    transition: "all 0.2s",
                    zIndex: "-3",
                    transform: key === show ? "scale(1)" : "scale(0)",
                  }}
                >
                  <span>
                    <div className={Styles.underline}>
                      <p style={{ fontWeight: "bold" }}>{item.head}</p>
                    </div>
                    <p>{item.description}</p>
                  </span>
                </div>
              </span>
            </li>
          ))}
        </ul>
      </Container>
    </div>
  )
}
const Circles = props => (
  <div className={Styles.businessBorder}>
    <img src={props.icon} style={{ height: "3rem" }} />
  </div>
)
export default ClinicalReach
