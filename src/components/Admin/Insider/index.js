import React, { useState } from "react"
import styles from "./style.module.scss"
import HeaderCommonComponent from '../../ui/HeaderCommon/HeaderCommon'
import PillBody from "./pillBody"
import insiderBanner from "../../../assets/images/ZPA-Banners-08.png"
import { getGroupOptionsbyGroupName } from "../../../services/Api/authGroups"
import InsiderReports from './insiderRep'
const _ = require("underscore")
const InsiderPage = (props) => {
  // const [insiderOptions, setInsiderOptions] = useState([])
  // const [customReports, setCustomReports] = useState([])
  // const [salesFlash, setSalesFlash] = useState([])
  // const [salesReport, setSalesReport] = useState([])
  // const [inventoryBalance, setInventoryBalance] = useState([])
  // const [askZIP, setAskZIP] = useState([])
  // const [detailedReports, setDetailedReports] = useState([])
  // const [loading, setLoading] = useState(true)
  // let options={}
  // React.useEffect(()=>{
  //   // getoptions()
  // },[])

  // const getoptions= async ()=>{
  //   try {
  //   options= await getGroupOptionsbyGroupName('insider')
  //   options && options.length && options[0].items.map((item,key)=>{
  //     if(item.type==="SalesFlash"){
  //       console.log(item.elements,"i am here");
  //       setSalesFlash(item.elements)
  //     }else if(item.type==="SalesReport"){
  //       setSalesReport(item.elements)
  //     }else if(item.type==="InventoryBalance"){
  //       setInventoryBalance(item.elements)
  //     }else if(item.type==="AskZIP"){
  //       setAskZIP(item.elements)
  //     }else if(item.type==="DetailedReports"){
  //       setDetailedReports(item.elements)
  //     }else if(item.type==="CustomReports"){
  //       setCustomReports(item.elements)
  //     }else{
  //       setInsiderOptions(options)
  //     }
  //   })
  //   console.log(options,"______________papso");
  //   setLoading(false)
  //   } catch (err) {
  //     setLoading(false)
  //   }
  // }
  // const [activeTab, setActiveTab] = useState(0)
  // const heading = ["Sales Report", "Sales Trend", "Sales Monitoring"]
  // const onTabChange = tabIdx => {
  //   if (activeTab !== tabIdx) {
  //     setActiveTab(tabIdx)
  //   }
  // }
  // const tabs =[
  //   "Sales Report",
  //   "Sales Flash",
  //   "Inventory Balance",
  //   "Ask Zip",
  //   "Detailed Reports",
  //   "Custom Reports",
  // ]
  return (
    <>
    {/* <HeaderCommonComponent HeaderImage={insiderBanner} heading={'Insider'} previousProps={props} tabs={tabs} tabSelected={activeTab} onTabChange={onTabChange} ></HeaderCommonComponent> */}

      <div className={`py-4 px-lg-5 ${styles.contentWrapper}`} style={{ minHeight: "75vh" }}>
        <InsiderReports />
        {/* {!loading?(activeTab === 0 ? (
          <>
            <PillBody buttonItems={InsiderReports} head={"mello"} />
          </>
        ) :
        activeTab === 1 ? (
          <>
            <PillBody buttonItems={salesFlash} head="lllll" />
          </>
        ) : 
        activeTab === 2 ? (
          <>
            <PillBody buttonItems={inventoryBalance} />
          </>
        ) :
        activeTab === 3 ? (
          <>
            <PillBody buttonItems={askZIP} />
          </>
        ) :
        activeTab === 4 ? (
          <>
            <PillBody buttonItems={detailedReports} />
          </>
        ) :
        activeTab === 5 ? <PillBody buttonItems={customReports} /> : null):null } */}
      </div>
    </>
  )
}

export default InsiderPage
