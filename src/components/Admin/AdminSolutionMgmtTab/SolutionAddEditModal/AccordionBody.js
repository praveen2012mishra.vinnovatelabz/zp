import React, { useState } from "react"
import { Container, Row, Col } from "react-bootstrap"
import { Plus, Dash } from "react-bootstrap-icons"
import InputGroup from "./inputGroup"

import styles from "./style.module.scss"

const _= require('underscore')
const AccordionBody = ({
  title,
  data,
  onDataChange,
  onAddField,
  onDeleteField,
  moduleOption,
  permissionOption
}) => {
  const [mapData,setMapData]= useState([])
  React.useEffect(()=>{
    setMapData(data)
  },[data])
  const onModuleChange = (value, idx) => {
    onDataChange(value, idx, "module")
  }

  const onPermissionChange = (value, idx) => {
    onDataChange(value, idx, "permission")
  }
  return (
    <Container
      className={[styles.accordion__body, styles.px48, "py-2"].join(" ")}
    >
      <Row className={`py-3 ${styles.borderBottom}`}>
        <Col sm={11}>
          <p className={`text-bold font-weight-bold h6`}>{title}</p>
        </Col>
        <Col sm={1}>
          <button
            className={`btn rounded-circle ${styles.addBtn}`}
            onClick={onAddField}
          >
            <Plus />
          </button>
        </Col>
      </Row>
      {mapData.length > 0? mapData.map((d, idx) => (
        <InputGroup
          key={idx}
          data={d}
          onModuleChange={e => onModuleChange(e,idx)}
          onPermissionChange={e => onPermissionChange(e,idx)}
          onDelete={() => onDeleteField(idx)}
          permissionOption={permissionOption}
          moduleOption={moduleOption}
        />
      )):null}
    </Container>
  )
}

export default AccordionBody
