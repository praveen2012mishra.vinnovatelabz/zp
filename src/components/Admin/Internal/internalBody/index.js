import React, { useState } from "react"
import Body from "../../../ui/ReportView/Body"
import ButtonTabs from "../../../ui/ButtonsTab"
import styles from "./style.module.scss"
import Msg from "../../../constant"

const InternalBody = ({ buttonItems=[],head="hello" }) => {
  const [activeTab, setActiveTab] = useState(0)
  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
    }
  }
console.log(activeTab, buttonItems[activeTab]);
   return (
      <div
        className={`${styles.header} inside-pills`}
        style={{
          marginTop: -79,
          marginLeft: -50,
          marginRight: -48,
          marginBottom: 30,
        }}
      >
        <ButtonTabs
          items={buttonItems}
          activeTab={activeTab}
          onTabChange={onTabChange}
        />{
          buttonItems.length===0? <div style={{ background: "#ffffff",
            minHeight: "100vh"}}><Msg /></div> :
          (buttonItems[activeTab] && <Body url={buttonItems[activeTab].url} label={buttonItems[activeTab].lable} />)
        }
      </div>
  )
}

export default InternalBody
