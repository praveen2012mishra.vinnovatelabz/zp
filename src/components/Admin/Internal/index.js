import React, { useState, useEffect } from 'react'
import { getGroupOptionsbyGroupName } from '../../../services/Api/authGroups'
import HeaderCommonComponent from '../../ui/HeaderCommon/HeaderCommon'
import InternalBody from "./internalBody"
import styles from "./style.module.scss"

function Internal(props) {
    const [tabSelected, setTab] = useState(0)
    const [loading, setLoading] = useState(false)
    const [activeData, setActiveData] = useState([])
    const [tabState, setTabState] = useState([])

    const tabs = ["Management Overview", "Finance", "Procurement", "HR", "Operations", "QBR", "CRM", "IMS", "Templates"]
    const onTabChange = tabIdx => {
        if (tabSelected !== tabIdx) {
            setTab(tabIdx)
            setActiveData(tabState[tabSelected])
        }
    }

    useEffect(() => {
        const getOptions = async () => {
            setLoading(true)
            try {
                const data = await getGroupOptionsbyGroupName('Internal')
                setTabState(tabs.map(t => {
                    const obj = data[0].items.find(m => m.type.toLowerCase() == t.replace(/ /g, "").toLowerCase())
                    if (obj) {
                        return obj.elements
                    }
                })
                )

                console.log(data);
                setLoading(false)
            } catch {
                setLoading(false)
            }
        }
        getOptions()
    }, [])

    console.log(tabState);

    return (
        <>
            <HeaderCommonComponent heading={'Internal'} previousProps={props} tabs={tabs} tabSelected={tabSelected} onTabChange={onTabChange} ></HeaderCommonComponent>
            <div className={`py-4 px-lg-5 ${styles.contentWrapper}`} style={{ minHeight: "75vh" }}>
                {!loading ? <InternalBody buttonItems={tabState[tabSelected]} /> : null}
            </div>

        </>
    )
}

export default Internal;