import React, { useEffect, useState } from "react"
import { Container, Row, Col } from "reactstrap"
import Styles from "./style.module.scss"
import Optimization from "../../../../../assets/images/svg/optimization.svg"
import JourneyIcon from "../../../../../assets/images/svg/journey.svg"
import BundlingIcon from "../../../../../assets/images/svg/bundling.svg"
import DoctorIcon from "../../../../../assets/images/svg/doctor.svg"
import PriceIcon from "../../../../../assets/images/svg/price.svg"
import TimingIcon from "../../../../../assets/images/svg/timing.svg"
import UtilizationIcon from "../../../../../assets/images/svg/utilization.svg"
import { Link, navigate } from "gatsby"
import { SubHeader } from "../../../../ui/subHeader"
import { getGroupOptionsbyGroupName } from "../../../../../services/Api/authGroups"
import Body from "../../../../ui/ReportView/Body"
import { Button } from "react-bootstrap"
// import SubHeader from "../../../../ui/subHeader"

const circleData = [
  {
    head: "Patient Journey",
    icon: JourneyIcon,
    description: `Detailed analytics on each
    individual patient’s response to drug
    use, medical history and particulars.`,
  },
  {
    head: "Doctor Prescription Behaviour",
    icon: DoctorIcon,
    description: `Insights into doctor’s pescription
    behaviour for preferred drug(s)
    during patient treatment cycle`,
  },
  {
    head: "Tender Utilisation",
    icon: UtilizationIcon,
    description: `Strong focus on maintaining good
    relationships with our partners and
    the patients we interact with.`,
  },
  {
    head: "Order Timing",
    icon: TimingIcon,
    description: `Consistent updates on status of
    orders, ETAs and a wide variety of
    delivery options are offered.`,
  },
  {
    head: "Price Sensitivity",
    icon: PriceIcon,
    description: `Prices are fair, reasonable and are
    flexible dependent on the quantity
    of each order.`,
  },
  {
    head: "Promotion Optimisation",
    icon: Optimization,
    description: `Speed up efficiency of promotional
    offers, marketing for any established
    or new pharmaceutical products.`,
  },
  {
    head: "Product Bundling",
    icon: BundlingIcon,
    description: `Easy mass bundling of products for
    shipments across the globe.`,
  },
  {
    head: "Infrared Base",
    icon: BundlingIcon,
    description: `Infrared Base subscription using Distribution and Prescription Data.`,
  },
]
export default function Tab1(props) {
  const [show, setShow] = React.useState(false)
  const [dataSet, setDataSet] = useState([]);
  const [showReport, setShowReport] = useState(false)
  const [link, setLink] = useState({})
  const Circles = props => (
    <div onClick={() => {
      if (props.urlItem.url) {
        setShowReport(true)
        setLink(props.urlItem)
      } else {
        alert("No url found")
      }
    }}
      className={Styles.circleBorder}>
      <img src={props.icon} style={{ height: "3rem" }} />
      {/* <svg style={{ height: "3rem" }}>{props.icon}</svg> */}
    </div>
  )

  const doctorButtons = [
    "Prescription Behaviour",
    "Patient Journey",
    "Doctor Profile",
  ]
  const heading = [
    "Doctor's Prescribing Behaviour",
    "Doctor's Patient Journey",
    "Doctor Profile",
  ]
  useEffect(() => {
    const func = async () => {
      const data = await getGroupOptionsbyGroupName("commercialExcellence")
      console.log(data);
      setDataSet(
        circleData.map(m => {
          const obj = data?.[0]?.items.filter(e => (e.type).toLowerCase() === "infrared")
          if (obj && obj.length > 0 && obj.length < 2) {
            const temp = obj[0].elements.find(e => e.lable == m.head)
            m.url = temp ? temp.url : ""
          }
          return m
        })
      )
    }
    func()
  }, [])

  return (
    <div className={Styles.main}>
      {/* tab div */}
      <SubHeader
        title="Infrared"
        subtitle="Identify Potential areas of opportunity and growth."
      />
      {/* diagram */}
      <Container className={`text-center ${Styles.diagramContainer}`}>
        {/* <span
          className={Styles.centerHead} */}
        {/* // style={{
          //   textAlign: "start !important",
          //   position: "absolute",
          //   marginTop: "5%",
          //   fontSize: 25,
          //   fontWeight: "bold",
          //   color: "#06333f",
          // }} */}
        {/* // >
        //   <span>{`Distribution/`}</span>
        //   <br />
        //   <span>{`Prescription Data`}</span>
        // </span> */}

        {showReport ? <Body url={link.url} label={link.lable} backListener={() => setShowReport(false)} /> :
          <ul className={Styles.circleContainer}>
            {dataSet.map((item, key) => (
              <li key={key} style={{ textAlign: "center" }}>
                <span
                  role="button"
                  onMouseEnter={() => setShow(key)}
                  onMouseLeave={() => setShow(false)}
                  key={key}
                  heading={item.head}
                >
                  <Link
                    to={
                      key === 1
                        ? `${props.basePath}/commercialExp/InfraredSales/doctorPrescription`
                        : props.location.pathname
                    }
                    state={{ buttonItems: doctorButtons, heading: heading }}
                    style={{ color: "#094658" }}
                  >
                    <Circles icon={item.icon} urlItem={item} />
                  </Link>
                  <div
                    className={Styles.item_description}
                    style={{
                      position: "absolute",
                      transition: "all 0.2s",
                      zIndex: show === 7 ? "2" : "-3",
                      transform: key === show ? "scale(1)" : "scale(0)",
                    }}
                  >
                    <span>
                      <div className={Styles.underline}>
                        <p style={{ fontWeight: "bold" }}>{item.head}</p>
                      </div>
                      <p>{item.description}</p>
                    </span>
                  </div>
                </span>
              </li>
            ))}
          </ul>
        }
      </Container>
    </div>
  )
}

