import React, { useState } from "react"
import styles from "./style.module.scss"
import style2 from "../../Insider/pillBody/style.module.scss"
import Tab1 from "./tab1"
import DoctorsPrescription from "../../Insider/pillBody/index"
import TabOptions from "../../../ui/taboption/index"
import HeaderImage from "../../../../assets/images/commercialBanner.png"
import Tab from "../../../Tab"
import Msg from "../../../constant"
import { basePath } from "../../../basePath"
import HeaderCommonComponent from '../../../ui/HeaderCommon/HeaderCommon'

export default function InfraredSales(props) {
  const lime = "#c3d422"
  const white = "#ffffff"
  const [tabSelected, setTab] = useState(0)
  const tabs = ["Infrared"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
  }
  const { location, buttonItems, heading } = props
  return (
    <HeaderCommonComponent heading={'Commercial Excellence'}
      HeaderImage={HeaderImage}
      height={''}
      previousProps={props} 
      tabs={tabs} 
      tabSelected={tabSelected} 
      onTabChange={onTabChange} >
        {tabSelected === 0 ? (
      location.pathname === `${basePath}/commercialExp/InfraredSales` ? (
        <Tab1 {...props} />
      ) : location.pathname ===
      `${basePath}/commercialExp/InfraredSales/doctorPrescription`? (
        <div className={`py-4 px-lg-5 ${style2.contentWrapper}`}>
          <DoctorsPrescription buttonItems={buttonItems} heading={heading} />
        </div>
      ) : (
        ""
      )
    ) : (
      <Msg />
    )}</HeaderCommonComponent>   
  )
}
