import React, { useEffect } from "react"
import { Container, Row, Col } from "react-bootstrap"
import cogo from "cogo-toast"
import styles from "./style.module.scss"
import ToggleButton from "../../ui/ToggleButton"
import { connect } from "react-redux"
import LoadingOverlay from "react-loading-overlay"
import {
  setAdminSettingsMessage,
  getAdminSettingsAction,
} from "../../../services/Redux/actions/adminSettingsAction"
const mapStateToProps = state => ({
  data: state.adminSettings.settingsData,
  err: state.adminSettings.err,
  notification: state.notification,
  messageData: state.adminSettings.getMessages,
})

const mapDispatchToProps = {
  setAdminSettingsMessage,
  getAdminSettingsAction,
}

const AdminSettingsTab = ({
  data,
  err,
  setAdminSettingsMessage,
  notification,
  getAdminSettingsAction,
  messageData,
}) => {
  const [status, setStatus] = React.useState(false)
  const [message, setMessage] = React.useState("")
  const [loading, setLoading] = React.useState(false)
  const [displayMessage, setDisplayMessage] = React.useState("")

  useEffect(() => {
    const settingAction = async () => await getAdminSettingsAction()
    settingAction()
  }, [])

  useEffect(() => {
    if (messageData.data?.maintenance_mode_status) {
      setMessage(messageData.data.maintenance_message)
      setStatus(true)
    }
  }, [messageData])

  const submitMessage = async e => {
    e.preventDefault()
    setLoading(true)
    let obj = {}
    if (!status) {
      obj = {
        maintenance_message: message,
        maintenance_mode_status: status,
      }
    } else {
      obj = {
        message,
        status,
      }
    }

    await setAdminSettingsMessage(obj)
  }

  // handle notifications
  if (notification.notify) {
    if (notification.type === "error") {
      cogo.error(notification.message)
      if (loading) setLoading(false)
    } else if (notification.type === "success") {
      cogo.success(notification.message)
      if (loading) setLoading(false)
    }

    // prevent showing notification in future render without any
    // actions
    notification.notify = false
  }
  if (err) {
    if (loading) setLoading(false)
  }

  return (
    <LoadingOverlay active={loading} spinner>
      <Container fluid className={styles.settings}>
        <p className="h5">
          <b>Maintenance Alert Management</b>
        </p>
        <Row className="mt-5">
          <Col>
            <label className="d-block mb-2">Maintenance Message:</label>
            <textarea
              rows="5"
              cols="50"
              className={styles.textarea}
              disabled={!status}
              value={message}
              onChange={e => setMessage(e.target.value)}
            />
          </Col>
          <Col>
            <label className="d-block mb-2">Maintenance Mode Message:</label>
            <ToggleButton
              checked={status}
              onChange={async () => {
                await setStatus(!status)
                status && setMessage("")
              }}
            />
          </Col>

          <Col xs={12}>
            <button
              className={`btn mt-2 ${styles.btn}`}
              onClick={submitMessage}
            >
              SAVE
            </button>
          </Col>
        </Row>

        <p className="h5 mt-5">
          <b>Maintenance Alert Management</b>
        </p>
        <Row className="mt-5 align-items-end">
          <Col xs={4}>
            <label className="d-block mb-2">Password:</label>
            <input className={styles.input} />
          </Col>
          <Col>
            <button className={`btn ${styles.btn}`}>SAVE</button>
          </Col>
        </Row>
      </Container>
    </LoadingOverlay>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminSettingsTab)
