import React, { useState, useEffect } from 'react'
import { Container } from 'react-bootstrap'
import HeaderCommonComponent from '../../../ui/HeaderCommon/HeaderCommon'
import { basePath } from '../../../basePath'
import ScenarioPlanner from './ScenarioPlanner'
import InventoryPlanner from './InventoryPlanner'
import SupplyChainHomePage from './plannerHomepage/index'

function SupplyChainPlanner(props) {
    const [tabSelected, setTab] = useState("")
    const [pageNumber, setPage] = useState(1)
    const [dropStatus, setStatus] = useState(false)
    const tabs = [" "]
    const onTabChange = tabIdx => {
        if (tabSelected !== tabIdx) {
            setTab(tabIdx)
        }
    }
    // console.log("basepath", basePath)
    const dropButtonItem = ['Approval Status', 'Month', 'Inventory Health', 'Brand']
    const resultStatus = ['Export results', 'Approve all'];
    const tableHeaderList = ['SKU', 'APPROVAL STATUS', 'INVENTORY HEALTH ', 'CURRENT STOCK', 'SKU', 'APPROVAL STATUS', 'INVENTORY HEALTH '];
    const tableValues = [['TriCycloBull 30mg 30s ', 'Approval', 'Urgent', '2500', '2300', '2', '1'], ['TriCycloBull 30mg 30s ', 'Pending Approval', 'NonNonUrgent', '2500', '2300', '2', '1'], ['TriCycloBull 30mg 30s ', 'Pending Approval', 'Urgent', '2500', '2300', '2', '1'], ['TriCycloBull 30mg 30s ', 'Pending Approval', 'NonUrgent', '2500', '2300', '2', '1'], ['TriCycloBull 30mg 30s ', 'Pending Approval', 'NonUrgent', '2500', '2300', '2', '1'], ['TriCycloBull 30mg 30s ', 'Pending Approval', 'NonUrgent', '2500', '2300', '2', '1']];
    console.log(basePath)
    return (
        <>
            <HeaderCommonComponent
                heading={'Supply Chain Excellence'}
                previousProps={props} tabs={tabs}
                tabSelected={tabSelected}
                onTabChange={onTabChange} />
            <SupplyChainHomePage />
            {
                basePath ==="/auth" ? (
                    <>
                        <InventoryPlanner />
                        {/* <ScenarioPlanner /> */}
                    </>
                ) : null
            }
        </>
    )
}

export default SupplyChainPlanner
