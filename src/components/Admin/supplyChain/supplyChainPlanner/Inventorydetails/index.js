import React from "react";
import styles from "./style.module.scss";

function Inventorydetails(props) {
    const montharray = ["JUL 2019", "AUG 2019", "SEPT 2019", "OCT 2019", "NOV 2019", "DEC 2019", "JAN 2020", "FEB 2020", "MAR 2020", "APR 2020", "MAY 2020", "JUN 2020", "JUL 2020", "AUG 2020", "SEPT 2020", "OCT 2020", "NOV 2020"]
    const dummydata = ["123", "120", "156", "234", "134", "156", "146", "156", "156", "178", "124", "146", "198", "156", "189", "178", "198"]

    return (
        <>
            <div>
                <h5>INVENTORY LEVEL DETAILS</h5>

                <div className={styles.Inventorybox}>
                    <table>
                        <tr className={styles.tablemonthrow}>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            {montharray.map((item, index) => (
                                <td className={styles.tableheader} id={index}>{item}</td>
                            ))}

                        </tr>
                        <tr className={styles.tablerow}>
                            <td>Baseline Demand</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>

                            {dummydata.map((item, index) => {
                                return item ?
                                    <><td className={styles.tablecolumn} id={index}>{item}</td></> :
                                    <><td><hr className={styles.border}></hr></td></>
                            })}
                        </tr>
                        <tr className={styles.tablerow}>
                            <td>Scenario 1</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>


                            {dummydata.map((item, index) => (
                                <td className={styles.tablecolumn} id={index}>{item}</td>
                            ))}
                        </tr>
                        <tr className={styles.tablerow}>
                            <td>Safety Stock</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>

                            {dummydata.map((item, index) => (
                                <td className={styles.tablecolumn} id={index}>{item}</td>
                            ))}
                        </tr>
                        <tr className={styles.tablerow}>
                            <td>Suggested PO Arrivals</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>


                            {dummydata.map((item, index) => (
                                <td className={styles.tablecolumn} id={index}>{item}</td>
                            ))}
                        </tr>
                        <tr className={styles.tablerow}>
                            <td>Planned PO Arrivals</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>


                            {dummydata.map((item, index) => (
                                <td className={styles.tablecolumn} id={index}>{item}</td>
                            ))}
                        </tr>
                        <tr className={styles.tablerow}>
                            <td>Projected Inventory</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>

                            {dummydata.map((item, index) => (
                                <td className={styles.tablecolumn} id={index}>{item}</td>
                            ))}
                        </tr>
                        <tr className={styles.lastrow}>
                            <td style={{ color: "#fff" }}>Inventory Coverage</td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>
                            <td><hr className={styles.border}></hr></td>


                            {dummydata.map((item, index) => (
                                <td className={styles.tablecolumn1} id={index}>{item}</td>
                            ))}
                        </tr>

                    </table>

                </div>

            </div>
        </>
    )
}

export default Inventorydetails