import React, { useState } from 'react'
import styles from "./style.module.scss"
import LineChart from '../Linechart/index'
import Inventorydetails from '../Inventorydetails'
import { BsCloud } from "react-icons/bs"
import { RiMoneyDollarBoxLine } from "react-icons/ri"
import { ImEyedropper } from "react-icons/im"
import { HiOutlineFire } from "react-icons/hi"
import { BsBriefcase } from "react-icons/bs"


function ScenarioPlanner(props) {

    const width = 800, height = 350, margin = 20
    const data = [
        { a: 1, b: 3 },
        { a: 2, b: 6 },
        { a: 3, b: 2 },
        { a: 4, b: 12 },
        { a: 5, b: 8 }
    ]
    const head1 = ["INVENTORY HEALTH", "LEAD TIME FORCAST", "REPLENISHMENT RATE", "REPLENISHMENT QUANTITY"];
    const head2 = ["FORCAST METHOD", "CURRENT STOCK", "AVG.SUPPLY(6 MONTHS)", "AVG.DEMAND(6 MONTHS)", "SELLS OUT IN(MONTH)", "LEAD TIME(MONTH)"];
    const data1 = ["Urgent", "139", "Dec 2020", "500"];
    const data2 = ["ARIMA", "85", "141", "156", "4", "2"];


    return (
        <>

            <div className={styles.container}>

                <h1>Scenario Planner</h1>
                <i class="fas fa-cloud" style={{ height: "10px", width: "20px" }}></i>
                <label className={styles.labell}>
                    TriCycloBull 30mg 30sec
                </label>
                <table className={styles.table}>
                    <tr className={styles.tableHeader}>
                        {head1.map((item, index) => (
                            <td className={styles.tabledata} id={index}>{item}</td>
                        ))}


                    </tr>
                    <tr className={styles.tablerow}>
                        {data1.map((item, index) => {
                            return item === "Urgent" ?
                                <>
                                    <td className={styles.fontColor} id={index}>{item}</td></> :


                                <>  <td className={styles.tabledata} id={index}>{item}</td>

                                </>
                        })}


                    </tr>

                </table>

                <table className={styles.table1}>
                    <tr className={styles.tableHeader}>
                        {head2.map((item, index) => (

                            <td className={styles.tabledata} id={index}>{item}</td>
                        ))}


                    </tr>
                    <tr className={styles.tablerow}>
                        {data2.map((item, index) => (

                            <td className={styles.tabledata} id={index}>{item}</td>
                        ))}

                    </tr>

                </table>

                <div className={styles.bodypart}>
                    <div style={{ height: 100, width: 100 }}></div>
                    <div className={styles.sidebar}>
                        <div><p className={styles.sidebartext}>Drag scenarios to points in time where they are expected to occur</p></div>
                        <ul className={styles.lists}>
                            <li><button className={styles.sidebarbtn}><BsCloud size={15} color="#ffffff" className={styles.fontMargin} />Weather changes</button></li>
                            <li> <button className={styles.sidebarbtn}><BsBriefcase size={15} color="#ffffff" className={styles.fontMargin} />Product Launch</button></li>
                            <li><button className={styles.sidebarbtn}><RiMoneyDollarBoxLine size={15} color="#ffffff" className={styles.fontMargin} />Promotions</button></li>
                            <li><button className={styles.sidebarbtn}><ImEyedropper size={15} color="#ffffff" className={styles.fontMargin} />COVID</button></li>
                            <li><button className={styles.sidebarbtn}><HiOutlineFire size={15} color="#ffffff" className={styles.fontMargin} />Competition</button></li>
                        </ul>
                        <p className={styles.sidebartext1}>Define a custom scenario if the event is not available above</p>
                        <button className={styles.cuttombtn}>Custom scenario</button>
                    </div>
                    <div className={styles.graph}>
                        <LineChart data={data} width={width} height={height} margin={margin} />
                    </div>
                </div>
                <div>
                    <Inventorydetails />
                </div>

            </div>

        </>
    )
}
export default ScenarioPlanner

