import React, { useState } from 'react'
import styles from "./style.module.scss"
import { Container } from 'reactstrap'
import { SubHeader } from '../../../../ui/subHeader'
import { FaBeer } from "react-icons/fa"
import { Circle } from 'react-bootstrap-icons'
import { Link } from "gatsby"
import DemandForecasting from '../../../../../assets/images/Asset 21.png'
import ControlTower from '../../../../../assets/images/Asset 22.png'
import ReplenishmentOptimisation from '../../../../../assets/images/Asset 24.png'
import InventoryOptimisation from '../../../../../assets/images/Asset 23.png'

const circleData = [
    {
        head: "Demand Forecasting",
        icon: DemandForecasting,
        description:
            `Detailed analytics on each
        individual patient’s response to drug
        use, medical history and particulars.`
    },
    {
        head: "Control Tower",
        icon: ControlTower,
        description: `Provides real-time information via fully customized
        dashboards that display specific metrics, allowing
        users to have end-to-end visibility over the entire
        supply chain`},
    {
        head: "Replenishment Optimisation",
        icon: ReplenishmentOptimisation,
        description: `Strong focus on maintaining good
        relationships with our partners and
        the patients we interact with.`},
    {
        head: "Inventory Optimisation",
        icon: InventoryOptimisation,
        description: `Consistent updates on status of
        orders, ETAs and a wide variety of
        delivery options are offered.`},
]
function InventoryOptimization() {
    const [show,setShow]=useState(false)
    const title = "Overview of Solutions";
    const subTitle = "Provide visibility and optimisation recommendations for supply chains";
    return (
        <div className={styles.main}>
            <SubHeader title={title} subtitle={subTitle} />
            <div style={{ width: "100%" }}>
                <Container style={{paddingBottom:20}} className={styles.diagramContainer}>
                    <ul className={styles.diagram}>
                        {
                            circleData.map((item, index) => (<li key={index} style={{ textAlign: "center" }}>
                                <span role="button" key={index} 
                                    onMouseEnter={() => setShow(index)}
                                    onMouseLeave={() => setShow(false)}
                                    heading={item.head}
                                >
                                    <a href="#">
                                        <Circles icon={item.icon} />
                                    </a>
                                    <div
                                        className={styles.item_description}
                                        style={{
                                            position: "absolute",
                                            transition: "all 0.2s",
                                            zIndex: "-3",
                                            transform: index === show ? "scale(1)" : "scale(0)",
                                        }}
                                    >
                                        <span>
                                            <div className={styles.underline}>
                                                <p style={{ fontWeight: "bold" }}>{item.head}</p>
                                            </div>
                                            <p>{item.description}</p>
                                        </span>
                                    </div>
                                </span>
                            </li>))
                        }
                    </ul>
                </Container>
            </div>
        </div>
    )
}

const Circles = props => (
    <div className={styles.circleBorder}>
        {/* <svg style={{height:"3rem"}}> */}
        <img src={props.icon} style={{ height: "3rem" }} />
        {/* </svg> */}
    </div>
)

export default InventoryOptimization
