import React, { useState } from 'react'
import styles from "./style.module.scss"
import TabOptions from "../../../ui/taboption/index"
import ControlHome from "./controlHome"
import Tab from '../../../Tab'
import Msg from '../../../constant'
import HeaderCommonComponent from '../../../ui/HeaderCommon/HeaderCommon'
import controlTowerBanner from '../../../../assets/images/controlTower.png'

function ControlTower(props) {
    const [tabSelected, setTab] = useState(0)
    const tabs = ["Control Tower", "Inventory Optimisation", "Replenishment Optimisation", "Demand Forecasting"]
    const onTabChange = tabIdx => {
        if (tabSelected !== tabIdx) {
            setTab(tabIdx)
        }
    }

    return (
        <>        
            <HeaderCommonComponent height={'15%'} HeaderImage={controlTowerBanner} heading={'Supply Chain Excellence'} previousProps={props} tabs={tabs} tabSelected={tabSelected} onTabChange={onTabChange} ><TabView index={tabSelected} /></HeaderCommonComponent>
        </>
    )
}
const TabView = ({ index }) => {
    if (index === 0) {
        return <ControlHome />
    }
    else if (index === 1) {
        return <Msg />
    }
    else if (index === 2) {
        return <Msg />
    }
    else if (index === 3) {
        return <Msg />
    }
}

export default ControlTower
