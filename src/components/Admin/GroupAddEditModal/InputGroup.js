import React from "react"
import { Row, Col } from "react-bootstrap"
import { Dash } from "react-bootstrap-icons"

import styles from "./style.module.scss"

const InputGroup = ({ data, onLabelChange, onUrlChange, onDelete }) => {
  return (
    <Row className={`py-2 ${styles.borderBottom}`}>
      <Col sm={6}>
        <label>LABEL</label>
        <input
          className={styles.input}
          value={data.lable || data.label}
          onChange={onLabelChange}
        />
      </Col>
      <Col sm={5}>
        <label>URL</label>
        <input
          className={styles.input}
          value={data.url}
          onChange={onUrlChange}
        />
      </Col>
      <Col sm={1}>
        <button
          className={`btn rounded-circle ${styles.deleteBtn}`}
          onClick={onDelete}
        >
          <Dash />
        </button>
      </Col>
    </Row>
  )
}

export default InputGroup
