import React from "react"
import AdminButtons from "./AdminLogsButtonTabs"
import styles from "./styles.module.scss"
import { Row, Col } from "react-bootstrap"
function AdminLogsButtons({ buttonItems, activeTab, onTabChange }) {
  return (
    <div
      className={`d-flex flex-row justify-content-between ${styles.buttonTabsAdmin}`}
    >
      {buttonItems?.map((item, idx) => (
        <Col className="colmd" md={4}>
          <AdminButtons
            key={item}
            active={idx === activeTab}
            activeClassname={styles.activeAdmin}
            title={item}
            onClick={() => onTabChange(idx)}
          />
        </Col>
      ))}
    </div>
  )
}

export default AdminLogsButtons
