import React from "react"

const TabItem = ({ onClick, active, activeClassname, title }) => (
  <div className={active ? activeClassname : ""} onClick={onClick}>
    <p>{title}</p>
  </div>
)

export default TabItem
