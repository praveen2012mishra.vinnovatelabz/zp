import React from "react"

import styles from "./style.module.scss"
import TabItem from "./TabItem"

const Tab = ({ items, activeTab, onTabChange }) => {

  return (
    <div className={`${styles.tabContainer} ${items.length<4?styles.displaySingleItem:''}`}>
    <div className={`d-flex flex-row justify-content-around ${items.length>3?styles.tabs:styles.tabatsmallno} ${items.length<4?styles.displaySingleItemWidth:''} ${styles.blueBackground}`} wid={items.length>3?'100%':"auto"}>
    {items.map((item, idx) => (
      <TabItem
        key={item}
        active={idx === activeTab}
        activeClassname={styles.active}
        title={item}
        onClick={() => onTabChange(idx)}
      />
    ))}
  </div>
    </div>
  )
}

export default Tab
