import React from "react"
import { navigate } from "gatsby"

import { isAdmin } from "../services/authService"

const AdminRoute = ({ component: Component, ...rest }) => {
  if (!isAdmin()) {
    localStorage.setItem("lastRoute", window.location.pathname)
    navigate("/Login")
    return null
  }
  return <Component {...rest} />
}

export default AdminRoute
