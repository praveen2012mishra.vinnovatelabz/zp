// checks localStorage if userType is admin
import Cookies from "js-cookie"
import { JsonWebTokenError } from "jsonwebtoken"
import { twofactorEnforceCheck } from "./Api/2FA.api"
let isBrowser = typeof window !== "undefined"

const checkStatus= async(samlToken)=>{
  let status=await twofactorEnforceCheck(samlToken.user.name_id)
  // await new Promise((resolve, reject) => setTimeout(resolve, 2000))
  console.log(status,"--------------API")
  return status
}

export const isAdmin =() => {
  //localStorage is not avaialable outside browser
  // if (Cookies.get("saml_response")) {
  //   let samlCookie= Cookies.get("saml_response")
  //   let samlToken = JSON.parse(samlCookie.substr(2, samlCookie.length))
  //   let finalCall=checkStatus(samlToken).then((res,rej)=>{
  //     if(res){
  //     let twoFaCookie= Cookies.get("2fa")? JSON.parse(Cookies.get("2fa")).authorized : null
  //     // let tok= twoFaCookie? JSON.parse(twoFaCookie).authorized:null
  //     console.log(twoFaCookie,"basepath 2fa cookie");
  //     if (twoFaCookie!==null){
  //       console.log("/auth")
  //       return true
  //     }
  //     else {
  //       console.log("/app")
  //       let route = isBrowser ? window.localStorage.getItem("isAdmin") : null
  //       return false
  //     }
  //   }else{
  //     console.log("/auth")
  //     return true
  //   }
  //   }).catch(e=>false)
  //   return finalCall
  // } else {
  //   console.log("/app")
  //   let route = isBrowser ? window.localStorage.getItem("isAdmin") : null
  //   return false
  // }
   return true
}
