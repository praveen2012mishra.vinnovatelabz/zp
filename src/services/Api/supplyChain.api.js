import axios from '../../../axiosConfig'
//const dummy="/group/fetchGroupUsers/EX Tab HKAMGEN Insider"
const apiQuery=`/group/fetchGroupUsers/`;
const api=apiQuery;
export const getGroupUsers= async (group_name)=>{
    let grpOP= await axios.get(api+group_name) 
    // add "group_name" when not dummy 
    //(let grpOP= await axios.get(api+group_name))
    return grpOP.data.data;
}

export const saveUserRoles = async (data) => {
    const url = "/users/saveUserRoles"
    let roles = await axios.post(url, data)
    return roles
}

export const getGroups = async () => {
    const url = "/group/getAllGroups"
    let groups = await axios.get(url)
    return groups
}
