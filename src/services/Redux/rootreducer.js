import { combineReducers } from "redux"

import adminReducer from "./reducers/adminReducer"
import notificationReducer from "./reducers/notificationReducer"
import sessionReducer from "./reducers/sessionReducer"
import settingsReducer from "./reducers/adminSettingsReducer"
import userGroupReducer from './reducers/useGroupsReducer'
export default combineReducers({
  admin: adminReducer,
  notification: notificationReducer,
  adminSession: sessionReducer,
  adminSettings: settingsReducer,
  userGroup:userGroupReducer
})
