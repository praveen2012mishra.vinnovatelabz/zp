import axios from "../../../../axiosConfig"
import {
    ADMIN_SESSION_DATA_FETCHED_ALL,
    ADMIN_TWO_FACTOR_DATA_FETCHED_ALL
} from "../actionTypes"
import { showNotification } from "./notification"

export const setAllSessionData = (err, data) => ({
    type: ADMIN_SESSION_DATA_FETCHED_ALL,
    payload: { err, data:data },
})
export const setAllTwoFactorData = (err, data) => ({
    type: ADMIN_TWO_FACTOR_DATA_FETCHED_ALL,
    payload: { err, data:data },
})
// SESSION MANAGEMENT
export const getAllSessionData = () => async dispatch => {
    try {
        const response = await axios.get("/session/getListofUserswithEnforcedSingleSession")
        if (Array.isArray(response.data)) {
            dispatch(setAllSessionData(null, response.data))
        }
        else {
            // if any other response comes except the group management data
            // array, dispatch with error
            dispatch(setAllSessionData(new Error("Failed to Fetch Data"), []))
            dispatch(showNotification("Failed to Session Data", "error"))
        }
    } catch (e) {
        dispatch(setAllSessionData(new Error("Failed to Fetch data"), []))
        dispatch(showNotification("Failed to Fetch Session data", "error"))
    }
}

export const resetSessionCount = (
    users
) => async dispatch => {
    try {
        const response = await axios.post(`/session/resetSessionCount`,users)
        if (response.data.success===true) {
            dispatch(showNotification("Reset Successful", "success"))
        } else {
            console.log("dummy")
            dispatch(showNotification("Failed to Reset", "error"))
        }
    } catch (e) {
        console.log(e, "<-------catch-------")
        dispatch(showNotification("Failed to Reset", "error"))
    }
}
//TWO FACTOR SESSIONS
export const getAllTwoFactorData = () => async dispatch => {
    try {
        const response = await axios.get("/session/getListofUserswithTwoFactorAuthentication")
        if (Array.isArray(response.data)) {
            dispatch(setAllTwoFactorData(null, response.data))
        }
        else {
            dispatch(setAllTwoFactorData(new Error("Failed to Fetch Data"), []))
            dispatch(showNotification("Failed to fetch Two factor List", "error"))
        }
    } catch (e) {
        dispatch(setAllTwoFactorData(new Error("Failed to Fetch Data"), []))
        dispatch(showNotification("Failed to fetch Two factor List", "error"))
    }
}

export const resetQR = (
    users
) => async dispatch => {
    try {
        const response = await axios.post(`/session/resetQRCode`,users)
        if (response.data.success===true) {
            dispatch(showNotification("Reset Successful", "success"))
        } else {
            console.log("dummy")
            dispatch(showNotification("Failed to Reset", "error"))
        }
    } catch (e) {
        console.log(e, "<-------catch-------")
        dispatch(showNotification("Failed to Reset", "error"))
    }
}