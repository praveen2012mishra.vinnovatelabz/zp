import axios from "../../../../axiosConfig"
import {
  SET_ADMIN_SETTINGS_MESSAGE,
  GET_ADMIN_SETTINGS_MESSAGE,
} from "../actionTypes"
import { showNotification } from "./notification"

export const setAllDataType = (err, data) => {
  return { type: SET_ADMIN_SETTINGS_MESSAGE, payload: { err, data } }
}
export const getAllDataType = (err, data) => {
  return { type: GET_ADMIN_SETTINGS_MESSAGE, payload: { err, data } }
}
export const setAdminSettingsMessage = message => async dispatch => {
  try {
    console.log(message)
    let response = await axios.post(`/settings/setMaintenanceSettings`, message)
    console.log(response)
    if (response.data.success) {
      dispatch(setAllDataType(null, response.data))
      dispatch(showNotification("Settings Updated Successfully", "success"))
    } else {
      dispatch(setAllDataType(null, response.data))
      dispatch(showNotification("Failed to Update Message", "error"))
    }
  } catch (e) {
    dispatch(showNotification("Failed to Update Message", "error"))
  }
}

export const getAdminSettingsAction = () => async dispatch => {
  try {
    const response = await axios.get(`/settings/getMaintenanceSettings`)
    if (response.data.success) {
      dispatch(getAllDataType(null, response.data))
    } else {
      dispatch(getAllDataType(null, response.data))
      dispatch(showNotification("Failed to Update Message", "error"))
    }
  } catch (err) {
    dispatch(showNotification("Failed to Update Message", "error"))
  }
}
