import { USER_GROUP } from "../actionTypes"

/* function to show a notification */
export const setUserGroupName = name => ({
  type: USER_GROUP,
  payload: { name },
})