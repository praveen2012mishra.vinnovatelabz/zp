import { SHOW_NOTIFICATION } from "../actionTypes"

/* function to show a notification */
export const showNotification = (message, type) => ({
  type: SHOW_NOTIFICATION,
  payload: { message, type },
})
