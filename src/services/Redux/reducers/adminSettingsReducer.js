import {
  SET_ADMIN_SETTINGS_MESSAGE,
  GET_ADMIN_SETTINGS_MESSAGE,
} from "../actionTypes"

const initialState = {
  settingsData: {},
  getMessages: {},
  err: "",
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_ADMIN_SETTINGS_MESSAGE:
      return {
        ...state,
        settingsData: payload.data,
        err: payload.err,
      }
    case GET_ADMIN_SETTINGS_MESSAGE:
      return {
        ...state,
        getMessages: payload.data,
        err: payload.err,
      }

    default:
      return state
  }
}
