import { SHOW_NOTIFICATION } from "../actionTypes"

const initialState = {
  notify: false,
  message: "",
  type: "",
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_NOTIFICATION: {
      return {
        notify: true,
        type: payload.type,
        message: payload.message,
      }
    }

    default:
      return state
  }
}
