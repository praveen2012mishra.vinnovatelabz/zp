import { USER_GROUP } from "../actionTypes"

const initialState = {
    name: "",
}

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case USER_GROUP: {
            return {
                name: payload.name
            }
        }

        default:
            return state
    }
}
