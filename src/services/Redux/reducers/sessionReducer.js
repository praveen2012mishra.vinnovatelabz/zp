import {
    ADMIN_SESSION_DATA_FETCHED_ALL,
    ADMIN_SESSION_DATA_RESET,
    ADMIN_TWO_FACTOR_DATA_FETCHED_ALL,
    ADMIN_RESET_QR
} from "../actionTypes"

const initialState = {
    session: [],
    twoFA:[],
    err: null,
}

export default (state = initialState, { type, payload }) => {
    switch (type) {
        case ADMIN_SESSION_DATA_FETCHED_ALL: {
            const newState = { session: payload.data, err: payload.err }
            return newState
        }
        case ADMIN_SESSION_DATA_RESET: {
            let updatedState=state.session
            updatedState=state.session.splice(0,state.session.length,...updatedState)
            const newState = { session: updatedState, err: payload.err }

            return newState
        }
        case ADMIN_TWO_FACTOR_DATA_FETCHED_ALL: {
            const newState = { twoFA: payload.data, err: payload.err }
            return newState
        }
        case ADMIN_RESET_QR: {
            let updatedState=state.twoFA
            updatedState=state.group.splice(0,state.twoFA.length,...updatedState)
            const newState = { twoFA: updatedState, err: payload.err }

            return newState
        }
        default:
            return state
    }
}
