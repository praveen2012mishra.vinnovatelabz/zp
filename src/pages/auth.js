import React from "react"
import { Router } from "@reach/router"
import AdminPage from "../components/Admin"
import AdminRoute from "../components/AdminRoute"
import InfraredSales from "../components/Admin/CommercialExp/InfraredSales/index"
import InsiderRoute from "../components/Admin/Insider"
import InsiderReports from "../components/Admin/Insider/insiderRep/index"
import DoctorPrescription from "../components/Admin/CommercialExp/DoctorsPrescribing/index"
import SubscriptionRoute from "../components/Admin/Subscription"
import DataManagement from "../components/Admin/BusinessIntelligence/DataManagement/index"
import ControlTower from "../components/Admin/supplyChain/controlTower"
import ClinicalReach from "../components/Admin/Insider/Clinical/index"
import Profile from "../components/Admin/Profile"
import SupplyChainPlanner from "../components/Admin/supplyChain/supplyChainPlanner"
import Internal from "../components/Admin/Internal"
import Carrers from "../pages/About/Careers"
import CaseStudies from "../pages/About/CaseStudies"
import DigitalEmpowerment from "../pages/About/DigitalEmpowerment"
import OurTeams from "../pages/About/OurTeams"


const Auth = () => {
  return (
    <Router basepath="/auth">
      <AdminRoute path="/admin" component={AdminPage} />
      <AdminRoute path="/insider" component={InsiderRoute} />
      <AdminRoute path="/subscription" component={SubscriptionRoute} />
      <AdminRoute path="/insider/insiderDetails" component={InsiderReports} />
      <AdminRoute path="/insider/clinicalReach" component={ClinicalReach} />
      <AdminRoute
        path="/commercialExp/InfraredSales"
        component={InfraredSales}
      />
      <AdminRoute
        path="/businessInt/datamanagement"
        component={DataManagement}
      />
      <AdminRoute
        path="/commercialExp/InfraredSales/doctorPrescription"
        component={DoctorPrescription}
      />

      <AdminRoute path="/supplyChain/controlTower" component={ControlTower} />


      <AdminRoute path="/profile" component={Profile} />
      <AdminRoute path="/supplyChain/planner" component={SupplyChainPlanner} />
      <AdminRoute path="/internal" component={Internal} />
      <AdminRoute path="/careers" component={Carrers} />
      <AdminRoute path="/caseStudies" component={CaseStudies} />
      <AdminRoute path="/digitalEmpowerment" component={DigitalEmpowerment} />
      <AdminRoute path="/ourTeams" component={OurTeams} />
      

    </Router>
  )
}

export default Auth
