import React from "react"
import { Router } from "@reach/router"
import { basePath } from "../components/basePath"
import LocalRoute from "../components/LocalRoute"
// import InsiderRoute from "../components/Admin/Insider"
// import InsiderReports from "../components/Admin/Insider/insiderRep/index"
import ControlTower from "../components/Admin/supplyChain/controlTower"
// import ClinicalReach from "../components/Admin/Insider/Clinical/index"
import DataManagement from "../components/Admin/BusinessIntelligence/DataManagement/index"
import InfraredSales from "../components/Admin/CommercialExp/InfraredSales/index"
import DoctorPrescription from "../components/Admin/CommercialExp/DoctorsPrescribing/index"
import SupplyChainPlanner from "../components/Admin/supplyChain/supplyChainPlanner"

const App = () => {
  return (
    <Router basepath="/app">
      {/* <LocalRoute path="/insider" component={InsiderRoute} />
      <LocalRoute path="/insider/insiderDetails" component={InsiderReports} />
      <LocalRoute path="/insider/clinicalReach" component={ClinicalReach} /> */}
      <LocalRoute
        path="/commercialExp/InfraredSales"
        component={InfraredSales}
      />
      <LocalRoute
        path="/businessInt/datamanagement"
        component={DataManagement}
      />
      <LocalRoute
        path="/commercialExp/InfraredSales/doctorPrescription"
        component={DoctorPrescription}
      />
      <LocalRoute path="/supplyChain/planner" component={SupplyChainPlanner} />
      <LocalRoute path="/supplyChain/controlTower" component={ControlTower} />
      
    </Router>
  )
}

export default App
