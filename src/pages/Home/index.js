import React, { Component } from "react"
// import { connect } from "react-redux"
// import { sampleFunction } from "./actions";
import { Row, Col, Container, Media } from "reactstrap"
// import bulbIcon from "../../assets/images/ZHA-bulb.png"
// import mimiThian from "../../assets/images/mimi-thian-jxUuXxUFfp4-unsplash.jpg"
// import medicine1 from "../../assets/images/kendal-L4iKccAChOc-unsplash.jpg"
// import medicine2 from "../../assets/images/adam-niescioruk-hWzrJsS8gwI-unsplash.jpg"

import "./style.scss"
import { getAdminSettingsAction } from "../../services/Redux/actions/adminSettingsAction"
// import * as Scroll from "react-scroll"
import { connect } from "react-redux";
// import Img from "gatsby-image";
import { useStaticQuery, StaticQuery, graphql } from "gatsby";
// import fetch from 'cross-fetch'
// import { useQuery } from '@apollo/react-hooks';
import LazyLoad from 'react-lazy-load';

// import { ApolloClient, InMemoryCache, gql, HttpLink } from '@apollo/client';

// const client = new ApolloClient({
//   // uri: 'https://48p1r2roz4.sse.codesandbox.io',
//   link: new HttpLink({ uri: 'https://ezpz.zuelligpharma.com/graphql', fetch}),
//   cache: new InMemoryCache()
// });

const mapStateToProps = state => ({
  messageData: state.adminSettings.getMessages,
})

const bulbIcon = "ZHA-bulb.png"
const mimiThian = "mimi-thian-jxUuXxUFfp4-unsplash.jpg"
const medicine1 = "kendal-L4iKccAChOc-unsplash.jpg"
const medicine2 = "adam-niescioruk-hWzrJsS8gwI-unsplash.jpg";
const homeBanner = "ZHA-banner-31.jpg"


const OptimizedImage = ({ src, ...props }) => {
  const StaticImages = useStaticQuery(graphql`
     query {
       bulbIcon: file(relativePath: { eq: "ZHA-bulb.png" }) {
        childImageSharp {
          fluid(quality: 30) {
           originalImg
          }
          resize(quality: 100) {
           src
          }
        }
      }, 
      mimiThian: file(relativePath: { eq: "mimi-thian-jxUuXxUFfp4-unsplash.jpg" }) {
        childImageSharp {
          fluid(quality: 30, maxWidth: 700) {
           originalImg
          }
          resize(quality: 70, width: 700) {
           src
          }
        }
      },
      medicine1: file(relativePath: { eq: "kendal-L4iKccAChOc-unsplash.jpg" }) {
        childImageSharp {
          fluid(quality: 20, maxWidth: 500) {
           originalImg
          }
          resize(quality: 50, , width: 700) {
           src
          }
        }
     },
     medicine2: file(relativePath: { eq: "adam-niescioruk-hWzrJsS8gwI-unsplash.jpg" }) {
      childImageSharp {
        fluid(quality: 20, maxWidth: 300) {
         originalImg
        }
        resize(quality: 50) {
         src
        }
      }
    },
      homeBanner: file(relativePath: { eq: "ZHA-banner-31.jpg" }) {
        childImageSharp {
          fluid(quality: 30, maxWidth: 1000) {
           originalImg
          }
          resize(quality: 70, width: 1000) {
           src
          }
        }
      }, 
      speedBanner: file(relativePath: { eq: "jared-arango-1-mh6U3qeGQ-unsplash.jpg" }) {
        childImageSharp {
          fluid(quality: 20, maxWidth: 700) {
           originalImg
          }
          resize(quality: 30, width: 1000) {
           src
          }
        }
      }
    }
    `,)

  // const data = useStaticQuery(graphql`
  //   query getImages {
  //     file(relativePath: { eq: "adam-niescioruk-hWzrJsS8gwI-unsplash.jpg" }) {
  //      childImageSharp {
  //        fluid(quality: 10) {
  //         originalImg
  //        }
  //        resize(quality: 50) {
  //         src
  //        }
  //      }
  //    }
  //   }
  //  `)

  // const data = StaticImages[src]

  // client.query({
  //   query: gql `
  //    query getImages($src: String) {
  //       file(relativePath: { eq: $src }) {
  //         childImageSharp {
  //         fluid(quality: 30) {
  //           originalImg
  //         }
  //         resize(quality: 50) {
  //           src
  //         }
  //       }
  //     }
  //   }
  //  `,  variables: {src: 'adam-niescioruk-hWzrJsS8gwI-unsplash.jpg'}
  // }).then(data => {
  //   console.log('image1', data, src)
  // })


  console.log('image1', StaticImages)
  // return null
  return <LazyLoad width={'100%'} height={props.lazyHeight}>
    {StaticImages[src]?<img
      src={StaticImages[src]?.childImageSharp.fluid.originalImg}
      // src={StaticImages[src].childImageSharp.resize.src}
      {...props}
    ></img>:null}
  </LazyLoad>
}

const mapDispatchToProps = { getAdminSettingsAction }

class Home extends Component {
  constructor(props) {
    super(props)
    this.insightRef = React.createRef()
  }
  async componentDidMount() {
    await this.props.getAdminSettingsAction()
  }
  componentWillReceiveProps(nextProps) {
    // var scroller = Scroll.scroller
    // this.props.getScroll(this.insightRef.offsetTop)
    // if (nextProps.scrollId && nextProps.childPath === "/") {
    //   scroller.scrollTo("insights", {
    //     duration: 300,
    //     smooth: "linear",
    //   })
    // }
  }
  render() {
    return (
      <div>
        {/*//////// banner area ///////////*/}
        <section className="home-banner align-items-center">
          <OptimizedImage className='w-100 fill-wrapper position-absolute' src={'homeBanner'} />
          <Container fluid className="container_padding">
            {this.props.messageData.data?.maintenance_mode_status ? (
              <div className="maintenance_message">
                <b>MAILTENANCE ALERT: </b>
                {this.props.messageData.data?.maintenance_message}
              </div>
            ) : (
                ""
              )}
            <Row style={{ marginRight: 0 }}>
              <Col lg={8} md={10} style={{ paddingLeft: "50px" }}>
                <div className="pl-lg-5">
                  <h1>
                    Actionable Insights,
                    <br /> Smarter Decisions
                  </h1>
                  <a href="#" className="more-btn mt-3 btn btn-link">
                    Learn More
                  </a>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        {/*////////// We innovate to meet your needs /////////*/}
        <section>
          <Container fluid>
            <Row>
              <Col
                xl={5}
                lg={6}
                className="bg-light-teal pt-4 pb-5 d-flex flex-column justify-content-between px-4"
              >
                <h2 className="title">We innovate to meet your needs</h2>
                <div className="pl-lg-4 pl-2 content-area">
                  <OptimizedImage
                    src={'bulbIcon'}
                    alt="bulb icon"
                    className="bulb-icon mb-4"
                  />
                  <h5 className="mb-2">
                    We are an agile and result-focused team. If our existing
                    solutions don’t meet your needs, we will create ones that
                    do, all so that you can focus on what really matters.
                  </h5>
                </div>
              </Col>
              <Col xl={7} lg={6} className="px-0">
                <OptimizedImage
                  src={'mimiThian'}
                  alt="mimi than image"
                  className="w-100 h-100 right-imge"
                />
              </Col>
            </Row>
          </Container>
        </section>

        {/*//////// We deliver solutions better,with speed and clarity. //////////*/}
        <section className="speed-section">
          <OptimizedImage className='w-100 object-cover fill-wrapper position-absolute' src={'speedBanner'} />
          <Container fluid>
            <Row>
              <Col xl={8} lg={9} className="pt-4 pb-5 px-4">
                <h2 className="title">
                  We deliver solutions better, with speed and clarity.
                </h2>
              </Col>
            </Row>
          </Container>
          <Container fluid className="speed-container">
            <Row className="justify-content-between px-5">
              <Col lg={3} md={4}>
                <div className="text-center speed-card mt-4">
                  <h6 className="mb-4">FASTER</h6>
                  <p className="text-white">
                    Data delivered faster than the competition
                  </p>
                </div>
              </Col>
              <Col lg={3} md={4}>
                <div className="text-center speed-card mt-4">
                  <h6 className="mb-4">SMARTER</h6>
                  <p className="text-white">
                    Visualised data at your finger tips
                  </p>
                </div>
              </Col>
              <Col lg={3} md={4}>
                <div className="text-center speed-card mt-4">
                  <h6 className="mb-4">BETTER</h6>
                  <p className="text-white">
                    Accurate, non-extrapolated data for your analysis
                  </p>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

        {/* ////////  Our Insights  /////// */}
        <section
          className="insights-section"
          name="insights"
          ref={el => (this.insightRef = el)}
        >
          <Container fluid>
            <Row>
              <Col xl={8} lg={9} className="pt-4 pb-5 px-4">
                <h2 className="title">Our Insights</h2>
              </Col>
            </Row>
            <Row className="px-lg-5">
              <Col md={8} sm={6} className="mt-4">
                <div className="custom-media h-100" style={{flexDirection:"row",display:"flex"}}>
                  <div left className="mr-3 media-left">
                    <OptimizedImage lazyHeight='100%' className="w-100 h-100 object-cover " src={'medicine1'} />
                    {/* <Media
                      className="w-100 h-100 object-cover"
                      object
                      src={medicine1}
                      alt="Generic placeholder image"
                    /> */}
                    {/* </OptimizedImage> */}
                  </div>
                  <div
                    body
                    className=" media-body border-left-0 pl-0 d-flex flex-column justify-content-between"
                  >
                    <div>
                      <small>5 MIN READ</small>
                      <h3 className="my-3">
                        Keeping it real — How we #FighttheFakes
                      </h3>
                      <p>
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam nonummy nibh euismod tincidunt ut...
                      </p>
                    </div>
                    <a href="#" className="read-more">
                      Read More
                    </a>
                  </div>
                </div>
              </Col>
              <Col md={4} sm={6} className="mt-4">
                <div className="custom-media flex-wrap">
                  <div left className="w-100 mb-3">
                    <OptimizedImage lazyHeight='80%' className="w-100 h-100 object-cover" src={'medicine2'} />
                    {/* </OptimizedImage> */}
                  </div>
                  <div
                    body
                    style={{height:"auto"}}
                    className=" media-body border-top-0 pt-0 d-flex flex-column justify-content-between"
                  >
                    <div>
                      <small>9 MIN READ</small>
                      <h4 className="my-3">
                        Why healthcare needs to breakdown the great wall of data
                      </h4>
                    </div>
                    <a href="#" className="read-more">
                      Read More
                    </a>
                  </div>
                </div>
              </Col>
              <Col md={4} xs={6} className="mt-4">
                <div className="custom-media flex-wrap">
                  <div
                    body
                    className="media-body border-top-0 pt-0 d-flex flex-column justify-content-between"
                  >
                    <div>
                      {/* <small>9 MIN READ</small> */}
                      <h4 className="my-3">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing elit
                      </h4>
                    </div>
                    <a href="#" className="read-more">
                      Read More
                    </a>
                  </div>
                </div>
              </Col>
              <Col md={4} xs={6} className="mt-4">
                <div className="custom-media flex-wrap">
                  <div
                    body
                    className=" media-body border-top-0 pt-0 d-flex flex-column justify-content-between"
                  >
                    <div>
                      {/* <small>9 MIN READ</small> */}
                      <h4 className="my-3">
                        Lorem ipsum dolor sit amet, consectetuer adipiscing
                        elit, sed diam
                      </h4>
                    </div>
                    <a href="#" className="read-more">
                      Read More
                    </a>
                  </div>
                </div>
              </Col>
              <Col md={4} xs={6} className="mt-4">
                <div className="custom-media flex-wrap">
                  <div
                    className=" media-body border-top-0 pt-0 d-flex flex-column justify-content-between"
                  >
                    <div>
                      {/* <small>9 MIN READ</small> */}
                      <h4 className="my-3">Lorem ipsum dolor sit amet</h4>
                    </div>
                    <a href="#" className="read-more">
                      Read More
                    </a>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>
      </div>
    )
  }
}
// const mapStateToProps = state => {
//     const home = state.home;
//     return (
//         {
//             show: home.show,
//         }
//     )
// }

// export default connect(mapStateToProps, { sampleFunction })(Home);
export default connect(mapStateToProps, mapDispatchToProps)(Home)
