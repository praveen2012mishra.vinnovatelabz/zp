import React, { useEffect } from "react"
import { createApolloFetch } from "apollo-fetch"
import Layout from "../../layouts"
import { Row, Col, Container, Media } from "reactstrap"
import medicine1 from "../../assets/images/kendal-L4iKccAChOc-unsplash.jpg"
const uri = "https://ezpz.zuelligpharma.com/graphql"

const arr = {
  imageUrl: medicine1,
  title: "Lorem Ipsum is simply dummy",
  excerpt:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  uri: "item.uri",
}
const query = `
query MyQuery {
    posts {
        nodes {
            uri
            title(format: RENDERED)
            excerpt(format: RENDERED)
            id
            postId
            status
            date
            dateGmt
            databaseId
            authorId
            categories {
                nodes {
                    id
                    name
                    parentId
                    uri
                    categoryId
                }
            }
            content(format: RENDERED)
            tags {
                nodes {
                    id
                    tagId
                    name
                }
            }
            authorDatabaseId
            slug
            author {
                node {
                    id
                    name
                    nicename
                    nickname
                    userId
                    username
                    slug
                    email
                    firstName
                    lastName
                    locale
                }
            }
            featuredImage {
                    node {
                        mediaItemUrl
                        mediaItemId
                        mimeType
                        mediaType
                        sizes
                        slug
                        sourceUrl(size: LARGE)
                        title
                        uri
                    }
                }
        }
    }
}`
const apolloFetch = createApolloFetch({ uri })

function Insights(props) {
  const [data, setData] = React.useState("")
  const [articleUrl, setArticleUrl] = React.useState("")
  useEffect(() => {
    apolloFetch({ query })
      .then(res => setData(res.data))
      .catch(err => console.log(err))
  }, [])
  const encodeUrl = (obj) => {
    var str = "";
    for (var key in obj) {
      if (str != "") {
        str += "&";
      }
      str += (key + "=" + encodeURIComponent(obj[key]));
    }
    return str
  }
  const structData = item => {
    let excerptTrim = `${item.excerpt.slice(3, 150)}...`
    const strData = {
      imageUrl: item.featuredImage.node.sourceUrl,
      // imageUrl:item.imageUrl,
      title: item.title,
      excerpt: excerptTrim,
      uri: item.uri,
      content: item.content,
      tags: item.tags.nodes[0].name,
      // categories:item.categories.nodes
    }
    // let urlData = JSON.stringify(strData)

    return strData
  }
  const readMore = (e, urlData) => {
    let encodedUrlData = encodeUrl(urlData)

    setArticleUrl(encodedUrlData)
  }
  let ar = Array(10).fill(arr)
  return (
    <div>
      {/* <h1>{data.posts.nodes[0].title}</h1> */}
      <section className="insights-section">
        <Container fluid>
          <Row>
            <Col xl={8} lg={9} className="pt-4 pb-5 px-4">
              <h2 className="insightTitle">Our Insights</h2>
            </Col>
          </Row>
          <Row className="px-lg-5">
            {data?.posts?.nodes.map((item, index) => {
              {/* {ar.map((item, index) => {  */ }
              let structDataVar = structData(item)
              return (
                <>
                  {index === 0 ? (
                    <Col md={12} sm={12} className="mt-4" style={{ maxHeight: "20em" }}>
                      <Media className="custom-media h-100" style={{ width: "100%" }}>
                        <Media left className="mr-3">
                          <Media
                            className="w-100 h-100 object-cover"
                            object
                            src={structDataVar.imageUrl}
                            alt="Generic placeholder image"
                          />
                        </Media>
                        <Media
                          body
                          className="border-left-0 pl-0 d-flex flex-column justify-content-between"
                        >
                          <div>
                            <small>5 MIN READ</small>
                            <h3 className="my-3">{structDataVar.title}</h3>
                            {structDataVar.excerpt}
                          </div>
                          <a
                            href={`/ReadArticle?${articleUrl}`}
                            onClick={e => readMore(e, structDataVar)}
                            className="read-more"
                          >
                            Read More
                          </a>
                        </Media>
                      </Media>
                    </Col>
                  ) : (
                      <Col md={4} sm={6} className="mt-4">
                        <Media className="custom-media flex-wrap">
                          <Media left className="w-100 mb-3">
                            <Media
                              className="w-100"
                              object
                              src={structDataVar.imageUrl}
                              alt="Generic placeholder image"
                            />
                          </Media>
                          <Media
                            body
                            className="border-top-0 pt-0 d-flex flex-column justify-content-between"
                          >
                            <div>
                              <small>9 MIN READ</small>
                              <h4 className="my-3">{structDataVar.title}</h4>
                            </div>
                            <a href="#" className="read-more">
                              Read More
                          </a>
                          </Media>
                        </Media>
                      </Col>
                    )}
                </>
              )
            })}
          </Row>
          {/* <Col md={4} xs={6} className="mt-4">
              <Media className="custom-media flex-wrap">
                <Media
                  body
                  className="border-top-0 pt-0 d-flex flex-column justify-content-between"
                >
                  <div>
                    <h4 className="my-3">
                      Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                      sed diam
                    </h4>
                  </div>
                  <a href="#" className="read-more">
                    Read More
                  </a>
                </Media>
              </Media>
            </Col> */}
          {/* <Col md={4} xs={6} className="mt-4">
              <Media className="custom-media flex-wrap">
                <Media
                  body
                  className="border-top-0 pt-0 d-flex flex-column justify-content-between"
                >
                  <div>
                    <h4 className="my-3">Lorem ipsum dolor sit amet</h4>
                  </div>
                  <a href="#" className="read-more">
                    Read More
                  </a>
                </Media>
              </Media>
            </Col> */}
        </Container>
      </section>
    </div>
  )
}

export default Insights
