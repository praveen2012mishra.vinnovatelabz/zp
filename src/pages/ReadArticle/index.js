import React,{useState,useEffect} from 'react'
import { Row, Col, Container, Media } from "reactstrap"
import Style from "./style.module.scss"
import RelatedArticles from "./RelatedArticles"
function SelectedArticle(props) {
    const [data,setData] = useState({});

    useEffect(()=>{
        let queryParams = parseURLParams(window.location.href)

        let str = queryParams.content
        //replaces the html tag from a text with blank string
        queryParams.content = str.replace( /(<([^>]+)>)/ig, ''); 
        setData(queryParams)
    },[])

    //Parses the data coming from URL
    function parseURLParams(url) {
    var queryStart = url.indexOf("?") + 1,
        queryEnd   = url.indexOf("#") + 1 || url.length + 1,
        query = url.slice(queryStart, queryEnd - 1),
        pairs = query.replace(/\+/g, " ").split("&"),
        parms = {}, i, n, v, nv;

    if (query === url || query === "") return;

    for (i = 0; i < pairs.length; i++) {
        nv = pairs[i].split("=", 2);
        n = decodeURIComponent(nv[0]);
        v = decodeURIComponent(nv[1]);

        if (!parms.hasOwnProperty(n)) parms[n] = "";
        parms[n] = v
    }
    return parms;
}
    return (
        <section className={Style.insightSection}>
        <Container fluid style={{padding:"0 5em"}}>
            <Row style={{justifyContent:"center"}}>
            <Col xl={12} lg={12} className="pt-4 px-4">
              <h2 className={Style.insightTitle}>Our Insights</h2>
            </Col>
          </Row>
          <Row style={{justifyContent:"center"}}>
            <Col xl={12} lg={12} className="pt-3 px-4">
              <h2 className={Style.dataTitle}>{data.title}</h2>
            </Col>
          </Row>
          <Row style={{justifyContent:"center"}}>
            <Col xl={12} lg={12} className="pt-5 px-4">
              <h2 className={Style.readingTime}>5 min read</h2>
            </Col>
          </Row>
          <Row style={{justifyContent:"center"}}>
            <Col xl={12} lg={12} className="pt-5 px-4">
              <img className={Style.image} src={data.imageUrl}/>
            </Col>
          </Row>
          <Row style={{justifyContent:"center"}}>
            <Col xl={12} lg={12} className="pt-5 pb-5 px-4">
                <p className={Style.content}>
                    {data.content}
                </p>
            </Col>
          </Row>
        </Container>
        <hr className={Style.hrColor}/>
        <RelatedArticles data={data}/>
        </section>
    )
}

export default SelectedArticle
