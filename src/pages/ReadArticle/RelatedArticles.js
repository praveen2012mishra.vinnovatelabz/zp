import React from 'react'
import { Row, Col, Container, Media } from "reactstrap"
import Style from "./style.module.scss"
import medicine1 from "../../assets/images/adam-niescioruk-hWzrJsS8gwI-unsplash.jpg"

const arr = {
  imageUrl: medicine1,
  title: "Lorem Ipsum is simply dummy",
  excerpt:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  uri: "item.uri",
}
let ar = Array(3).fill(arr)
function RelatedArticles({data}) {
    return (
        <Container fluid style={{padding:"0 5em"}}>
            <Row>
                <Col xl={12} lg={12} className="pt-4 pb-4 px-4">
              <h2 className={Style.relatedArticles}>Related Articles</h2>
            </Col>
            </Row>
             <Row>
                 {
                     ar.map((item,index)=>{
                         return <Col md={4} sm={6} className="mt-4">
                <Media className="custom-media flex-wrap">
                    <Media left className="w-100 mb-3">
                        <Media
                            className="w-100"
                            object
                            src={item.imageUrl}
                            alt="Generic placeholder image"
                        />
                        </Media>
                    <Media
                        body
                        className="border-top-0 pt-0 d-flex flex-column justify-content-between"
                    >
                        <div>
                           <small>9 MIN READ</small>
                           <h4 className="my-3">{item.title}</h4>
                         </div>
                        <a href="#" className="read-more">
                            Read More
                         </a>
                    </Media>
                </Media>
            </Col>
        })
        }
          </Row>
        </Container>
    )
}

export default RelatedArticles
