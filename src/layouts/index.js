import React, { useState, useEffect } from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import Header from "../components/Header"
import Footer from "../components/Footer"
import Home from "../pages/Home/index"
import "./style.scss"
// import * as Scroll from "react-scroll"
// import { rhythm } from "utils/typography"

const LayOut = ({ children }) => {
  const [childPath, setChildPath] = useState("")
  // const [scrollPage, setScrollPage] = useState(false)
  const [scrollBeh, setScrollBeh] = useState("")
  const [togg, setTogg] = useState(false)
  useEffect(() => setChildPath(children.props.path), [])
  useEffect(() => {
    if (childPath === "/") children.props.navigate("/")
  }, [childPath])

  // const insightsChange = async () => {
  //   var scroller = Scroll.scroller
  //   if (childPath !== "/") {
  //     setChildPath("/")
  //     setScrollPage(true)
  //   } else {
  //     scroller.scrollTo("insights", {
  //       duration: 300,
  //       smooth: "linear",
  //     })
  //   }
  // }

  // const getScroll = (ref, toggle) => {
  //   setScrollBeh(ref)
  //   setTogg(toggle)
  // }
  return (
    <div>
      <Header
        // click={() => insightsChange()}
        // scrollId={scrollPage}
        // insightScrollPoint={scrollBeh}
        toggle={togg}
        urlRoute={children.props.path}
      />
      {childPath == "/" ? (
        <Home
          childPath={childPath}
          // scrollId={scrollPage}
          // getScroll={(e, toggle) => getSinsightsChangecroll(e, toggle)}
        />
      ) : (
        children
      )}
      <Footer />
    </div>
  )
}
export default LayOut
